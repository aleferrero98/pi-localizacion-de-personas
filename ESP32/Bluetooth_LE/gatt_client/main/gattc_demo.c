
/****************************************************************************
* 
* Este programa muestra los clientes GATT BLE, escanea los dispositivos BLE y
* se conecta a un dispositivo. 
* Ejecute la demostración del servidor gatt, la demostración del cliente se conectará 
* automáticamente a la demostración del servidor gatt. Dos disp podran intercambiar datos.
* La demostración del cliente habilitará la notificación de gatt_server después de la conexión
*
****************************************************************************/

//extraido de -> https://github.com/espressif/esp-idf/tree/master/examples/bluetooth/bluedroid/ble/gatt_client 
//tutorial en -> https://github.com/espressif/esp-idf/blob/master/examples/bluetooth/bluedroid/ble/gatt_client/tutorial/Gatt_Client_Example_Walkthrough.md

#include <stdint.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "nvs.h"
#include "nvs_flash.h"

#include "esp_bt.h"                 //Configura el controllador BT y VHCI desde el host
#include "esp_gap_ble_api.h"        //Implementa la conf GAP, como parametros de conexion
#include "esp_gattc_api.h"          //Implementa la conf de cliente GATT, como conexion de perifericos 
                                    //y busqueda de servicios
#include "esp_gatt_defs.h"
#include "esp_bt_main.h"            //Inicializa y habilita el stack Bluedroid 
#include "esp_gatt_common_api.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "esp_bt_device.h"

#include "esp_gap_bt_api.h" //CLASSIC BLUETOOTH GAP API

#define GATTC_TAG           ""
#define OVERFLOW_TAG        "OVERFLOW"

#define TIMESTAMP_INICIO    1618949411
#define DURACION_SCANNING   30 //duracion del escaneo en segundos
#define CANT_DEVICES        5 //cantidad de dispositivos distintos que puede encontrar y guardar info simultaneamente.
#define MAX_CANT_RSSI       20 //cantidad maxima de rssi que se guardan para un mismo device remoto, si se llena el array y hay mas mediciones, estas ultimas se pierden.


/* Declare static functions */
static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);

typedef struct {
    uint64_t timestamp; //tiempo en el que se mide el rssi
    int8_t rssi; // valor de rssi
} time_rssi_t;

/**
 * @struct estructura que contiene los datos de un dispositivo encontrado
 */
typedef struct {
    uint8_t bdname_len;
    uint8_t bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1]; //nombre del dispositivo bluetooth
    esp_bd_addr_t bda; //MAC address
    int8_t txpower;
    char tipo_mac_addr[11];
    char tipo_BT[10];
    time_rssi_t rssi[MAX_CANT_RSSI]; //estructura que contiene varios valores de rssi medidos y su timestamp correspondiente.
} data_device_t;

/*
 * El cliente GATT normalmente escanea a sus vecinos e intenta conectarse a ellos, en caso de interes.
 * Sin embargo para realizar el escaneo, primero necesitamos establecer los parametros de configuracion.
 * Esto terminara luego del registro de las aplicaciones de perfiles, porque el registro 
 * una vez terminada, desencadena un evento ESP_GATTC_REG_EVT. 
 * La primera vez que se activa este evento, el controlador GATT lo captura y asigna la interfaz GATT al
 * perfil A, luego el evento lo reenvia al controlador de eventos GATT del perfil A.
 * En el controlador de eventos, el evento se usa para llamar la funcion esp_ble_gap_set_scan_params() 
 * que toma una instancia de la estructura ble_scan_params como parametro.
 * */
static esp_ble_scan_params_t ble_scan_params = {
    .scan_type              = BLE_SCAN_TYPE_ACTIVE,         //Tipo de escaneo - ACTIVO
    .own_addr_type          = BLE_ADDR_TYPE_PUBLIC,         //Tipo de direccion del propietario - DIRECCION PUBLICA
    .scan_filter_policy     = BLE_SCAN_FILTER_ALLOW_ALL,    //Politica de filtro de escaneo - ALL 
    .scan_interval          = 0x50,                         /*Intervalo de escaneo, esto se define como 
                                                             *el intervalo de tiempo desde que el controlador
                                                             *inicio su ultimo escaneo LE hasta que comienza
                                                              el siguiente escaneo LE - 100 ms (1.25 ms * 0x50) */
    .scan_window            = 0x30,                         /*Ventana de escaneo, la duracion del escaneo
                                                             *LE_Scan_Window debe ser menor o igual que 
                                                             *LE_Scan_Interval - 60 ms (1.25 ms * 0x30).*/
    .scan_duplicate         = BLE_SCAN_DUPLICATE_DISABLE
};

static data_device_t dev_info; //struct temporal que guarda los datos de 1 solo device
static data_device_t devices[CANT_DEVICES]; //arreglo de estructuras que contienen informacion, una por cada dispositivo.

/**
 * @brief Convierte la MAC address a desde un arreglo de bytes a formato String.
 * @return devuelve un puntero al string generado.
 */
static char *bda2str(esp_bd_addr_t bda, char *str, size_t size)
{
    if (bda == NULL || str == NULL || size < 18) {
        return NULL;
    }

    uint8_t *p = bda;
    sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
            p[0], p[1], p[2], p[3], p[4], p[5]);
    return str;
}

/**
 * @brief funcion que inicializa estructura de info de dispositivos a cero.
 */
void bt_struct_devices_init(void)
{
    memset(devices, 0, sizeof(data_device_t)*CANT_DEVICES);
    memset(&dev_info, 0, sizeof(data_device_t)); //inicializa estructura a cero
}

/**
 * @brief funcion que actualiza los datos de un dispositivo Bluetooth en el caso que ya haya sido registrado previamente.
 * Si se detecta por primera vez, se genera una estructura nueva para guardar sus datos.
 */
void actualizar_datos(data_device_t *nuevos_datos){
    static uint8_t index_write = 0; //index_write me indica la posicion del arreglo donde se debe escribir si se encuentra un nuevo device
    static uint8_t limite = 0; //limite me marca cuántos lugares del arreglo estan ocupados

    //si se alcanzó la capacidad maxima del buffer que guarda las mediciones, se empieza a sobreescribir en la primera posicion
    if(limite >= CANT_DEVICES){
        ESP_LOGW(OVERFLOW_TAG, "Se alcanzó la capacidad maxima del buffer, se sobreescribirán los datos");
        index_write = 0; //index_write me indica la posicion del arreglo donde se debe escribir
    }

    //ESP_LOGI(GATTC_TAG, "LIMITE: %d", limite);
    //ESP_LOGI(GATTC_TAG, "INDEX: %d", index_write);

    for(int i = 0; i < limite; i++){ //se compara solo con las posiciones que tenga algun dato (no vacias)
        if(memcmp(devices[i].bda, nuevos_datos->bda, ESP_BD_ADDR_LEN) == 0){ //se comparan las MAC addr, si son iguales se actualizan los datos
            devices[i].bdname_len = nuevos_datos->bdname_len;
            devices[i].txpower = nuevos_datos->txpower;
            memcpy(devices[i].bdname, nuevos_datos->bdname, nuevos_datos->bdname_len);
            strcpy(devices[i].tipo_mac_addr, nuevos_datos->tipo_mac_addr);
            strcpy(devices[i].tipo_BT, nuevos_datos->tipo_BT);

            for(uint8_t n = 0; n < MAX_CANT_RSSI; n++){
                if(devices[i].rssi[n].timestamp == 0){ //se busca un espacio vacio en el array de rssi.
                    devices[i].rssi[n].rssi = nuevos_datos->rssi[0].rssi;
                    devices[i].rssi[n].timestamp = nuevos_datos->rssi[0].timestamp;
                    break;
                }
            }
         /*   ESP_LOGI(GATTC_TAG, "ACTUALIZO");
            ESP_LOGI(GATTC_TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
            ESP_LOGI(GATTC_TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
            ESP_LOGI(GATTC_TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi); */
            
            return;
        }
    }
    //si llega aqui es porque no esta registrado el dispositivo
    memcpy(&devices[index_write], nuevos_datos, sizeof(data_device_t));
    limite++;
    index_write++;
    /*
    ESP_LOGI(GATTC_TAG, "GUARDO NUEVO");
    ESP_LOGI(GATTC_TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
    ESP_LOGI(GATTC_TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
    ESP_LOGI(GATTC_TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi);*/
    
}

/**
 * @brief Funcion de callback, se llama siempre que se encuentra un dispositivo, cuando se setean los parametros 
 * y cuando comienza y termina el tiempo de scanning.
 */
static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    uint8_t *adv_name = NULL;
    uint8_t adv_name_len = 0;
    uint8_t *adv_txpower = NULL;
    uint8_t adv_txpower_len = 0;
   // uint8_t *adv_dev_class = NULL;
   // uint8_t adv_dev_class_len = 0;
    char bda_str[18];

    data_device_t *p_dev = &dev_info;
    memset(p_dev, 0, sizeof(data_device_t)); //borra posibles datos anteriores en la struct

    switch (event) {
    /*
     * Una vez que se establecen los parametros de escaneo, se activa el evento ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT
     * que es manejado por el controlador de eventos GAP esp_gap_cb().
     * Este evento se utiliza para iniciar el escaneo de servidores GATT cercanos
     * 
     * El escaneo comienza usando la funcion esp_ble_gap_start_scanning() la cual toma los parametros 
     * representando la duracion del escaneo continuo en segundos. Una vez que finaliza el período de 
     * escaneo, se activa un evento ESP_GAP_SEARCH_INQ_CMPL_EVT.
    */
    case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT: {
        //the unit of the duration is second
       // uint32_t duration = 30;
        esp_ble_gap_start_scanning(DURACION_SCANNING);
        ESP_LOGI(GATTC_TAG, "BLE scanning start.");
        break;
    }
    case ESP_GAP_BLE_SCAN_START_COMPLETE_EVT:
        //scan start complete event to indicate scan start successfully or failed
        if (param->scan_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(GATTC_TAG, "scan start failed, error status = %x", param->scan_start_cmpl.status);
            break;
        }
        ESP_LOGI(GATTC_TAG, "scan start success");

        break;
    case ESP_GAP_BLE_SCAN_RESULT_EVT: {
        /*
         * Los resultados del escaneo se muestran tan pronto como llegan con el evento 
         * ESP_GAP_BLE_SCAN_RESULT_EVT, que incluye los siguientes parámetros:
         *
         *  struct ble_scan_result_evt_param {
                    esp_gap_search_evt_t search_evt;   ->Tipo de evento encontrado
                    esp_bd_addr_t bda;                 ->Direccion del disp BT
                    esp_bt_dev_type_t dev_type;        ->Tipo de dispositivo
                    esp_ble_addr_type_t ble_addr_type; ->Tipo de direccion de disp BT
                    esp_ble_evt_type_t ble_evt_type;   ->Tipo de evento de resultado de escaneo BLE
                    int rssi;                          ->RSSI
                    uint8_t  ble_adv[ESP_BLE_ADV_DATA_LEN_MAX + ESP_BLE_SCAN_RSP_DATA_LEN_MAX]; ->EIR recibido
                    int flag;                          -> Bit de flags de datos
                    int num_resps;                     -> Numero de resultados escaneados
                    uint8_t adv_data_len;              -> Adv data length 
                    uint8_t scan_rsp_len;              ->Scan response length
            } scan_rst; 
         *
         * Este evento tambien incluye una lista de subeventos.
         * Subeventos de ESP_GAP_BLE_SCAN_RESULT_EVT:
            typedef enum {
                ESP_GAP_SEARCH_INQ_RES_EVT             = 0,  Resultado de consulta por disp
                ESP_GAP_SEARCH_INQ_CMPL_EVT            = 1,  Resultado de consulta completa
                ESP_GAP_SEARCH_DISC_RES_EVT            = 2,  Resultado del descubrimiento para un dispositivo del mismo nivel
                ESP_GAP_SEARCH_DISC_BLE_RES_EVT        = 3,  Resultado del descubrimiento para el servicio basado en BLE GATT en un dispositivo similar
                ESP_GAP_SEARCH_DISC_CMPL_EVT           = 4,  Resultado del descubrimiento completo
                ESP_GAP_SEARCH_DI_DISC_CMPL_EVT        = 5,  Descubrimiento completo.
                ESP_GAP_SEARCH_SEARCH_CANCEL_CMPL_EVT  = 6,  Busqueda cancelada
            } esp_gap_search_evt_t;
         *
         * Estamos interesados ​​en el evento ESP_GAP_SEARCH_INQ_RES_EVT, que se llama cada vez 
         * que se encuentra un nuevo dispositivo. 
         * También estamos interesados ​​en el ESP_GAP_SEARCH_INQ_CMPL_EVT, 
         * que se activa cuando se completa la duración del escaneo 
         * y se puede usar para reiniciar el procedimiento de escaneo
         */
        esp_ble_gap_cb_param_t *scan_result = (esp_ble_gap_cb_param_t *)param;
        switch (scan_result->scan_rst.search_evt) {
        case ESP_GAP_SEARCH_INQ_RES_EVT:
             bda2str(scan_result->scan_rst.bda, bda_str, 18); //toma la MAC, la transforma a string y guarda en bda_str
             memcpy(p_dev->bda, scan_result->scan_rst.bda, ESP_BD_ADDR_LEN); //se obtiene la MAC address
            /*
             * Para obtener el nombre del dispositivo, utilizamos la función esp_ble_resolve_adv_data(), 
             * que toma los datos almacenados en scan_result-> scan_rst.ble_adv (es el EIR) 
             * el tipo de datos y la longitud, para extraer el valor del marco del paquete.
             * Luego se imprime el nombre del dispositivo.
            */
            //ble_adv es el EIR recibido
            adv_name = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv, ESP_BLE_AD_TYPE_NAME_CMPL, &adv_name_len);
            //se obtiene la potencia del dispositivo remoto emisor
            adv_txpower = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv, ESP_BLE_AD_TYPE_TX_PWR, &adv_txpower_len);

          //  adv_dev_class = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv, ESP_BLE_AD_MANUFACTURER_SPECIFIC_TYPE, &adv_dev_class_len);
             
           
            //se obtiene el tipo de MAC address recibida (publica o random)
            switch(scan_result->scan_rst.ble_addr_type){
            case BLE_ADDR_TYPE_PUBLIC:
                strcpy(p_dev->tipo_mac_addr, "PUBLICA");
                break;
            case BLE_ADDR_TYPE_RANDOM:
                strcpy(p_dev->tipo_mac_addr, "RANDOM");
                break;
            case BLE_ADDR_TYPE_RPA_PUBLIC:
                strcpy(p_dev->tipo_mac_addr, "RPA PUBLIC");
                break;
            case BLE_ADDR_TYPE_RPA_RANDOM:
            default:
                strcpy(p_dev->tipo_mac_addr, "RPA RANDOM");
                break;
            }

            //se obtiene el tipo de BT que tiene el device remoto
            switch(scan_result->scan_rst.dev_type){
            case ESP_BT_DEVICE_TYPE_BREDR:
                strcpy(p_dev->tipo_BT, "BR/EDR");
                break;
            case ESP_BT_DEVICE_TYPE_BLE:
                strcpy(p_dev->tipo_BT, "BLE");
                break;
            case ESP_BT_DEVICE_TYPE_DUMO:
            default:
                strcpy(p_dev->tipo_BT, "Dual Mode");
                break;
            }

            if(adv_name == NULL){
                strcpy((char *)p_dev->bdname, "-"); //si no se pudo encontrar el nombre
                p_dev->bdname_len = adv_name_len;
            }else{ 
                memcpy(p_dev->bdname, (uint8_t *)adv_name, adv_name_len);
                p_dev->bdname[adv_name_len] = '\0';
                p_dev->bdname_len = adv_name_len;
            }

            if(adv_txpower == NULL){
                p_dev->txpower = 0; //si no se pudo determinar la potencia del Tx entonces es 0 por defecto.
            }else{
                p_dev->txpower = (int8_t) *adv_txpower;
            }

            char txpower_str[5];
            if(p_dev->txpower == 0){
                sprintf(txpower_str, "%s", "-");
            }else{
                sprintf(txpower_str, "%d", p_dev->txpower);
            }

            p_dev->rssi[0].rssi = scan_result->scan_rst.rssi; //se guarda directamente en la posicion 0 total se actualiza al final de esta funcion.
            p_dev->rssi[0].timestamp = time(NULL) + TIMESTAMP_INICIO;

            ESP_LOGI(GATTC_TAG, "[%u] MAC: %s (%s) | Nombre: %s | Tipo BT: %s | TxPower: %s dBm | RSSI %d dBm", 
                    (unsigned) p_dev->rssi[0].timestamp, bda_str, p_dev->tipo_mac_addr, p_dev->bdname, p_dev->tipo_BT, txpower_str, p_dev->rssi[0].rssi);

            actualizar_datos(p_dev);  
            break;
        case ESP_GAP_SEARCH_INQ_CMPL_EVT:
            ESP_LOGI(GATTC_TAG, "BLE scanning stop.");
            //Una vez que finaliza el período de escaneo, se activa un evento ESP_GAP_SEARCH_INQ_CMPL_EVT.
            esp_ble_gap_start_scanning(DURACION_SCANNING); //se da comienzo a un nuevo escaneo
            break;
        default:
            break;
        }
        break;
    }

    case ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT:
        if (param->scan_stop_cmpl.status != ESP_BT_STATUS_SUCCESS){
            ESP_LOGE(GATTC_TAG, "scan stop failed, error status = %x", param->scan_stop_cmpl.status);
            break;
        }
        ESP_LOGI(GATTC_TAG, "stop scan successfully");
         ESP_LOGI(GATTC_TAG, "ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT -------------------------------------------------------");
        break;
/*
    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
        if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS){
            ESP_LOGE(GATTC_TAG, "adv stop failed, error status = %x", param->adv_stop_cmpl.status);
            break;
        }
        ESP_LOGI(GATTC_TAG, "stop adv successfully");
         ESP_LOGI(GATTC_TAG, "ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT -------------------------------------------------------");
        break;*/
    default:
        break;
    }
}

/**
 * @brief funcion main principal, es el punto de entrada del programa.
 */
void app_main(void)
{
    /* Initialize NVS.
    * La función principal comienza inicializando la biblioteca de almacenamiento no volátil. 
    * Esta biblioteca permite guardar pares clave-valor en la memoria flash y algunos componentes, 
    * como la biblioteca Wi-Fi, la utilizan para guardar el SSID y la contraseña:
    */
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );

    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT)); //libera memoria del controlador del BT Classic


    /*
    * Inicializacion del controlador BT, crea primero una estructura de conf de controlador BT 
    * llamada esp_bt_controller_config_t con la conf predeterminada generada por la macro 
    * BT_CONTROLLER_INIT_CONFIG_DEFAULT()
    * El controlador BT implementa la interfaz de controlador de host (HCI) en el lado del controlador,
    * la capa de enlace (LL) y la capa fisica (PHY).
    * El controlador BT es invisible para las aplicaciones del usuario y se ocupa de las capas inferiores
    * de la pila BLE.
    * La configuracion del controlador incluye la configuracion del tamaño de pila del controlador BT,
    * la prioridad y la velocidad en baudios de HCI. Con la configuracion creada, el controlador BT
    * se inicializa y habilita con la funcion esp_bt_controller_init()
    */
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT(); //config por defecto
    ret = esp_bt_controller_init(&bt_cfg);
    if (ret) {
        ESP_LOGE(GATTC_TAG, "%s initialize controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }
    /*
    * Siguiente, el controlador es habilitado en modo BLE
    * El controlador debe ser habilitado en ESP_BT_MODE_BTDM, si necesitas usar el dual mode (BLE + BT).
    * 
    * Existen cuatro modos de Bluetooth soportados:
    *   ESP_BT_MODE_IDLE: Bluetooth no corriendo
    *   ESP_BT_MODE_BLE: Modo BLE
    *   ESP_BT_MODE_CLASSIC_BT: Modo BL clasico
    *   ESP_BT_MODE_BTDM: Dual mode (BLE + BT Classic)
    */
    ret = esp_bt_controller_enable(ESP_BT_MODE_BLE);
    if (ret) {
        ESP_LOGE(GATTC_TAG, "%s enable controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    /*
    * Luego de la inicializacion del controlador BT, se inicializa el stack Bluedroid,
    * el cual incluye las definiciones y API comunes para BT clasico y BLE, se inicializa
    * y habilita por: 
    *   ret = esp_bluedroid_init();
    *   ret = esp_bluedroid_enable();
    */
    ret = esp_bluedroid_init();  //inicializa stack Bluedroid
    if (ret) {
        ESP_LOGE(GATTC_TAG, "%s init bluetooth failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    ret = esp_bluedroid_enable();  //habilita stack Bluedroid
    if (ret) {
        ESP_LOGE(GATTC_TAG, "%s enable bluetooth failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    /*
    * La función principal finaliza registrando los controladores de eventos GAP y GATT, 
    * así como el perfil de la aplicación y establece el tamaño máximo de MTU admitido.
    * 
    * Los manejadores de eventos GAP y GATT son las funciones que se utilizan para 
    * capturar los eventos generados por la pila BLE y ejecutar funciones para configurar 
    * los parámetros de la aplicación.
    * 
    * Además, los controladores de eventos también se utilizan para manejar eventos de 
    * lectura y escritura provenientes de la central. 
    * El controlador de eventos GAP se encarga de escanear y conectarse a los servidores y 
    * el controlador GATT administra los eventos que suceden después de que el cliente se ha 
    * conectado a un servidor, como la búsqueda de servicios y la escritura y lectura de datos.
    * 
    * Los controladores de eventos GAP y GATT se registran mediante:
    *   esp_ble_gap_register_callback();
    *   esp_ble_gattc_register_callback();
    * 
    * Las funciones esp_gap_cb() y esp_gattc_cb() maneja todos los eventos generados por el stack BLE.
    */
   
    //register the  callback function to the gap module
    ret = esp_ble_gap_register_callback(esp_gap_cb);
    if (ret){
        ESP_LOGE(GATTC_TAG, "%s gap register failed, error code = %x\n", __func__, ret);
        return;
    }

    //se configura el nombre del dispositivo Bluetooth
    char *dev_name = "ESP32 BLE";
    esp_ble_gap_set_device_name(dev_name);

    //se imprime la direccion MAC y el tipo
  /*  esp_bd_addr_t local_mac_addr; 
    uint8_t *tipo_addr = BLE_ADDR_TYPE_PUBLIC;
    //uint8_t *tipo_addr = BLE_ADDR_TYPE_RANDOM;
    char bda_str[18]; 
    if(esp_ble_gap_get_local_used_addr(local_mac_addr, tipo_addr) == ESP_OK){
       ESP_LOGI(GATTC_TAG, "MAC ADDRESS: %s, TIPO: ", bda2str(local_mac_addr, bda_str, 18)); //convierte MAC a formato string e imprime
    }*/

    //obtiene mac address PÚBLICA de la placa ESP32 y la imprime.
    const uint8_t * bdaddr_esp32;
    bdaddr_esp32 = esp_bt_dev_get_address();
    if(bdaddr_esp32 != NULL){
        ESP_LOGI(GATTC_TAG, "ESP32 PUBLIC MAC ADDRESS: %x:%x:%x:%x:%x:%x", 
                 bdaddr_esp32[0],bdaddr_esp32[1],bdaddr_esp32[2],
                 bdaddr_esp32[3],bdaddr_esp32[4],bdaddr_esp32[5]);
    }

    bt_struct_devices_init(); //inicializa estructura a cero

    //Los valores de escaneo se establecen mediante la funcion esp_ble_gap_set_scan_params()
    esp_err_t scan_ret = esp_ble_gap_set_scan_params(&ble_scan_params);
    if (scan_ret){
        ESP_LOGE(GATTC_TAG, "set scan params error, error code = %x", scan_ret);
    }
}