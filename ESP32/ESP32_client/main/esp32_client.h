/**
 * @file bluetooth_discovery.c
 * @brief 
 * @author Alejandro Ferrero y Jeremías Agustinoy
 * @version 1.0
 * @date 26/04/2021
 */ 
#ifndef ESP_CLIENT_H
#define ESP_CLIENT_H

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_bt.h"
#include "esp_bt_main.h"        //Inicializa y habilita el stack Bluedroid 
#include "esp_bt_device.h"
#include "esp_gap_bt_api.h"     //CLASSIC BLUETOOTH GAP API

#include <stdbool.h>
#include <stdio.h>
#include "esp_gap_ble_api.h"        //Implementa la conf GAP, como parametros de conexion
#include "esp_gattc_api.h"          //Implementa la conf de cliente GATT, como conexion de perifericos y busqueda de servicios
#include "esp_gatt_defs.h"
#include "esp_gatt_common_api.h"

#include <sys/param.h>
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"
#include "addr_from_stdin.h"
#include "lwip/err.h"
#include "lwip/sockets.h"

#include "freertos/semphr.h"

#define DEV_NAME            "ESP32 - Moe" //Nombre del dispositivo bluetooth ESP32
//#define DEV_NAME            "ESP32 - Larry"
//#define DEV_NAME            "ESP32 - Curly"

#define TAG                 ""
#define OVERFLOW_TAG        "OVERFLOW"

//#define DURACION_INQUIRY    ESP_BT_GAP_MAX_INQ_LEN  //Maximum inquiry duration, unit is 1.28s. El minimo es ESP_BT_GAP_MIN_INQ_LEN
#define DURACION_INQUIRY    10 //para BT Classic
#define DURACION_SCANNING   30 //duracion del escaneo en segundos para BLE
#define CANT_DEVICES        10 //cantidad de dispositivos distintos que puede encontrar y guardar info simultaneamente.
#define MAX_CANT_RSSI       20 //cantidad maxima de rssi que se guardan para un mismo device remoto, si se llena el array y hay mas mediciones, estas ultimas se pierden.

#define INTENTO_CONEXION    3
#define MSEG_DELAY          3000 //mseg de delay en el chequeo de envio de datos por socket

#if defined(CONFIG_EXAMPLE_IPV4)
#define HOST_IP_ADDR CONFIG_EXAMPLE_IPV4_ADDR
#elif defined(CONFIG_EXAMPLE_IPV6)
#define HOST_IP_ADDR CONFIG_EXAMPLE_IPV6_ADDR
#else
#define HOST_IP_ADDR ""
#endif

#define PORT CONFIG_EXAMPLE_PORT

typedef uint8_t uuid_t[16];

typedef struct __attribute__((packed)) {
    uint64_t timestamp; //tiempo en el que se mide el rssi
    int8_t rssi; // valor de rssi
} time_rssi_t;

/**
 * @struct estructura que contiene los datos de un dispositivo encontrado
 */
typedef struct __attribute__((packed)) {
    uint8_t bdname_len;
    //uint8_t eir_len;
    //time_rssi_t rssi[MAX_CANT_RSSI]; //estructura que contiene varios valores de rssi medidos y su timestamp correspondiente.
    time_rssi_t rssi; //estructura que contiene un valor de rssi medido y su timestamp correspondiente.
    uint32_t cod; //codigo de dispositivo
    //uint8_t eir[ESP_BT_GAP_EIR_DATA_LEN];
    uint8_t bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1]; //nombre del dispositivo bluetooth
    esp_bd_addr_t bda; //MAC address
    char bda_str[18]; //MAC addr en formato string
    char major_device_class[60]; //tipo de dispositivo BT
    int8_t txpower;
} classic_device_t;

/**
 * @struct estructura que contiene los datos de un dispositivo encontrado
 */
typedef struct __attribute__((packed)) {
    uint8_t bdname_len;
    uint8_t bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1]; //nombre del dispositivo bluetooth
    esp_bd_addr_t bda; //MAC address
    char bda_str[18]; //MAC addr en formato string
    int8_t txpower;
    char tipo_mac_addr[11];
    char tipo_BT[10];
    //time_rssi_t rssi[MAX_CANT_RSSI]; //estructura que contiene varios valores de rssi medidos y su timestamp correspondiente.
    time_rssi_t rssi; //estructura que contiene un valor de rssi medido y su timestamp correspondiente.
    char num_celular[11];
} ble_device_t;

/**
 * @brief estructura que contiene datos de un dispositivo BLE o de un dispositivo BT Classic.
 * Dependiendo del tipo de Bluetooth que se detectó, los campos de la struct que corresponden a la
 * clase contraria se establecen en cero o como un string vacío. 
 * Se hace una única estructura para que sea mas simple la transferencia por sockets al servidor y
 * su posterior decodificacion.
 */
typedef struct __attribute__((packed)) {
    uint8_t bdname_len;
    uint8_t bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1]; //nombre del dispositivo bluetooth
    esp_bd_addr_t bda; //MAC address
    char bda_str[18]; //MAC addr en formato string
    time_rssi_t rssi[MAX_CANT_RSSI]; //estructura que contiene varios valores de rssi medidos y su timestamp correspondiente.
    int8_t txpower;

    // exclusivo de BT Classic
    uint32_t cod; //codigo de dispositivo
    char major_device_class[60]; //tipo de dispositivo BT

    // exclusivo de BLE
    char tipo_mac_addr[11];
    char tipo_BT[10];
    char num_celular[11];

} data_device_t;

/**
 * Estructura con los parámetros del advertising.
 * Para poder ser detectado por los smartphones cercanos.
 */
static esp_ble_adv_params_t adv_params = {
    .adv_int_min        = 0x20,         //Minimum advertising interval for undirected and low duty cycle directed advertising. 
    .adv_int_max        = 0x40,         //Maximum advertising interval (Range: 0x0020 to 0x4000)
    .adv_type           = ADV_TYPE_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC, //muestra MAC pública del ESP32
    //.peer_addr            =
    //.peer_addr_type       =
    .channel_map        = ADV_CHNL_ALL, //todos los canales
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY, //scan and connection requests from anyone.
};

/*
 * El cliente GATT normalmente escanea a sus vecinos e intenta conectarse a ellos, en caso de interes.
 * Sin embargo para realizar el escaneo, primero necesitamos establecer los parametros de configuracion del escaneo.
 */
static esp_ble_scan_params_t ble_scan_params = {
    .scan_type              = BLE_SCAN_TYPE_ACTIVE,         //Tipo de escaneo - ACTIVO
    .own_addr_type          = BLE_ADDR_TYPE_PUBLIC,         //Tipo de direccion del propietario - DIRECCION PUBLICA
    .scan_filter_policy     = BLE_SCAN_FILTER_ALLOW_ALL,    //Politica de filtro de escaneo - ALL 
    .scan_interval          = 0x50,                         /*Intervalo de escaneo, esto se define como 
                                                             *el intervalo de tiempo desde que el controlador
                                                             *inicio su ultimo escaneo LE hasta que comienza
                                                              el siguiente escaneo LE - 100 ms (1.25 ms * 0x50) */
    .scan_window            = 0x30,                         /*Ventana de escaneo, la duracion del escaneo
                                                             *LE_Scan_Window debe ser menor o igual que 
                                                             *LE_Scan_Interval - 60 ms (1.25 ms * 0x30).*/
    .scan_duplicate         = BLE_SCAN_DUPLICATE_DISABLE
};

static classic_device_t dev_classic_info; //struct temporal que guarda los datos de 1 solo device de tipo BT Classic
static ble_device_t dev_ble_info; //struct temporal que guarda los datos de 1 solo device de tipo BLE
static data_device_t devices[CANT_DEVICES]; //arreglo de estructuras que contienen informacion, una por cada dispositivo.
static uint8_t index_write = 0; //index_write me indica la posicion del arreglo donde se debe escribir si se encuentra un nuevo device
static uint8_t limite = 0; //limite me marca cuántos lugares del arreglo estan ocupados

SemaphoreHandle_t mutex_devices = NULL;
SemaphoreHandle_t mutex_index_write = NULL;
SemaphoreHandle_t mutex_limite = NULL;

static const data_device_t *payload = devices;
static uint64_t timestamp_inicial = 0;

void crear_mutex(void);
void set_limite(uint8_t new_limite);
void set_index_write(uint8_t new_index);
static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);
static char *bda2str(esp_bd_addr_t bda, char *str, size_t size);
static bool get_name_from_eir(uint8_t *eir, uint8_t *bdname, uint8_t *bdname_len);
static bool get_tx_power_from_eir(uint8_t *eir, int8_t *txpower, uint8_t *txpower_len);
void actualizar_datos_classic(classic_device_t *nuevos_datos);
void actualizar_datos_ble(ble_device_t *nuevos_datos);
static void update_device_info(esp_bt_gap_cb_param_t *param);
void limpiar_buffer(void);
bool check_envio(void);
void bt_app_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param);
void bt_classic_ble_config(void);
static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);
static char *uuid2str(uuid_t uuid, char *str, size_t size);
static char *get_celular_from_uuid(uuid_t uuid, char *str, size_t size);
static bool is_phone_number(uuid_t uuid);

void print_devices_registrados(void);

/**
 * La tarea que maneja las funciones: esp_gap_cb(), bt_app_gap_cb(), update_device_info() que son para el manejo del
 * Bluetooth Classic y BLE es la misma tarea y se llama BTC_TASK. 
 * Es decir, que hay una misma tarea que maneja tanto el BT Classic como el BLE y es distinta de la tarea 'main' y
 * de la tarea tcp_client que se encarga del socket cliente.
 */

#endif //ESP_CLIENT_H