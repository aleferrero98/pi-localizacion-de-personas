/**
 * @file bluetooth_discovery.c
 * @brief 
 * @author Alejandro Ferrero y Jeremías Agustinoy
 * @version 1.0
 * @date 26/04/2021
 */ 

#include "esp32_client.h"

uint64_t cant_mediciones = 0;
/* Stores the handle of the task that will be notified when the
wifi setup is complete. */
static TaskHandle_t xTaskToNotify = NULL;

/**
 * @brief establece un nuevo valor para la variable global limite.
 * @param new_limite nuevo valor de limite
 */
void set_limite(uint8_t new_limite){
    xSemaphoreTake(mutex_limite, portMAX_DELAY); //espera indefinidamente a que este disponible
    limite = new_limite; 
    xSemaphoreGive(mutex_limite); 
}

/**
 * @brief establece un nuevo valor para la variable global index_write.
 * @param new_index nuevo valor de index_write
 */
void set_index_write(uint8_t new_index){
    xSemaphoreTake(mutex_index_write, portMAX_DELAY); //espera indefinidamente a que este disponible
    index_write = new_index; 
    xSemaphoreGive(mutex_index_write); 
}

/**
 * @brief Crea todos los mutex que se usan para acceso exclusivo a distintas variables y estructuras en el codigo.
 */
void crear_mutex(void){
    mutex_devices = xSemaphoreCreateMutex();
    mutex_index_write = xSemaphoreCreateMutex();
    mutex_limite = xSemaphoreCreateMutex();

    //se chequea que todos los mutex se han podido crear correctamente
    if(mutex_devices == NULL || mutex_index_write == NULL || mutex_limite == NULL){
        ESP_LOGE(TAG, "MUTEX no pudo ser creado");
        return;
    }
}

/**
 * @brief tarea que maneja la comunicacion por sockets WiFi.
 */
static void tcp_client_task(void *pvParameters)
{
    char rx_buffer[100]; //buffer de recepcion
    char host_ip[] = HOST_IP_ADDR;
    int addr_family = 0;
    int ip_protocol = 0;
    int err;
    int len;

    //ESP_LOGI(TAG, "task tcp_client_task(): %s", pcTaskGetTaskName(NULL));

    while (1) { 
#if defined(CONFIG_EXAMPLE_IPV4)
        struct sockaddr_in dest_addr;
        dest_addr.sin_addr.s_addr = inet_addr(host_ip);
        dest_addr.sin_family = AF_INET;
        dest_addr.sin_port = htons(PORT);
        addr_family = AF_INET;
        ip_protocol = IPPROTO_IP;
#elif defined(CONFIG_EXAMPLE_IPV6)
        struct sockaddr_in6 dest_addr = { 0 };
        inet6_aton(host_ip, &dest_addr.sin6_addr);
        dest_addr.sin6_family = AF_INET6;
        dest_addr.sin6_port = htons(PORT);
        dest_addr.sin6_scope_id = esp_netif_get_netif_impl_index(EXAMPLE_INTERFACE);
        addr_family = AF_INET6;
        ip_protocol = IPPROTO_IPV6;
#elif defined(CONFIG_EXAMPLE_SOCKET_IP_INPUT_STDIN)
        struct sockaddr_storage dest_addr = { 0 };
        ESP_ERROR_CHECK(get_addr_from_stdin(PORT, SOCK_STREAM, &ip_protocol, &addr_family, &dest_addr));
#endif
        //crea el socket
        int sock =  socket(addr_family, SOCK_STREAM, ip_protocol);
        if (sock < 0) {
            ESP_LOGE(TAG, "No se pudo crear el socket: errno %d", errno);
            break;
        }
        ESP_LOGI(TAG, "Socket creado correctamente, conectandose a %s:%d", host_ip, PORT);

        //se conecta con el servidor
        err = connect(sock, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_in6));
        if (err != 0) {
            ESP_LOGW(TAG, "Socket no se pudo conectar en el primer intento: errno %d", errno);
            //intenta algunas veces mas conectarse y si no tuvo exito finaliza la task
            for(int i = 0; i < INTENTO_CONEXION; i++)
            {
                ESP_LOGW(TAG, "Reintentando conexion, intento %d/%d", i+1, INTENTO_CONEXION);
                vTaskDelay(3000 / portTICK_PERIOD_MS); //espera 3 seg
                sock =  socket(addr_family, SOCK_STREAM, ip_protocol);
                err = connect(sock, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_in6));
                if (err == 0){break;} //no hay errores, salgo del for
            }
            if(err != 0){ //si se terminaron las iteraciones y siguen los errores, finalizo el socket
                ESP_LOGE(TAG, "Socket no se pudo conectar: errno %d", errno);
                break;
            } 
        }
        ESP_LOGI(TAG, "Conexion exitosa");
        /* Send a notification to main task, bringing it out of the Blocked state. */
        xTaskNotifyGive(xTaskToNotify);
        xTaskToNotify = NULL;

        //ENVIO NOMBRE ESP32 (1 VEZ)
        char *dev_name = DEV_NAME;
        err = send(sock, dev_name, sizeof(DEV_NAME), 0);

        if (err < 0) {
            ESP_LOGE(TAG, "Error ocurrido durante el envio: errno %d", errno);
            break;
        }else{
            ESP_LOGI(TAG, "Envió %d bytes", sizeof(DEV_NAME));
        }

        //RECEPCION TIMESTAMP INICIAL (1 VEZ)
        len = recv(sock, rx_buffer, sizeof(rx_buffer), 0); 
        if (len < 0) {
            // Error ocurrido durante la recepcion
            ESP_LOGE(TAG, "recv timestamp fallo: errno %d", errno);
            break;
        } else {
            // Dato recibido
            rx_buffer[len] = 0; // Termina el buffer en nulo para tratarlo como cadena y elimina el \n
            timestamp_inicial = atoi(rx_buffer);
        
            ESP_LOGI(TAG, "Recibio %d bytes desde %s:", len, host_ip);
            ESP_LOGI(TAG, "TIMESTAMP INICIAL: %lu", (unsigned long)timestamp_inicial);
        }

        //COMIENZA ENVIO PERIODICO DE DATOS AL SERVIDOR
        while (1) {
            while(!check_envio()){
                vTaskDelay(MSEG_DELAY / portTICK_PERIOD_MS);
            }
            //se envian todos los que son distinto de cero
            //*************VER BIEN ESTO, SI SE ENVIAN TODOS JUNTOS O QUEEEEE*******************//
            xSemaphoreTake(mutex_limite, portMAX_DELAY);
            xSemaphoreTake(mutex_devices, portMAX_DELAY); //payload apunta a devices
            err = send(sock, payload, sizeof(data_device_t)*limite, 0);
            xSemaphoreGive(mutex_devices);
            xSemaphoreGive(mutex_limite);
            if (err < 0) {
                ESP_LOGE(TAG, "Error ocurrido durante el envio: errno %d", errno);
                break;
            }else{
                ESP_LOGI(TAG, "Envió %d bytes", sizeof(data_device_t)*limite);
                printf("CANTIDAD MEDICIONES: %llu \n", cant_mediciones);
            }

            limpiar_buffer(); //borra el buffer porque ya envio los datos

            len = recv(sock, rx_buffer, sizeof(rx_buffer) - 1, 0); //recibo ACK del servidor
            if (len < 0) {
                // Error ocurrido durante la recepcion
                ESP_LOGE(TAG, "recv fallo: errno %d", errno);
                break;
            }
            else {
                // Dato recibido
                rx_buffer[len] = 0; // Termina el buffer en nulo para tratarlo como cadena y elimina el \n
                //rx_buffer[len] = '\0'; // Termina el buffer en nulo para tratarlo como cadena
                ESP_LOGI(TAG, "Recibio %d bytes desde %s: %s", len, host_ip, rx_buffer);
                //ESP_LOGI(TAG, "%s", rx_buffer);
            }

            if(strcmp(rx_buffer, "FIN") == 0){ //cierra la conexion al recibir un "FIN"
                ESP_LOGW(TAG, "APAGANDO SOCKET CLIENTE");
                shutdown(sock, 0);
                close(sock);
                break;
            }
        }

        if(strcmp(rx_buffer, "FIN") == 0){ 
            break; //sale del bucle externo tambien
        }

        if (sock != -1) { //en caso de algun error en el cliente, cierra el socket y lo reinicia
            ESP_LOGE(TAG, "Apagando socket y reiniciandolo");
            shutdown(sock, 0);
            close(sock);
        }

    }
    vTaskDelete(NULL);
}

/**
 * @brief chequea si hay datos en el buffer para enviar.
 * si el buffer esta vacio, no envía.
 * si el buffer esta lleno, envía.
 * si el buffer tiene datos (pero no esta lleno) envia periodicamente.
 * @return false si no se debe enviar, true si se debe enviar.
 */
bool check_envio(void){
    xSemaphoreTake(mutex_limite, portMAX_DELAY);  
     
    if(limite == CANT_DEVICES){ //buffer lleno
        xSemaphoreGive(mutex_limite);
        return true;
    }else if(limite == 0){ //buffer vacio
        xSemaphoreGive(mutex_limite);
        return false;
    }else{
        xSemaphoreGive(mutex_limite);
        return true;
    }
}

/**
 * @brief imprime la cantidad de dispositivos descubiertos junto con su MAC address y su nombre (si lo detecta).
 */
void print_devices_registrados(void){
    //ESP_LOGI(TAG, "task en print_devices_registrados(): %s", pcTaskGetTaskName(NULL));

    printf("\n-------------------- RESUMEN DISPOSITIVOS ENCONTRADOS -----------------------\n");
    xSemaphoreTake(mutex_limite, portMAX_DELAY);
    printf("Se han registrado %d dispositivos distintos\n", limite);

    xSemaphoreTake(mutex_devices, portMAX_DELAY);
    for(int i = 0; i < limite; i++){ //se busca solo las posiciones que tenga algun dato (no vacias)
        if(devices[i].cod != 0){ //si el cod es 0 es BLE
            //bda2str(devices[i].classic_data.bda, bda_str, 18); 
            printf("MAC: %s | Name: %s | Classic\n", devices[i].bda_str, devices[i].bdname);
        }else{
            //bda2str(devices[i].ble_data.bda, bda_str, 18); 
            printf("MAC: %s | Name: %s | BLE\n", devices[i].bda_str, devices[i].bdname);
        }
    }
    xSemaphoreGive(mutex_devices); 
    xSemaphoreGive(mutex_limite);
    printf("-----------------------------------------------------------------------------\n\n");
}

/**
 * @brief Convierte la MAC address a desde un arreglo de bytes a formato String.
 * @return devuelve un puntero al string generado.
 */
static char *bda2str(esp_bd_addr_t bda, char *str, size_t size)
{
    if (bda == NULL || str == NULL || size < 18) {
        return NULL;
    }

    uint8_t *p = bda;
    sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
            p[0], p[1], p[2], p[3], p[4], p[5]);
    return str;
}

/**
 * @brief tranforma los 128 bits del UUID en string.
 */
static char *uuid2str(uuid_t uuid, char *str, size_t size){
    if(uuid == NULL || str == NULL || size < 37) { //el uuid son 36 caracteres (contando los guiones)
        return NULL;
    }

    uint8_t *p = uuid;
    sprintf(str, "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x",
            p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9], p[10], 
            p[11], p[12], p[13], p[14], p[15]);
    return str;
}

/**
 * @brief Obtiene del numero de celular del UUID (10 primeros bytes hexadecimales)
 */
static char *get_celular_from_uuid(uuid_t uuid, char *str, size_t size){
    if(uuid == NULL || str == NULL || size < 11) { //debe ser de 10 digitos el num de celular
        return NULL;
    }

    uint8_t *p = uuid;
    sprintf(str, "%02x%02x%02x%02x%02x",
            p[0], p[1], p[2], p[3], p[4]);
    return str;
}

/**
 * @brief Chequea si hay un nro de telefono en el UUID.
 * Identifica que hay un nro de celular codificado en el UUID ya que los 10 primeros digitos corresponden al nro,
 * y el resto son todas 'a'.
 * @returns true si hay un nro de telefono, false en caso contrario.
 */
static bool is_phone_number(uuid_t uuid){
    uint8_t *p = uuid;
    p = p + 5; //salteo el numero (10 digitos -> 5 bytes)
    uint8_t resto[11] = {0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa}; //el resto deben ser 22 'a'.

    if(memcmp(p, resto, 11) == 0){
        return true;
    }
    return false;
}

/**
 * @brief funcion que inicializa estructura de info de dispositivos a cero.
 */
void limpiar_buffer(void)
{
    //ESP_LOGI(TAG, "task en limpiar_buffer(): %s", pcTaskGetTaskName(NULL));
    xSemaphoreTake(mutex_devices, portMAX_DELAY);
    memset(devices, 0, sizeof(data_device_t)*CANT_DEVICES);
    xSemaphoreGive(mutex_devices); 
    set_index_write(0); //se establece posicion del arreglo a escribir a cero
    set_limite(0); //array vacio

    //printf("tam data_device_t: %d\n", sizeof(data_device_t));
    //printf("tam time_rssi_t: %d\n", sizeof(time_rssi_t));
}

/**
 * EIR(Extended Inquiry Response) provides information about discoverable devices during a Bluetooth Inquiry.
 * The Inquiry requests all listening ("discoverable") Bluetooth devices to identify themselves.
 * The Inquiring Device then has to issue a request to each device to determine the local name and any services that each device can support.
 * The Extended Inquiry Response (EIR) replaces the standard Inquiry Response and contains additional information.
 * Legacy devices are supported so the set of responses from all devices may include both EIR and standard Inquiry Responses.
 * The EIR data packet is a fixed size so the number of characters available for the device local name may be insufficient for the entire device local name. 
 * 
 */
static bool get_name_from_eir(uint8_t *eir, uint8_t *bdname, uint8_t *bdname_len)
{
    uint8_t *rmt_bdname = NULL;
    uint8_t rmt_bdname_len = 0;

    if (!eir) {
        return false;
    }

    rmt_bdname = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_CMPL_LOCAL_NAME, &rmt_bdname_len);
    if (!rmt_bdname) {
        rmt_bdname = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_SHORT_LOCAL_NAME, &rmt_bdname_len);
    }

    if (rmt_bdname) {
        if (rmt_bdname_len > ESP_BT_GAP_MAX_BDNAME_LEN) {
            rmt_bdname_len = ESP_BT_GAP_MAX_BDNAME_LEN;
        }

        if (bdname) {
            memcpy(bdname, rmt_bdname, rmt_bdname_len);
            bdname[rmt_bdname_len] = '\0';
        }
        if (bdname_len) {
            *bdname_len = rmt_bdname_len;
        }
        return true;
    }

    return false;
}

/**
 * @brief obtiene el TX power del dispositivo remoto desde el paquete EIR.
 */
static bool get_tx_power_from_eir(uint8_t *eir, int8_t *txpower, uint8_t *txpower_len)
{
    uint8_t *rmt_txpower = NULL;
    uint8_t rmt_txpower_len = 0;

    if (!eir) {
        return false;
    }

    //Tx power level, value is 1 octet ranging from -127 to 127, unit is dBm
    rmt_txpower = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_TX_POWER_LEVEL, &rmt_txpower_len); 
    if (rmt_txpower == NULL) { 
        return false; //si no fue encontrado, esp_bt_gap_resolve_eir_data() retorna NULL
    }

    if (rmt_txpower) {
        if (txpower) {
            memcpy(txpower, rmt_txpower, rmt_txpower_len);
            txpower[rmt_txpower_len] = '\0';
        }
        if (txpower_len) {
            *txpower_len = rmt_txpower_len;
        }

        return true;
    }

    return false;
}

/**
 * @brief funcion que actualiza los datos de un dispositivo Bluetooth en el caso que ya haya sido registrado previamente.
 * Si se detecta por primera vez, se genera una estructura nueva para guardar sus datos.
 */
void actualizar_datos_classic(classic_device_t *nuevos_datos){

    cant_mediciones++;

    //si se alcanzó la capacidad maxima del buffer que guarda las mediciones, se empieza a sobreescribir en la primera posicion
    xSemaphoreTake(mutex_limite, portMAX_DELAY);
    if(limite >= CANT_DEVICES){
        xSemaphoreGive(mutex_limite);
        ESP_LOGW(OVERFLOW_TAG, "Se alcanzó la capacidad maxima del buffer, se sobreescribirán los datos");
        set_index_write(0); //index_write me indica la posicion del arreglo donde se debe escribir
    }else{
        xSemaphoreGive(mutex_limite);
    }

    xSemaphoreTake(mutex_limite, portMAX_DELAY);
    xSemaphoreTake(mutex_devices, portMAX_DELAY);
    for(int i = 0; i < limite; i++){ //se compara solo con las posiciones que tenga algun dato (no vacias)
        if(memcmp(devices[i].bda, nuevos_datos->bda, ESP_BD_ADDR_LEN) == 0){ //se comparan las MAC addr, si son iguales se actualizan los datos
            if(nuevos_datos->bdname_len != 0){ //el nombre se actualiza solo si es distinto de cero
                devices[i].bdname_len = nuevos_datos->bdname_len;
                memcpy(devices[i].bdname, nuevos_datos->bdname, nuevos_datos->bdname_len);
            }
            devices[i].cod = nuevos_datos->cod;
            strcpy(devices[i].major_device_class, nuevos_datos->major_device_class);
            //strcpy(devices[i].bda_str, nuevos_datos->bda_str);
            devices[i].txpower = nuevos_datos->txpower;

            for(uint8_t n = 0; n < MAX_CANT_RSSI; n++){
                if(devices[i].rssi[n].timestamp == 0){ //se busca un espacio vacio en el array de rssi.
                    devices[i].rssi[n].rssi = nuevos_datos->rssi.rssi;
                    devices[i].rssi[n].timestamp = nuevos_datos->rssi.timestamp;
                    break;
                }
            }
          /*  ESP_LOGI(TAG, "ACTUALIZO");
            ESP_LOGI(TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
            ESP_LOGI(TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
            ESP_LOGI(TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi); */
            xSemaphoreGive(mutex_devices); 
            xSemaphoreGive(mutex_limite);
            return;
        }
    }
    limite++;
    xSemaphoreGive(mutex_limite);

    //si llega aqui es porque no esta registrado el dispositivo
    xSemaphoreTake(mutex_index_write, portMAX_DELAY);
    //Se agrega un device nuevo a la lista
    memcpy(devices[index_write].bda, nuevos_datos->bda, ESP_BD_ADDR_LEN);                 //MAC addr
    devices[index_write].bdname_len = nuevos_datos->bdname_len;                           //length name
    strcpy(devices[index_write].bda_str, nuevos_datos->bda_str);                          //MAC addr como string
    devices[index_write].cod = nuevos_datos->cod;                                         //CoD
    strcpy(devices[index_write].major_device_class, nuevos_datos->major_device_class);    //Major device class
    devices[index_write].txpower = nuevos_datos->txpower;                                 //Tx Power
    memcpy(devices[index_write].bdname, nuevos_datos->bdname, nuevos_datos->bdname_len);  //name
    devices[index_write].rssi[0].rssi = nuevos_datos->rssi.rssi;                          //RSSI
    devices[index_write].rssi[0].timestamp = nuevos_datos->rssi.timestamp;                //timestamp
    //los datos de BLE van null
    strcpy(devices[index_write].tipo_mac_addr, "");                                       //tipo MAC addr
    strcpy(devices[index_write].tipo_BT, "");                                             //tipo BT
    strcpy(devices[index_write].num_celular, "");                                         //Nro de celular

    xSemaphoreGive(mutex_devices); 
    index_write++;
    xSemaphoreGive(mutex_index_write);

   /* xSemaphoreTake(mutex_limite, portMAX_DELAY);
    limite++;
    xSemaphoreGive(mutex_limite); */
   /* xSemaphoreTake(mutex_index_write, portMAX_DELAY);
    index_write++;
    xSemaphoreGive(mutex_index_write); */

/*  ESP_LOGI(TAG, "GUARDO NUEVO");
    ESP_LOGI(TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
    ESP_LOGI(TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
    ESP_LOGI(TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi);*/
    
}

/**
 * @brief funcion que actualiza los datos de un dispositivo Bluetooth en el caso que ya haya sido registrado previamente.
 * Si se detecta por primera vez, se genera una estructura nueva para guardar sus datos.
 */
void actualizar_datos_ble(ble_device_t *nuevos_datos){

    cant_mediciones++;

    //si se alcanzó la capacidad maxima del buffer que guarda las mediciones, se empieza a sobreescribir en la primera posicion
    xSemaphoreTake(mutex_limite, portMAX_DELAY);
    if(limite >= CANT_DEVICES){
        xSemaphoreGive(mutex_limite);
        ESP_LOGW(OVERFLOW_TAG, "Se alcanzó la capacidad maxima del buffer, se sobreescribirán los datos");
        set_index_write(0); //index_write me indica la posicion del arreglo donde se debe escribir
    }else{
        xSemaphoreGive(mutex_limite);
    }

    xSemaphoreTake(mutex_limite, portMAX_DELAY);
    xSemaphoreTake(mutex_devices, portMAX_DELAY); 
    for(int i = 0; i < limite; i++){ //se compara solo con las posiciones que tenga algun dato (no vacias)
        if(memcmp(devices[i].bda, nuevos_datos->bda, ESP_BD_ADDR_LEN) == 0){ //se comparan las MAC addr, si son iguales se actualizan los datos
            if(nuevos_datos->bdname_len != 0){ //el nombre se actualiza solo si es distinto de cero
                devices[i].bdname_len = nuevos_datos->bdname_len;
                memcpy(devices[i].bdname, nuevos_datos->bdname, nuevos_datos->bdname_len);
            }
            devices[i].txpower = nuevos_datos->txpower;
            strcpy(devices[i].tipo_mac_addr, nuevos_datos->tipo_mac_addr);
            strcpy(devices[i].tipo_BT, nuevos_datos->tipo_BT);
            strcpy(devices[i].num_celular, nuevos_datos->num_celular);
            //strcpy(devices[i].bda_str, nuevos_datos->bda_str);

            for(uint8_t n = 0; n < MAX_CANT_RSSI; n++){
                if(devices[i].rssi[n].timestamp == 0){ //se busca un espacio vacio en el array de rssi.
                    devices[i].rssi[n].rssi = nuevos_datos->rssi.rssi;
                    devices[i].rssi[n].timestamp = nuevos_datos->rssi.timestamp;
                    break;
                }
            }
         /*   ESP_LOGI(TAG, "ACTUALIZO");
            ESP_LOGI(TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
            ESP_LOGI(TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
            ESP_LOGI(TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi); */
            xSemaphoreGive(mutex_devices);
            xSemaphoreGive(mutex_limite);
            return;
        }
    }
    limite++;
    xSemaphoreGive(mutex_limite);

    //si llega aqui es porque no esta registrado el dispositivo
    xSemaphoreTake(mutex_index_write, portMAX_DELAY);
    //se agrega device nuevo
    //memcpy(&devices[index_write].ble_data, nuevos_datos, sizeof(ble_device_t));
    memcpy(devices[index_write].bda, nuevos_datos->bda, ESP_BD_ADDR_LEN);                   //MAC addr
    devices[index_write].bdname_len = nuevos_datos->bdname_len;                             //length name
    devices[index_write].txpower = nuevos_datos->txpower;                                   //Tx Power
    memcpy(devices[index_write].bdname, nuevos_datos->bdname, nuevos_datos->bdname_len);    //name
    strcpy(devices[index_write].tipo_mac_addr, nuevos_datos->tipo_mac_addr);                //tipo MAC addr
    strcpy(devices[index_write].tipo_BT, nuevos_datos->tipo_BT);                            //tipo BT
    strcpy(devices[index_write].num_celular, nuevos_datos->num_celular);                    //nro de celular
    strcpy(devices[index_write].bda_str, nuevos_datos->bda_str);                            //MAC addr como string
    devices[index_write].rssi[0].rssi = nuevos_datos->rssi.rssi;                            //RSSI
    devices[index_write].rssi[0].timestamp = nuevos_datos->rssi.timestamp;                  //timestamp
    //los datos de BT Classic van null
    devices[index_write].cod = 0;                                                           //CoD
    strcpy(devices[index_write].major_device_class, "");                                    //Major device class

    xSemaphoreGive(mutex_devices);
    index_write++;
    xSemaphoreGive(mutex_index_write); 

    /*xSemaphoreTake(mutex_limite, portMAX_DELAY);
    limite++;
    xSemaphoreGive(mutex_limite); */
    /*xSemaphoreTake(mutex_index_write, portMAX_DELAY);
    index_write++;
    xSemaphoreGive(mutex_index_write); */
    /*
    ESP_LOGI(TAG, "GUARDO NUEVO");
    ESP_LOGI(TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
    ESP_LOGI(TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
    ESP_LOGI(TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi);*/
    
}

/**
 * @brief obtiene datos de un dispositivo detectado.
 * Deteca solo el tipo BT Classic.
 */
static void update_device_info(esp_bt_gap_cb_param_t *param)
{
    //ESP_LOGI(TAG, "task en update_device_info(): %s", pcTaskGetTaskName(NULL));
    //char bda_str[18]; //MAC addr en formato string
    uint8_t eir_len = 0;
    uint8_t eir[ESP_BT_GAP_EIR_DATA_LEN];
    uint8_t txpower_len = 0;
    int8_t txpower = 0;

    classic_device_t *p_dev = &dev_classic_info;
    memset(p_dev, 0, sizeof(classic_device_t)); //borra posibles datos anteriores en la struct

    esp_bt_gap_dev_prop_t *p;

    bda2str(param->disc_res.bda, p_dev->bda_str, 18); //toma la MAC, la transforma a string y guarda en bda_str
    memcpy(p_dev->bda, param->disc_res.bda, ESP_BD_ADDR_LEN); //se obtiene la MAC address
    
    // el CoD y el RSSI se encuentran como propiedades dentro de la struct disc_res_param.
    // en el siguiente for se enumeran las propiedades y se verifica si es el COD o el RSSI y en tales casos se pide el valor.
    for (int i = 0; i < param->disc_res.num_prop; i++) {
        p = param->disc_res.prop + i;
        switch (p->type) {
        case ESP_BT_GAP_DEV_PROP_COD: //obtiene el CoD
            p_dev->cod = *(uint32_t *)(p->val);
            break;
        case ESP_BT_GAP_DEV_PROP_RSSI: //obtiene el RSSI
           // p_dev->rssi = *(int8_t *)(p->val);
            p_dev->rssi.rssi = *(int8_t *)(p->val); //se guarda directamente en la posicion 0 total se actualiza al final de esta funcion.
            p_dev->rssi.timestamp = time(NULL) + timestamp_inicial;  
            break;
        case ESP_BT_GAP_DEV_PROP_BDNAME: { //obtiene el nombre del device
            uint8_t len = (p->len > ESP_BT_GAP_MAX_BDNAME_LEN) ? ESP_BT_GAP_MAX_BDNAME_LEN :
                          (uint8_t)p->len;
            memcpy(p_dev->bdname, (uint8_t *)(p->val), len);
            p_dev->bdname[len] = '\0';
            p_dev->bdname_len = len;
            break;
        }
        case ESP_BT_GAP_DEV_PROP_EIR: { //obtiene el EIR
            memcpy(eir, (uint8_t *)(p->val), p->len);
            eir_len = p->len;
            break;
        }
        default:
            break;
        }
    }

    if (esp_bt_gap_is_valid_cod(p_dev->cod)){ //se fija si el CoD es valido, y en base al Major device class obtiene el tipo de dispositivo
       switch(esp_bt_gap_get_cod_major_dev(p_dev->cod)){
        case ESP_BT_COD_MAJOR_DEV_MISC:
            strcpy(p_dev->major_device_class, "Miscellaneous");
            break;
        case ESP_BT_COD_MAJOR_DEV_COMPUTER:
            strcpy(p_dev->major_device_class, "Computer");
            break;
        case ESP_BT_COD_MAJOR_DEV_PHONE:
            strcpy(p_dev->major_device_class, "Phone(cellular, cordless, pay phone, modem)");
            break;
        case ESP_BT_COD_MAJOR_DEV_LAN_NAP:
            strcpy(p_dev->major_device_class, "LAN, Network Access Point");
            break;
        case ESP_BT_COD_MAJOR_DEV_AV:
            strcpy(p_dev->major_device_class, "Audio/Video(headset, speaker, stereo, video display, VCR)");
            break;    
        case ESP_BT_COD_MAJOR_DEV_PERIPHERAL:
            strcpy(p_dev->major_device_class, "Peripheral(mouse, joystick, keyboard)");
            break;  
        case ESP_BT_COD_MAJOR_DEV_IMAGING:
            strcpy(p_dev->major_device_class, "Imaging(printer, scanner, camera, display)");
            break;  
        case ESP_BT_COD_MAJOR_DEV_WEARABLE:
            strcpy(p_dev->major_device_class, "Wearable");
            break;  
        case ESP_BT_COD_MAJOR_DEV_TOY:
            strcpy(p_dev->major_device_class, "Toy");
            break;  
        case ESP_BT_COD_MAJOR_DEV_HEALTH:
            strcpy(p_dev->major_device_class, "Health");
            break;  
        case ESP_BT_COD_MAJOR_DEV_UNCATEGORIZED:
        default:
            strcpy(p_dev->major_device_class, "Uncategorized: device not specified");
            break;
       } 
    }

    //si no se puede obtener el nombre en p_dev->bdname es porque está en el paquete EIR
    if (eir_len != 0 && p_dev->bdname_len == 0) {
        get_name_from_eir(eir, p_dev->bdname, &p_dev->bdname_len);
        get_tx_power_from_eir(eir, &txpower, &txpower_len);
    }
    p_dev->txpower = txpower; //guarda la potencia del dispositivo remoto detectado, cero en caso de que no se haya podido encontrar el TxPower en el EIR.
    char txpower_str[5];
    if(txpower == 0){
        sprintf(txpower_str, "%s", "-");
    }else{
        sprintf(txpower_str, "%d", txpower);
    }

    ESP_LOGI(TAG, "[%u] MAC: %s | Nombre: %s | CoD: 0x%x | Tipo: %s | TxPower: %s dBm | RSSI: %d dBm", 
            (unsigned) p_dev->rssi.timestamp, p_dev->bda_str, p_dev->bdname, p_dev->cod, p_dev->major_device_class, txpower_str, p_dev->rssi.rssi);


    actualizar_datos_classic(p_dev);   
}

/**
 * @brief Funcion de callback para BT Classic
 */
void bt_app_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param)
{
  //  ESP_LOGI(TAG, "task en bt_app_gap_cb(): %s", pcTaskGetTaskName(NULL));
    switch (event) {
    case ESP_BT_GAP_DISC_RES_EVT: { //device discovery result event
        update_device_info(param);
        break;
    }
    case ESP_BT_GAP_DISC_STATE_CHANGED_EVT: { // discovery state changed event
        if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STOPPED) { //disc_st_chg = discovery state changed parameter struct
            ESP_LOGI(TAG, "Device BT Classic discovery stopped.");
            esp_bt_gap_start_discovery(ESP_BT_INQ_MODE_GENERAL_INQUIRY, DURACION_INQUIRY, 0); //vuelve a arrancar
            print_devices_registrados(); //imprime devices encontrados hasta el momento

        } else if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STARTED) {
            ESP_LOGI(TAG, "BT Classic Discovery started.");
        }
        break;
    }
    default: {
        ESP_LOGI(TAG, "event: %d", event);
        break;
    }
    }
}

/**
 * @brief Funcion de callback, se llama siempre que se encuentra un dispositivo, cuando se setean los parametros 
 * y cuando comienza y termina el tiempo de scanning.
 * Se utiliza solo para BLE.
 */
static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
  //  ESP_LOGI(TAG, "task en esp_gap_cb(): %s", pcTaskGetTaskName(NULL));
    uint8_t *adv_name = NULL;
    uint8_t adv_name_len = 0;
    uint8_t *adv_txpower = NULL;
    uint8_t adv_txpower_len = 0;
    char uuid_str[37];
    uuid_t uuid;
    //char bda_str[18];

    ble_device_t *p_dev = &dev_ble_info;
    memset(p_dev, 0, sizeof(ble_device_t)); //borra posibles datos anteriores en la struct

    switch (event) {
    /*
     * Una vez que se establecen los parametros de escaneo, se activa el evento ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT
     * Este evento se utiliza para iniciar el escaneo de servidores GATT cercanos
     * El escaneo comienza usando la funcion esp_ble_gap_start_scanning()
     * Una vez que finaliza el período de escaneo, se activa un evento ESP_GAP_SEARCH_INQ_CMPL_EVT.
    */
    case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT: {
        //the unit of the duration is second
        esp_ble_gap_start_scanning(DURACION_SCANNING);
        break;
    }
    case ESP_GAP_BLE_SCAN_START_COMPLETE_EVT:
        //scan start complete event to indicate scan start successfully or failed
        if (param->scan_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(TAG, "scan start failed, error status = %x", param->scan_start_cmpl.status);
            break;
        }
        ESP_LOGI(TAG, "BLE Discovery started.");
        break;
    case ESP_GAP_BLE_SCAN_RESULT_EVT: {
        /*
         * Los resultados del escaneo se muestran tan pronto como llegan con el evento ESP_GAP_BLE_SCAN_RESULT_EVT
         * Este evento tambien incluye una lista de subeventos.
         * Estamos interesados ​​en el evento ESP_GAP_SEARCH_INQ_RES_EVT, que se llama cada vez 
         * que se encuentra un nuevo dispositivo. 
         * También estamos interesados ​​en el ESP_GAP_SEARCH_INQ_CMPL_EVT, 
         * que se activa cuando se completa la duración del escaneo y se puede usar para reiniciar el procedimiento de escaneo
         */
        esp_ble_gap_cb_param_t *scan_result = (esp_ble_gap_cb_param_t *)param;
        switch (scan_result->scan_rst.search_evt) {
        case ESP_GAP_SEARCH_INQ_RES_EVT:
            bda2str(scan_result->scan_rst.bda, p_dev->bda_str, 18); //toma la MAC, la transforma a string y guarda en bda_str
            memcpy(p_dev->bda, scan_result->scan_rst.bda, ESP_BD_ADDR_LEN); //se obtiene la MAC address
            //ble_adv es el EIR recibido
            adv_name = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv, ESP_BLE_AD_TYPE_NAME_CMPL, &adv_name_len);
            //se obtiene la potencia del dispositivo remoto emisor
            adv_txpower = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv, ESP_BLE_AD_TYPE_TX_PWR, &adv_txpower_len);
           
            //se obtiene el tipo de MAC address recibida (publica o random)
            switch(scan_result->scan_rst.ble_addr_type){
            case BLE_ADDR_TYPE_PUBLIC:
                strcpy(p_dev->tipo_mac_addr, "PUBLICA");
                break;
            case BLE_ADDR_TYPE_RANDOM:
                strcpy(p_dev->tipo_mac_addr, "RANDOM");
                break;
            case BLE_ADDR_TYPE_RPA_PUBLIC:
                strcpy(p_dev->tipo_mac_addr, "RPA PUBLIC");
                break;
            case BLE_ADDR_TYPE_RPA_RANDOM:
            default:
                strcpy(p_dev->tipo_mac_addr, "RPA RANDOM");
                break;
            }

            //se obtiene el tipo de BT que tiene el device remoto
            switch(scan_result->scan_rst.dev_type){
            case ESP_BT_DEVICE_TYPE_BREDR:
                strcpy(p_dev->tipo_BT, "BR/EDR");
                break;
            case ESP_BT_DEVICE_TYPE_BLE:
                strcpy(p_dev->tipo_BT, "BLE");
                break;
            case ESP_BT_DEVICE_TYPE_DUMO:
            default:
                strcpy(p_dev->tipo_BT, "Dual Mode");
                break;
            }

            if(adv_name == NULL){
                strcpy((char *)p_dev->bdname, "-"); //si no se pudo encontrar el nombre
                p_dev->bdname_len = adv_name_len;
            }else{ 
                memcpy(p_dev->bdname, (uint8_t *)adv_name, adv_name_len);
                p_dev->bdname[adv_name_len] = '\0';
                p_dev->bdname_len = adv_name_len;
            }

            if(adv_txpower == NULL){
                p_dev->txpower = 0; //si no se pudo determinar la potencia del Tx entonces es 0 por defecto.
            }else{
                p_dev->txpower = (int8_t) *adv_txpower;
            }

            char txpower_str[5];
            if(p_dev->txpower == 0){
                sprintf(txpower_str, "%s", "-");
            }else{
                sprintf(txpower_str, "%d", p_dev->txpower);
            }

            p_dev->rssi.rssi = scan_result->scan_rst.rssi; // se actualiza al final de esta funcion.
            p_dev->rssi.timestamp = time(NULL) + timestamp_inicial;
            
            memcpy(uuid, (scan_result->scan_rst.ble_adv + 6), 16); //se obtiene el uuid
            printf("UUID: %s\n", uuid2str(uuid, uuid_str, 37));
            if(is_phone_number(uuid)){
                //con la caracteristica son 10 digitos el numero de celular
                get_celular_from_uuid(uuid, p_dev->num_celular, 11);
            } else {
                strcpy(p_dev->num_celular, "");
            }
            //printf("CELULAR: %s\n", p_dev->num_celular);

            ESP_LOGI(TAG, "[%u] MAC: %s (%s) | Nombre: %s | Tipo BT: %s | Nro Celular: %s | TxPower: %s dBm | RSSI %d dBm", 
                    (unsigned) p_dev->rssi.timestamp, p_dev->bda_str, p_dev->tipo_mac_addr, p_dev->bdname, p_dev->tipo_BT, 
                    p_dev->num_celular, txpower_str, p_dev->rssi.rssi);

            actualizar_datos_ble(p_dev);  
            break;
        case ESP_GAP_SEARCH_INQ_CMPL_EVT:
            //Una vez que finaliza el período de escaneo, se activa un evento ESP_GAP_SEARCH_INQ_CMPL_EVT.
            ESP_LOGI(TAG, "Device BLE discovery stopped.");
            esp_ble_gap_start_scanning(DURACION_SCANNING); //se da comienzo a un nuevo escaneo
            break;
        default:
            break;
        }
        break;
    }

    case ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT:
        if (param->scan_stop_cmpl.status != ESP_BT_STATUS_SUCCESS){
            ESP_LOGE(TAG, "scan stop failed, error status = %x", param->scan_stop_cmpl.status);
            break;
        }
        ESP_LOGI(TAG, "stop scan successfully");
        ESP_LOGI(TAG, "ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT -------------------------------------------------------");
        break;
    
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
        //advertising start complete event to indicate advertising start successfully or failed
        if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(TAG, "Advertising start failed\n");
        }
        break;

    default:
        break;
    }
}

/**
 * @brief funcion que realiza algunas configuraciones extras al bluetooth classic y BLE, 
 * ademas de lo que se inicializa en el main().
 */
void bt_classic_ble_config(void)
{
    char *dev_name = DEV_NAME;        
    esp_err_t set_dev_name_ret;

    set_dev_name_ret = esp_bt_dev_set_device_name(dev_name); //set name para BT Classic
    if(set_dev_name_ret != ESP_OK){
        ESP_LOGE(TAG, "set device name failed, error code = %x", set_dev_name_ret);
    }

    set_dev_name_ret = esp_ble_gap_set_device_name(dev_name); //set name para BLE
    if(set_dev_name_ret != ESP_OK){
        ESP_LOGE(TAG, "set device name failed, error code = %x", set_dev_name_ret);
    }
    
    ESP_LOGI(TAG, "Nombre: %s", dev_name);

    //obtiene mac address de la placa ESP32 y la imprime.
    const uint8_t * bdaddr_esp32;
    bdaddr_esp32 = esp_bt_dev_get_address();
    if(bdaddr_esp32 != NULL){
        ESP_LOGI(TAG, "ESP32 BT MAC ADDRESS: %x:%x:%x:%x:%x:%x", bdaddr_esp32[0],bdaddr_esp32[1],bdaddr_esp32[2],
                                                                 bdaddr_esp32[3],bdaddr_esp32[4],bdaddr_esp32[5]);
    }

    /* set discoverable and connectable mode, wait to be connected */
    esp_bt_gap_set_scan_mode(ESP_BT_CONNECTABLE, ESP_BT_GENERAL_DISCOVERABLE);

    /* register GAP callback function */
    esp_bt_gap_register_callback(bt_app_gap_cb);

    //register the  callback function to the gap module
    esp_err_t ret = esp_ble_gap_register_callback(esp_gap_cb);
    if (ret){
        ESP_LOGE(TAG, "%s gap register failed, error code = %x\n", __func__, ret);
        return;
    }

    //comienza envio de paquetes advertising (para poder ser detectado por smartphones BLE)
    esp_ble_gap_start_advertising(&adv_params);
    
    //segundo parametro es duración de la consulta en unidades de 1,28 segundos, que van desde 0x01 a 0x30.
    //tercer parametro es el Número de respuestas que se pueden recibir antes de que se detenga la Consulta, el valor 0 indica un número ilimitado de respuestas. 
    esp_bt_gap_start_discovery(ESP_BT_INQ_MODE_GENERAL_INQUIRY, DURACION_INQUIRY, 0);

    //Los valores de escaneo se establecen mediante la funcion esp_ble_gap_set_scan_params()
    esp_err_t scan_ret = esp_ble_gap_set_scan_params(&ble_scan_params);
    if (scan_ret){
        ESP_LOGE(TAG, "set scan params error, error code = %x", scan_ret);
    }
  //  ESP_LOGI(TAG, "task en bt_classic_ble_config(): %s", pcTaskGetTaskName(NULL));
}

/**
 * @brief funcion principal donde comienza la ejecución, inicializa y habilita el controlador bluetooth 
 * y el stack Bluedroid y chequea errores.
 */
void app_main(void)
{
    /* Initialize NVS — it is used to store PHY calibration data */
    esp_err_t ret = nvs_flash_init(); //Initialize the default NVS partition.
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );

    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    if ((ret = esp_bt_controller_init(&bt_cfg)) != ESP_OK) {
        ESP_LOGE(TAG, "%s initialize controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bt_controller_enable(ESP_BT_MODE_BTDM)) != ESP_OK) {
        ESP_LOGE(TAG, "%s enable controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bluedroid_init()) != ESP_OK) {
        ESP_LOGE(TAG, "%s initialize bluedroid failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bluedroid_enable()) != ESP_OK) {
        ESP_LOGE(TAG, "%s enable bluedroid failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    ESP_ERROR_CHECK(esp_netif_init()); //inicializa el stack TCP/IP
    ESP_ERROR_CHECK(esp_event_loop_create_default()); //Create default event loop.
    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());

    /* Store the handle of the calling task. */
    xTaskToNotify = xTaskGetCurrentTaskHandle();
    
    crear_mutex(); //alloca memoria para los mutexs
    /* inititialize device information */
    limpiar_buffer(); //inicializa estructuras a cero


    //crea tarea que controla el socket cliente y envia los datos periodicamente al servidor
    xTaskCreate(tcp_client_task, "tcp_client", 4096, NULL, 5, NULL); //prioridad 5 

    //se suspende la task main hasta que se haya logrado la conexion con el socket servidor 
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY); //bloquea indefinidamente y pone el valor del semaforo a cero (no decrementa en 1)

  //  ESP_LOGI(TAG, "task en app_main(): %s", pcTaskGetTaskName(NULL));
    ESP_LOGI(TAG, "cant de tasks: %d", uxTaskGetNumberOfTasks());

    bt_classic_ble_config();
}
