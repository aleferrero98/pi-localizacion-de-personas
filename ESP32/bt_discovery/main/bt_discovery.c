/****************************************************************************
*
* This file is for Classic Bluetooth device and service discovery Demo.
*
****************************************************************************/

//extraido de https://github.com/espressif/esp-idf/tree/master/examples/bluetooth/bluedroid/classic_bt/bt_discovery
// que es el EIR -> https://docs.huihoo.com/symbian/nokia-symbian3-developers-library-v0.8/GUID-F2A793F1-A5B5-526B-B147-771D440B13A2.html

#include <stdint.h>
#include <string.h>
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_bt.h"
#include "esp_bt_main.h"
#include "esp_bt_device.h"
#include "esp_gap_bt_api.h" //CLASSIC BLUETOOTH GAP API

#define GAP_TAG             ""
#define OVERFLOW_TAG        "OVERFLOW"

#define TIMESTAMP_INICIO    1618949411
//#define DURACION_INQUIRY    ESP_BT_GAP_MAX_INQ_LEN  //Maximum inquiry duration, unit is 1.28s. El minimo es ESP_BT_GAP_MIN_INQ_LEN
#define DURACION_INQUIRY    10
#define CANT_DEVICES        5 //cantidad de dispositivos distintos que puede encontrar y guardar info simultaneamente.
#define MAX_CANT_RSSI       20 //cantidad maxima de rssi que se guardan para un mismo device remoto, si se llena el array y hay mas mediciones, estas ultimas se pierden.

typedef struct {
    uint64_t timestamp; //tiempo en el que se mide el rssi
    int8_t rssi; // valor de rssi
} time_rssi_t;

/**
 * @struct estructura que contiene los datos de un dispositivo encontrado
 */
typedef struct {
    uint8_t bdname_len;
    uint8_t eir_len;
    time_rssi_t rssi[MAX_CANT_RSSI]; //estructura que contiene varios valores de rssi medidos y su timestamp correspondiente.
    //int8_t rssi; // valor de rssi
    uint32_t cod; //codigo de dispositivo
    uint8_t eir[ESP_BT_GAP_EIR_DATA_LEN];
    uint8_t bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1]; //nombre del dispositivo bluetooth
    esp_bd_addr_t bda; //MAC address
    char major_device_class[60]; //tipo de dispositivo BT
    int8_t txpower;
} app_gap_cb_t;

static app_gap_cb_t dev_info; //struct temporal que guarda los datos de 1 solo device
static app_gap_cb_t devices[CANT_DEVICES]; //arreglo de estructuras que contienen informacion, una por cada dispositivo.

/**
 * @brief Convierte la MAC address a desde un arreglo de bytes a formato String.
 * @return devuelve un puntero al string generado.
 */
static char *bda2str(esp_bd_addr_t bda, char *str, size_t size)
{
    if (bda == NULL || str == NULL || size < 18) {
        return NULL;
    }

    uint8_t *p = bda;
    sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
            p[0], p[1], p[2], p[3], p[4], p[5]);
    return str;
}

/**
 * EIR(Extended Inquiry Response) provides information about discoverable devices during a Bluetooth Inquiry.
 * The Inquiry requests all listening ("discoverable") Bluetooth devices to identify themselves.
 * The Inquiring Device then has to issue a request to each device to determine the local name and any services that each device can support.
 * The Extended Inquiry Response (EIR) replaces the standard Inquiry Response and contains additional information.
 * Legacy devices are supported so the set of responses from all devices may include both EIR and standard Inquiry Responses.
 * The EIR data packet is a fixed size so the number of characters available for the device local name may be insufficient for the entire device local name. 
 * 
 */
static bool get_name_from_eir(uint8_t *eir, uint8_t *bdname, uint8_t *bdname_len)
{
    uint8_t *rmt_bdname = NULL;
    uint8_t rmt_bdname_len = 0;

    if (!eir) {
        return false;
    }

    rmt_bdname = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_CMPL_LOCAL_NAME, &rmt_bdname_len);
    if (!rmt_bdname) {
        rmt_bdname = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_SHORT_LOCAL_NAME, &rmt_bdname_len);
    }

    if (rmt_bdname) {
        if (rmt_bdname_len > ESP_BT_GAP_MAX_BDNAME_LEN) {
            rmt_bdname_len = ESP_BT_GAP_MAX_BDNAME_LEN;
        }

        if (bdname) {
            memcpy(bdname, rmt_bdname, rmt_bdname_len);
            bdname[rmt_bdname_len] = '\0';
        }
        if (bdname_len) {
            *bdname_len = rmt_bdname_len;
        }
        return true;
    }

    return false;
}

/**
 * @brief obtiene el TX power del dispositivo remoto desde el paquete EIR.
 */
static bool get_tx_power_from_eir(uint8_t *eir, int8_t *txpower, uint8_t *txpower_len)
{
    uint8_t *rmt_txpower = NULL;
    uint8_t rmt_txpower_len = 0;

    if (!eir) {
        ESP_LOGI(GAP_TAG, "RETORNO 1");
        return false;
    }

    //Tx power level, value is 1 octet ranging from -127 to 127, unit is dBm
    rmt_txpower = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_TX_POWER_LEVEL, &rmt_txpower_len); 
    if (rmt_txpower == NULL) { 
        //ESP_LOGI(GAP_TAG, "RETORNO 2");
        return false; //si no fue encontrado, esp_bt_gap_resolve_eir_data() retorna NULL
    }

    if (rmt_txpower) {
        if (txpower) {
            memcpy(txpower, rmt_txpower, rmt_txpower_len);
            txpower[rmt_txpower_len] = '\0';
        }
        if (txpower_len) {
            *txpower_len = rmt_txpower_len;
        }

        return true;
    }

    return false;
}

/**
 * @brief funcion que actualiza los datos de un dispositivo Bluetooth en el caso que ya haya sido registrado previamente.
 * Si se detecta por primera vez, se genera una estructura nueva para guardar sus datos.
 */
void actualizar_datos(app_gap_cb_t *nuevos_datos){
    static uint8_t index_write = 0; //index_write me indica la posicion del arreglo donde se debe escribir si se encuentra un nuevo device
    static uint8_t limite = 0; //limite me marca cuántos lugares del arreglo estan ocupados

    //si se alcanzó la capacidad maxima del buffer que guarda las mediciones, se empieza a sobreescribir en la primera posicion
    if(limite >= CANT_DEVICES){
        ESP_LOGW(OVERFLOW_TAG, "Se alcanzó la capacidad maxima del buffer, se sobreescribirán los datos");
        index_write = 0; //index_write me indica la posicion del arreglo donde se debe escribir
    }

  //  ESP_LOGI(GAP_TAG, "LIMITE: %d", limite);
  //  ESP_LOGI(GAP_TAG, "INDEX: %d", index_write);

    for(int i = 0; i < limite; i++){ //se compara solo con las posiciones que tenga algun dato (no vacias)
        if(memcmp(devices[i].bda, nuevos_datos->bda, ESP_BD_ADDR_LEN) == 0){ //se comparan las MAC addr, si son iguales se actualizan los datos
            devices[i].bdname_len = nuevos_datos->bdname_len;
            devices[i].eir_len = nuevos_datos->eir_len;
            devices[i].cod = nuevos_datos->cod;
            strcpy(devices[i].major_device_class, nuevos_datos->major_device_class);
            devices[i].txpower = nuevos_datos->txpower;
            memcpy(devices[i].eir, nuevos_datos->eir, nuevos_datos->eir_len);
            memcpy(devices[i].bdname, nuevos_datos->bdname, nuevos_datos->bdname_len);

            for(uint8_t n = 0; n < MAX_CANT_RSSI; n++){
                if(devices[i].rssi[n].timestamp == 0){ //se busca un espacio vacio en el array de rssi.
                    devices[i].rssi[n].rssi = nuevos_datos->rssi[0].rssi;
                    devices[i].rssi[n].timestamp = nuevos_datos->rssi[0].timestamp;
                    break;
                }
            }
            ESP_LOGI(GAP_TAG, "ACTUALIZO");
            ESP_LOGI(GAP_TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
            ESP_LOGI(GAP_TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
            ESP_LOGI(GAP_TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi);
            return;
        }
    }
    //si llega aqui es porque no esta registrado el dispositivo
    memcpy(&devices[index_write], nuevos_datos, sizeof(app_gap_cb_t));
    limite++;
    index_write++;

    ESP_LOGI(GAP_TAG, "GUARDO NUEVO");
    ESP_LOGI(GAP_TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
    ESP_LOGI(GAP_TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
    ESP_LOGI(GAP_TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi);

   // memset(nuevos_datos, 0, sizeof(app_gap_cb_t)); //borra datos en struct
    
}

static void update_device_info(esp_bt_gap_cb_param_t *param)
{
    char bda_str[18]; //MAC addr en formato string
    uint8_t txpower_len = 0;
    int8_t txpower = 0;

    app_gap_cb_t *p_dev = &dev_info;
    memset(p_dev, 0, sizeof(app_gap_cb_t)); //borra posibles datos anteriores en la struct

    esp_bt_gap_dev_prop_t *p;

    bda2str(param->disc_res.bda, bda_str, 18); //toma la MAC, la transforma a string y guarda en bda_str


    //si el dispositivo ya fue encontrado y la MAC no es la misma entonces retorna
    /*if (p_dev->dev_found && 0 != memcmp(param->disc_res.bda, p_dev->bda, ESP_BD_ADDR_LEN)) {
        return;
    }*/

   /* if (!esp_bt_gap_is_valid_cod(cod) ||
            !(esp_bt_gap_get_cod_major_dev(cod) == ESP_BT_COD_MAJOR_DEV_PHONE)) {
        return;
    }*/

    memcpy(p_dev->bda, param->disc_res.bda, ESP_BD_ADDR_LEN); //se obtiene la MAC address
    
    // el CoD y el RSSI se encuentran como propiedades dentro de la struct disc_res_param.
    // en el siguiente for se enumeran las propiedades y se verifica si es el COD o el RSSI y en tales casos se pide el valor.
    for (int i = 0; i < param->disc_res.num_prop; i++) {
        p = param->disc_res.prop + i;
        switch (p->type) {
        case ESP_BT_GAP_DEV_PROP_COD: //obtiene el CoD
            p_dev->cod = *(uint32_t *)(p->val);
            break;
        case ESP_BT_GAP_DEV_PROP_RSSI: //obtiene el RSSI
           // p_dev->rssi = *(int8_t *)(p->val);
            p_dev->rssi[0].rssi = *(int8_t *)(p->val); //se guarda directamente en la posicion 0 total se actualiza al final de esta funcion.
            p_dev->rssi[0].timestamp = time(NULL) + TIMESTAMP_INICIO;  
            break;
        case ESP_BT_GAP_DEV_PROP_BDNAME: { //obtiene el nombre del device
            uint8_t len = (p->len > ESP_BT_GAP_MAX_BDNAME_LEN) ? ESP_BT_GAP_MAX_BDNAME_LEN :
                          (uint8_t)p->len;
            memcpy(p_dev->bdname, (uint8_t *)(p->val), len);
            p_dev->bdname[len] = '\0';
            p_dev->bdname_len = len;
            break;
        }
        case ESP_BT_GAP_DEV_PROP_EIR: { //obtiene el EIR
            memcpy(p_dev->eir, (uint8_t *)(p->val), p->len);
            p_dev->eir_len = p->len;
            break;
        }
        default:
            break;
        }
    }

    if (esp_bt_gap_is_valid_cod(p_dev->cod)){ //se fija si el CoD es valido, y en base al Major device class obtiene el tipo de dispositivo
       switch(esp_bt_gap_get_cod_major_dev(p_dev->cod)){
        case ESP_BT_COD_MAJOR_DEV_MISC:
            strcpy(p_dev->major_device_class, "Miscellaneous");
            break;
        case ESP_BT_COD_MAJOR_DEV_COMPUTER:
            strcpy(p_dev->major_device_class, "Computer");
            break;
        case ESP_BT_COD_MAJOR_DEV_PHONE:
            strcpy(p_dev->major_device_class, "Phone(cellular, cordless, pay phone, modem)");
            break;
        case ESP_BT_COD_MAJOR_DEV_LAN_NAP:
            strcpy(p_dev->major_device_class, "LAN, Network Access Point");
            break;
        case ESP_BT_COD_MAJOR_DEV_AV:
            strcpy(p_dev->major_device_class, "Audio/Video(headset, speaker, stereo, video display, VCR)");
            break;    
        case ESP_BT_COD_MAJOR_DEV_PERIPHERAL:
            strcpy(p_dev->major_device_class, "Peripheral(mouse, joystick, keyboard)");
            break;  
        case ESP_BT_COD_MAJOR_DEV_IMAGING:
            strcpy(p_dev->major_device_class, "Imaging(printer, scanner, camera, display)");
            break;  
        case ESP_BT_COD_MAJOR_DEV_WEARABLE:
            strcpy(p_dev->major_device_class, "Wearable");
            break;  
        case ESP_BT_COD_MAJOR_DEV_TOY:
            strcpy(p_dev->major_device_class, "Toy");
            break;  
        case ESP_BT_COD_MAJOR_DEV_HEALTH:
            strcpy(p_dev->major_device_class, "Health");
            break;  
        case ESP_BT_COD_MAJOR_DEV_UNCATEGORIZED:
        default:
            strcpy(p_dev->major_device_class, "Uncategorized: device not specified");
            break;
       } 
    }

    //si no se puede obtener el nombre en p_dev->bdname es porque está en el paquete EIR
    if (p_dev->eir_len != 0 && p_dev->bdname_len == 0) {
        get_name_from_eir(p_dev->eir, p_dev->bdname, &p_dev->bdname_len);
        get_tx_power_from_eir(p_dev->eir, &txpower, &txpower_len);

        //esp_bt_gap_cancel_discovery();
    }
    p_dev->txpower = txpower; //guarda la potencia del dispositivo remoto detectado, cero en caso de que no se haya podido encontrar el TxPower en el EIR.
    char txpower_str[5];
    if(txpower == 0){
        sprintf(txpower_str, "%s", "-");
    }else{
        sprintf(txpower_str, "%d", txpower);
    }

    ESP_LOGI(GAP_TAG, "[%u] MAC: %s | Nombre: %s | CoD: 0x%x | Tipo: %s | TxPower: %s dBm | RSSI: %d dBm", 
            (unsigned) p_dev->rssi[0].timestamp, bda_str, p_dev->bdname, p_dev->cod, p_dev->major_device_class, txpower_str, p_dev->rssi[0].rssi);


    actualizar_datos(p_dev);    
}

/**
 * @brief funcion que inicializa estructura de info de dispositivos a cero.
 */
void bt_struct_devices_init(void)
{
    //app_gap_cb_t *p_devs = &devices;
    memset(devices, 0, sizeof(app_gap_cb_t)*CANT_DEVICES);
    memset(&dev_info, 0, sizeof(app_gap_cb_t)); //inicializa estructura a cero
}

/**
 * @brief Funcion de callback
 */
void bt_app_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param)
{
    switch (event) {
    case ESP_BT_GAP_DISC_RES_EVT: { //device discovery result event
        update_device_info(param);
        break;
    }
    case ESP_BT_GAP_DISC_STATE_CHANGED_EVT: { // discovery state changed event
        if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STOPPED) { //disc_st_chg = discovery state changed parameter struct
            ESP_LOGI(GAP_TAG, "Device discovery stopped.");
            esp_bt_gap_start_discovery(ESP_BT_INQ_MODE_GENERAL_INQUIRY, DURACION_INQUIRY, 0); //vuelve a arrancar
        } else if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STARTED) {
            ESP_LOGI(GAP_TAG, "Discovery started.");
        }
        break;
    }
    default: {
        ESP_LOGI(GAP_TAG, "event: %d", event);
        break;
    }
    }
    return;
}

void bt_app_gap_start_up(void)
{
    char *dev_name = "ESP32 DE ALE";
    esp_bt_dev_set_device_name(dev_name);

    //obtiene mac address de la placa ESP32 y la imprime.
    const uint8_t * bdaddr_esp32;
    bdaddr_esp32 = esp_bt_dev_get_address();
    if(bdaddr_esp32 != NULL){
        ESP_LOGI(GAP_TAG, "ESP32 MAC ADDRESS: %x:%x:%x:%x:%x:%x", bdaddr_esp32[0],bdaddr_esp32[1],bdaddr_esp32[2],
                                                                bdaddr_esp32[3],bdaddr_esp32[4],bdaddr_esp32[5]);
    }

    /* set discoverable and connectable mode, wait to be connected */
    esp_bt_gap_set_scan_mode(ESP_BT_CONNECTABLE, ESP_BT_GENERAL_DISCOVERABLE);

    /* register GAP callback function */
    esp_bt_gap_register_callback(bt_app_gap_cb);

    /* inititialize device information */
    bt_struct_devices_init(); //inicializa estructura a cero
    
    //segundo parametro es duración de la consulta en unidades de 1,28 segundos, que van desde 0x01 a 0x30.
    //tercer parametro es el Número de respuestas que se pueden recibir antes de que se detenga la Consulta, el valor 0 indica un número ilimitado de respuestas. 
    esp_bt_gap_start_discovery(ESP_BT_INQ_MODE_GENERAL_INQUIRY, DURACION_INQUIRY, 0);
}

/**
 * @brief funcion principal donde comienza la ejecución, inicializa y habilita el controlador bluetooth 
 * y el stack Bluedroid y chequea errores.
 */
void app_main(void)
{
    /* Initialize NVS — it is used to store PHY calibration data */
    esp_err_t ret = nvs_flash_init(); //Initialize the default NVS partition.
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );

    /* This function releases the BSS, data and other sections of the controller to heap. The total size is about 70k bytes. */
    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_BLE));

    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    if ((ret = esp_bt_controller_init(&bt_cfg)) != ESP_OK) {
        ESP_LOGE(GAP_TAG, "%s initialize controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bt_controller_enable(ESP_BT_MODE_CLASSIC_BT)) != ESP_OK) {
        ESP_LOGE(GAP_TAG, "%s enable controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bluedroid_init()) != ESP_OK) {
        ESP_LOGE(GAP_TAG, "%s initialize bluedroid failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bluedroid_enable()) != ESP_OK) {
        ESP_LOGE(GAP_TAG, "%s enable bluedroid failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    bt_app_gap_start_up();
}
