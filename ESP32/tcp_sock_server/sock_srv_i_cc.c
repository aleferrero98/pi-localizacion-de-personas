#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <time.h>

#define TAM 				256
#define MAX_CANT_RSSI       20 //cantidad maxima de rssi que se guardan para un mismo device remoto, si se llena el array y hay mas mediciones, estas ultimas se pierden.
#define CANT_DEVICES        10 //cantidad de dispositivos distintos que puede encontrar y guardar info simultaneamente.
/// Maximum bytes of Bluetooth device name
#define ESP_BT_GAP_MAX_BDNAME_LEN             (248)

/// Maximum size of EIR Significant part
#define ESP_BT_GAP_EIR_DATA_LEN               (240)
/// Bluetooth address length
#define ESP_BD_ADDR_LEN     6

typedef uint8_t esp_bd_addr_t[ESP_BD_ADDR_LEN];

typedef struct {
    uint64_t timestamp; //tiempo en el que se mide el rssi
    int8_t rssi; // valor de rssi
} time_rssi_t;

/**
 * @struct estructura que contiene los datos de un dispositivo encontrado
 */
typedef struct __attribute__((packed)) {
    uint8_t bdname_len;
    uint8_t eir_len;
    time_rssi_t rssi[MAX_CANT_RSSI]; //estructura que contiene varios valores de rssi medidos y su timestamp correspondiente.
    //int8_t rssi; // valor de rssi
    uint32_t cod; //codigo de dispositivo
    uint8_t eir[ESP_BT_GAP_EIR_DATA_LEN];
    uint8_t bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1]; //nombre del dispositivo bluetooth
    esp_bd_addr_t bda; //MAC address
    char major_device_class[60]; //tipo de dispositivo BT
    int8_t txpower;
} classic_device_t;

/**
 * @struct estructura que contiene los datos de un dispositivo encontrado
 */
typedef struct __attribute__((packed)) {
    uint8_t bdname_len;
    uint8_t bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1]; //nombre del dispositivo bluetooth
    esp_bd_addr_t bda; //MAC address
    int8_t txpower;
    char tipo_mac_addr[11];
    char tipo_BT[10];
    time_rssi_t rssi[MAX_CANT_RSSI]; //estructura que contiene varios valores de rssi medidos y su timestamp correspondiente.
} ble_device_t;

/**
 * @brief union que contiene datos de o un dispositivo BLE o de un dispositivo Classic.
 */
typedef union __attribute__((packed)) {
    classic_device_t classic_data;
    ble_device_t ble_data;
} data_device_t;

static data_device_t devices[CANT_DEVICES]; //arreglo de estructuras que contienen informacion, una por cada dispositivo.


/**
 * @brief Convierte la MAC address a desde un arreglo de bytes a formato String.
 * @return devuelve un puntero al string generado.
 */
static char *bda2str(esp_bd_addr_t bda, char *str, size_t size)
{
    if (bda == NULL || str == NULL || size < 18) {
        return NULL;
    }

    uint8_t *p = bda;
    sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
            p[0], p[1], p[2], p[3], p[4], p[5]);
    return str;
}

/**
 * @brief imprime la cantidad de dispositivos descubiertos junto con su MAC address y su nombre (si lo detecta).
 */
void print_devices_registrados(int cantidad){
    char bda_str[18];
    esp_bd_addr_t mac_null;
    memset(mac_null, 0, sizeof(esp_bd_addr_t));
    printf("\n-------------------- RESUMEN DISPOSITIVOS RECIBIDOS -----------------------\n");
    printf("Se han recibido %d dispositivos distintos \n", cantidad);
    for(int i = 0; i < cantidad; i++){ //se busca solo las posiciones que tenga algun dato (no vacias)
        if(memcmp(devices[i].classic_data.bda, mac_null, ESP_BD_ADDR_LEN) != 0){
            bda2str(devices[i].classic_data.bda, bda_str, 18); 
            printf("MAC: %s | Name: %s \n", bda_str, devices[i].classic_data.bdname);
        }else{
            bda2str(devices[i].ble_data.bda, bda_str, 18); 
            printf("MAC: %s | Name: %s \n", bda_str, devices[i].ble_data.bdname);
        }
    }
    printf("-----------------------------------------------------------------------------\n");
}

int main( int argc, char *argv[] ) {
	int sockfd, newsockfd, puerto, clilen, pid;
	//char buffer[TAM];
	//data_device_t *buffer;
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	time_t timestamp;
	char finalizar = 'n';

	if ( argc < 2 ) {
        fprintf( stderr, "Uso: %s <puerto>\n", argv[0] );
		exit(EXIT_FAILURE);
	}

	sockfd = socket( AF_INET, SOCK_STREAM, 0);

	memset( (char *) &serv_addr, 0, sizeof(serv_addr) );
	puerto = atoi( argv[1] );
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons( puerto );

	if ( bind(sockfd, ( struct sockaddr *) &serv_addr, sizeof( serv_addr ) ) < 0 ) {
		perror( "ligadura" );
		exit(EXIT_FAILURE);
	}

    printf( "Proceso: %d - socket disponible: %d\n", getpid(), ntohs(serv_addr.sin_port) );

	listen( sockfd, 5 );
	clilen = sizeof( cli_addr );

	while( 1 ) {
		newsockfd = accept( sockfd, (struct sockaddr *) &cli_addr, &clilen );

		pid = fork(); 

		if ( pid == 0 ){  
			// Proceso hijo
		    close( sockfd );
			timestamp = time(NULL);
			if(timestamp == ((time_t) -1)){
				perror("ERROR al obtener timestamp");
				exit(EXIT_FAILURE);
			}
			printf("timestamp actual %lu \n", timestamp);
			
			n = write(newsockfd, &timestamp, sizeof(time_t)); //envia el timestamp actual
			if ( n < 0 ) {
				perror( "escritura en socket" );
				exit(EXIT_FAILURE);
			}
		    
		    while( 1 ){
				memset( devices, 0, TAM );
				n = read(newsockfd, &devices, sizeof(data_device_t)*CANT_DEVICES);
				if ( n < 0 ) {
					perror( "lectura de socket" );
					exit(EXIT_FAILURE);
				}
				
				printf( "PROCESO %d. ", getpid() );
				print_devices_registrados(n/sizeof(data_device_t));
			//	printf( "Recibí: %s", buffer );
				
				finalizar = getchar();
				if(finalizar == 'S' || finalizar == 's'){
					n = write(newsockfd, "FIN", 4); //envia finalizar conexion con el cliente
					if ( n < 0 ) {
						perror( "escritura en socket" );
						exit( 1 );
					}
				}else{
					n = write(newsockfd, "OK", 3); //envia ACK al cliente
					if ( n < 0 ) {
						perror( "escritura en socket" );
						exit( 1 );
					}
				}

				// Verificación de si hay que terminar
				/*buffer[strlen(buffer)-1] = '\0';
				if( !strcmp( "fin", buffer ) ) {
					printf("PROCESO %d. Como recibí 'fin', termino la ejecución.\n\n", getpid());
					exit(EXIT_SUCCESS);
				}*/
		    }
		} else {//proceso padre
			printf("SERVIDOR: Nuevo cliente, que atiende el proceso hijo: %d\n", pid);
			close(newsockfd);
		}
	}
	return EXIT_SUCCESS; 
} 


/*printf("[%u] MAC: %s | Nombre: %s | CoD: 0x%x | Tipo: %s | TxPower: %s dBm | RSSI: %d dBm", 
      (unsigned) p_dev->rssi[0].timestamp, bda_str, p_dev->bdname, p_dev->cod, p_dev->major_device_class, txpower_str, p_dev->rssi[0].rssi);

printf("[%u] MAC: %s (%s) | Nombre: %s | Tipo BT: %s | TxPower: %s dBm | RSSI %d dBm", 
      (unsigned) p_dev->rssi[0].timestamp, bda_str, p_dev->tipo_mac_addr, p_dev->bdname, p_dev->tipo_BT, txpower_str, p_dev->rssi[0].rssi);
	  */