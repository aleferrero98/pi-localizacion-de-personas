/**
 * @file bluetooth_discovery.c
 * @brief 
 * @author Alejandro Ferrero y Jeremías Agustinoy
 * @version 1.0
 * @date 26/04/2021
 */ 

#include "bluetooth_discovery.h"

/**
 * Estructura con los parámetros del advertising.
 * Para poder ser detectado por los smartphones cercanos.
 */
static esp_ble_adv_params_t adv_params = {
    .adv_int_min        = 0x20,         //Minimum advertising interval for undirected and low duty cycle directed advertising. 
    .adv_int_max        = 0x40,         //Maximum advertising interval (Range: 0x0020 to 0x4000)
    .adv_type           = ADV_TYPE_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC, //muestra MAC pública del ESP32
    //.peer_addr            =
    //.peer_addr_type       =
    .channel_map        = ADV_CHNL_ALL, //todos los canales
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY, //scan and connection requests from anyone.
};



/**
 * @brief imprime la cantidad de dispositivos descubiertos junto con su MAC address y su nombre (si lo detecta).
 */
void print_devices_registrados(){
    char bda_str[18];
    esp_bd_addr_t mac_null;
    memset(mac_null, 0, sizeof(esp_bd_addr_t));
    ESP_LOGI(TAG, "\n");
    ESP_LOGI(TAG, "-------------------- RESUMEN DISPOSITIVOS ENCONTRADOS -----------------------");
    ESP_LOGI(TAG, "Se han registrado %d dispositivos distintos", limite);
    for(int i = 0; i < limite; i++){ //se busca solo las posiciones que tenga algun dato (no vacias)
        if(memcmp(devices[i].classic_data.bda, mac_null, ESP_BD_ADDR_LEN) != 0){
            bda2str(devices[i].classic_data.bda, bda_str, 18); 
            ESP_LOGI(TAG, "MAC: %s | Name: %s | Classic", bda_str, devices[i].classic_data.bdname);
        }else{
            bda2str(devices[i].ble_data.bda, bda_str, 18); 
            ESP_LOGI(TAG, "MAC: %s | Name: %s | BLE", bda_str, devices[i].ble_data.bdname);
        }
    }
    ESP_LOGI(TAG, "-----------------------------------------------------------------------------");
    ESP_LOGI(TAG, "\n");
}

/**
 * @brief Convierte la MAC address a desde un arreglo de bytes a formato String.
 * @return devuelve un puntero al string generado.
 */
static char *bda2str(esp_bd_addr_t bda, char *str, size_t size)
{
    if (bda == NULL || str == NULL || size < 18) {
        return NULL;
    }

    uint8_t *p = bda;
    sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
            p[0], p[1], p[2], p[3], p[4], p[5]);
    return str;
}

/**
 * @brief tranforma los 128 bits del UUID en string.
 */
static char *uuid2str(uuid_t uuid, char *str, size_t size){
    if(uuid == NULL || str == NULL || size < 37) {
        return NULL;
    }

    uint8_t *p = uuid;
    sprintf(str, "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x",
            p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9], p[10], 
            p[11], p[12], p[13], p[14], p[15]);
    return str;
}

/**
 * @brief Obtiene del numero de celular del UUID (10 primeros bytes hexadecimales)
 */
static char *get_celular_from_uuid(uuid_t uuid, char *str, size_t size){
    if(uuid == NULL || str == NULL || size < 11) {
        return NULL;
    }

    uint8_t *p = uuid;
    sprintf(str, "%02x%02x%02x%02x%02x",
            p[0], p[1], p[2], p[3], p[4]);
    return str;
}

/**
 * @brief funcion que inicializa estructura de info de dispositivos a cero.
 */
void bt_struct_devices_init(void)
{
    memset(devices, 0, sizeof(data_device_t)*CANT_DEVICES);
    memset(&dev_classic_info, 0, sizeof(classic_device_t)); //inicializa estructura a cero
    memset(&dev_ble_info, 0, sizeof(ble_device_t)); //inicializa estructura a cero
}

/**
 * EIR(Extended Inquiry Response) provides information about discoverable devices during a Bluetooth Inquiry.
 * The Inquiry requests all listening ("discoverable") Bluetooth devices to identify themselves.
 * The Inquiring Device then has to issue a request to each device to determine the local name and any services that each device can support.
 * The Extended Inquiry Response (EIR) replaces the standard Inquiry Response and contains additional information.
 * Legacy devices are supported so the set of responses from all devices may include both EIR and standard Inquiry Responses.
 * The EIR data packet is a fixed size so the number of characters available for the device local name may be insufficient for the entire device local name. 
 * 
 */
static bool get_name_from_eir(uint8_t *eir, uint8_t *bdname, uint8_t *bdname_len)
{
    uint8_t *rmt_bdname = NULL;
    uint8_t rmt_bdname_len = 0;

    if (!eir) {
        return false;
    }

    rmt_bdname = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_CMPL_LOCAL_NAME, &rmt_bdname_len);
    if (!rmt_bdname) {
        rmt_bdname = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_SHORT_LOCAL_NAME, &rmt_bdname_len);
    }

    if (rmt_bdname) {
        if (rmt_bdname_len > ESP_BT_GAP_MAX_BDNAME_LEN) {
            rmt_bdname_len = ESP_BT_GAP_MAX_BDNAME_LEN;
        }

        if (bdname) {
            memcpy(bdname, rmt_bdname, rmt_bdname_len);
            bdname[rmt_bdname_len] = '\0';
        }
        if (bdname_len) {
            *bdname_len = rmt_bdname_len;
        }
        return true;
    }

    return false;
}

/**
 * @brief obtiene el TX power del dispositivo remoto desde el paquete EIR.
 */
static bool get_tx_power_from_eir(uint8_t *eir, int8_t *txpower, uint8_t *txpower_len)
{
    uint8_t *rmt_txpower = NULL;
    uint8_t rmt_txpower_len = 0;

    if (!eir) {
        ESP_LOGI(TAG, "RETORNO 1");
        return false;
    }

    //Tx power level, value is 1 octet ranging from -127 to 127, unit is dBm
    rmt_txpower = esp_bt_gap_resolve_eir_data(eir, ESP_BT_EIR_TYPE_TX_POWER_LEVEL, &rmt_txpower_len); 
    if (rmt_txpower == NULL) { 
        //ESP_LOGI(TAG, "RETORNO 2");
        return false; //si no fue encontrado, esp_bt_gap_resolve_eir_data() retorna NULL
    }

    if (rmt_txpower) {
        if (txpower) {
            memcpy(txpower, rmt_txpower, rmt_txpower_len);
            txpower[rmt_txpower_len] = '\0';
        }
        if (txpower_len) {
            *txpower_len = rmt_txpower_len;
        }

        return true;
    }

    return false;
}

/**
 * @brief funcion que actualiza los datos de un dispositivo Bluetooth en el caso que ya haya sido registrado previamente.
 * Si se detecta por primera vez, se genera una estructura nueva para guardar sus datos.
 */
void actualizar_datos_classic(classic_device_t *nuevos_datos){
    //si se alcanzó la capacidad maxima del buffer que guarda las mediciones, se empieza a sobreescribir en la primera posicion
    if(limite >= CANT_DEVICES){
        ESP_LOGW(OVERFLOW_TAG, "Se alcanzó la capacidad maxima del buffer, se sobreescribirán los datos");
        index_write = 0; //index_write me indica la posicion del arreglo donde se debe escribir
    }

  //  ESP_LOGI(TAG, "LIMITE: %d", limite);
  //  ESP_LOGI(TAG, "INDEX: %d", index_write);

    for(int i = 0; i < limite; i++){ //se compara solo con las posiciones que tenga algun dato (no vacias)
        if(memcmp(devices[i].classic_data.bda, nuevos_datos->bda, ESP_BD_ADDR_LEN) == 0){ //se comparan las MAC addr, si son iguales se actualizan los datos
            devices[i].classic_data.bdname_len = nuevos_datos->bdname_len;
            devices[i].classic_data.eir_len = nuevos_datos->eir_len;
            devices[i].classic_data.cod = nuevos_datos->cod;
            strcpy(devices[i].classic_data.major_device_class, nuevos_datos->major_device_class);
            devices[i].classic_data.txpower = nuevos_datos->txpower;
            memcpy(devices[i].classic_data.eir, nuevos_datos->eir, nuevos_datos->eir_len);
            memcpy(devices[i].classic_data.bdname, nuevos_datos->bdname, nuevos_datos->bdname_len);

            for(uint8_t n = 0; n < MAX_CANT_RSSI; n++){
                if(devices[i].classic_data.rssi[n].timestamp == 0){ //se busca un espacio vacio en el array de rssi.
                    devices[i].classic_data.rssi[n].rssi = nuevos_datos->rssi[0].rssi;
                    devices[i].classic_data.rssi[n].timestamp = nuevos_datos->rssi[0].timestamp;
                    break;
                }
            }
          /*  ESP_LOGI(TAG, "ACTUALIZO");
            ESP_LOGI(TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
            ESP_LOGI(TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
            ESP_LOGI(TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi); */
            return;
        }
    }
    //si llega aqui es porque no esta registrado el dispositivo
    memcpy(&devices[index_write].classic_data, nuevos_datos, sizeof(classic_device_t));
    limite++;
    index_write++;

/*  ESP_LOGI(TAG, "GUARDO NUEVO");
    ESP_LOGI(TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
    ESP_LOGI(TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
    ESP_LOGI(TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi);*/

   // memset(nuevos_datos, 0, sizeof(app_gap_cb_t)); //borra datos en struct
    
}

/**
 * @brief funcion que actualiza los datos de un dispositivo Bluetooth en el caso que ya haya sido registrado previamente.
 * Si se detecta por primera vez, se genera una estructura nueva para guardar sus datos.
 */
void actualizar_datos_ble(ble_device_t *nuevos_datos){
    //si se alcanzó la capacidad maxima del buffer que guarda las mediciones, se empieza a sobreescribir en la primera posicion
    if(limite >= CANT_DEVICES){
        ESP_LOGW(OVERFLOW_TAG, "Se alcanzó la capacidad maxima del buffer, se sobreescribirán los datos");
        index_write = 0; //index_write me indica la posicion del arreglo donde se debe escribir
    }

    //ESP_LOGI(TAG, "LIMITE: %d", limite);
    //ESP_LOGI(TAG, "INDEX: %d", index_write);

    for(int i = 0; i < limite; i++){ //se compara solo con las posiciones que tenga algun dato (no vacias)
        if(memcmp(devices[i].ble_data.bda, nuevos_datos->bda, ESP_BD_ADDR_LEN) == 0){ //se comparan las MAC addr, si son iguales se actualizan los datos
            devices[i].ble_data.bdname_len = nuevos_datos->bdname_len;
            devices[i].ble_data.txpower = nuevos_datos->txpower;
            memcpy(devices[i].ble_data.bdname, nuevos_datos->bdname, nuevos_datos->bdname_len);
            strcpy(devices[i].ble_data.tipo_mac_addr, nuevos_datos->tipo_mac_addr);
            strcpy(devices[i].ble_data.tipo_BT, nuevos_datos->tipo_BT);

            for(uint8_t n = 0; n < MAX_CANT_RSSI; n++){
                if(devices[i].ble_data.rssi[n].timestamp == 0){ //se busca un espacio vacio en el array de rssi.
                    devices[i].ble_data.rssi[n].rssi = nuevos_datos->rssi[0].rssi;
                    devices[i].ble_data.rssi[n].timestamp = nuevos_datos->rssi[0].timestamp;
                    break;
                }
            }
         /*   ESP_LOGI(TAG, "ACTUALIZO");
            ESP_LOGI(TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
            ESP_LOGI(TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
            ESP_LOGI(TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi); */
            
            return;
        }
    }
    //si llega aqui es porque no esta registrado el dispositivo
    memcpy(&devices[index_write].ble_data, nuevos_datos, sizeof(ble_device_t));
    limite++;
    index_write++;
    /*
    ESP_LOGI(TAG, "GUARDO NUEVO");
    ESP_LOGI(TAG, "DATOS STRUCTTTTT0: %s %d", devices[0].bdname, devices[0].rssi[0].rssi);
    ESP_LOGI(TAG, "DATOS STRUCTTTTT1: %s %d", devices[1].bdname, devices[1].rssi[0].rssi);
    ESP_LOGI(TAG, "DATOS STRUCTTTTT2: %s %d", devices[2].bdname, devices[2].rssi[0].rssi);*/
    
}

static void update_device_info(esp_bt_gap_cb_param_t *param)
{
    char bda_str[18]; //MAC addr en formato string
    uint8_t txpower_len = 0;
    int8_t txpower = 0;

    classic_device_t *p_dev = &dev_classic_info;
    memset(p_dev, 0, sizeof(classic_device_t)); //borra posibles datos anteriores en la struct

    esp_bt_gap_dev_prop_t *p;

    bda2str(param->disc_res.bda, bda_str, 18); //toma la MAC, la transforma a string y guarda en bda_str


    //si el dispositivo ya fue encontrado y la MAC no es la misma entonces retorna
    /*if (p_dev->dev_found && 0 != memcmp(param->disc_res.bda, p_dev->bda, ESP_BD_ADDR_LEN)) {
        return;
    }*/

   /* if (!esp_bt_gap_is_valid_cod(cod) ||
            !(esp_bt_gap_get_cod_major_dev(cod) == ESP_BT_COD_MAJOR_DEV_PHONE)) {
        return;
    }*/

    memcpy(p_dev->bda, param->disc_res.bda, ESP_BD_ADDR_LEN); //se obtiene la MAC address
    
    // el CoD y el RSSI se encuentran como propiedades dentro de la struct disc_res_param.
    // en el siguiente for se enumeran las propiedades y se verifica si es el COD o el RSSI y en tales casos se pide el valor.
    for (int i = 0; i < param->disc_res.num_prop; i++) {
        p = param->disc_res.prop + i;
        switch (p->type) {
        case ESP_BT_GAP_DEV_PROP_COD: //obtiene el CoD
            p_dev->cod = *(uint32_t *)(p->val);
            break;
        case ESP_BT_GAP_DEV_PROP_RSSI: //obtiene el RSSI
           // p_dev->rssi = *(int8_t *)(p->val);
            p_dev->rssi[0].rssi = *(int8_t *)(p->val); //se guarda directamente en la posicion 0 total se actualiza al final de esta funcion.
            p_dev->rssi[0].timestamp = time(NULL) + TIMESTAMP_INICIO;  
            break;
        case ESP_BT_GAP_DEV_PROP_BDNAME: { //obtiene el nombre del device
            uint8_t len = (p->len > ESP_BT_GAP_MAX_BDNAME_LEN) ? ESP_BT_GAP_MAX_BDNAME_LEN :
                          (uint8_t)p->len;
            memcpy(p_dev->bdname, (uint8_t *)(p->val), len);
            p_dev->bdname[len] = '\0';
            p_dev->bdname_len = len;
            break;
        }
        case ESP_BT_GAP_DEV_PROP_EIR: { //obtiene el EIR
            memcpy(p_dev->eir, (uint8_t *)(p->val), p->len);
            p_dev->eir_len = p->len;
            break;
        }
        default:
            break;
        }
    }

    if (esp_bt_gap_is_valid_cod(p_dev->cod)){ //se fija si el CoD es valido, y en base al Major device class obtiene el tipo de dispositivo
       switch(esp_bt_gap_get_cod_major_dev(p_dev->cod)){
        case ESP_BT_COD_MAJOR_DEV_MISC:
            strcpy(p_dev->major_device_class, "Miscellaneous");
            break;
        case ESP_BT_COD_MAJOR_DEV_COMPUTER:
            strcpy(p_dev->major_device_class, "Computer");
            break;
        case ESP_BT_COD_MAJOR_DEV_PHONE:
            strcpy(p_dev->major_device_class, "Phone(cellular, cordless, pay phone, modem)");
            break;
        case ESP_BT_COD_MAJOR_DEV_LAN_NAP:
            strcpy(p_dev->major_device_class, "LAN, Network Access Point");
            break;
        case ESP_BT_COD_MAJOR_DEV_AV:
            strcpy(p_dev->major_device_class, "Audio/Video(headset, speaker, stereo, video display, VCR)");
            break;    
        case ESP_BT_COD_MAJOR_DEV_PERIPHERAL:
            strcpy(p_dev->major_device_class, "Peripheral(mouse, joystick, keyboard)");
            break;  
        case ESP_BT_COD_MAJOR_DEV_IMAGING:
            strcpy(p_dev->major_device_class, "Imaging(printer, scanner, camera, display)");
            break;  
        case ESP_BT_COD_MAJOR_DEV_WEARABLE:
            strcpy(p_dev->major_device_class, "Wearable");
            break;  
        case ESP_BT_COD_MAJOR_DEV_TOY:
            strcpy(p_dev->major_device_class, "Toy");
            break;  
        case ESP_BT_COD_MAJOR_DEV_HEALTH:
            strcpy(p_dev->major_device_class, "Health");
            break;  
        case ESP_BT_COD_MAJOR_DEV_UNCATEGORIZED:
        default:
            strcpy(p_dev->major_device_class, "Uncategorized: device not specified");
            break;
       } 
    }

    //si no se puede obtener el nombre en p_dev->bdname es porque está en el paquete EIR
    if (p_dev->eir_len != 0 && p_dev->bdname_len == 0) {
        get_name_from_eir(p_dev->eir, p_dev->bdname, &p_dev->bdname_len);
        get_tx_power_from_eir(p_dev->eir, &txpower, &txpower_len);

        //esp_bt_gap_cancel_discovery();
    }
    p_dev->txpower = txpower; //guarda la potencia del dispositivo remoto detectado, cero en caso de que no se haya podido encontrar el TxPower en el EIR.
    char txpower_str[5];
    if(txpower == 0){
        sprintf(txpower_str, "%s", "-");
    }else{
        sprintf(txpower_str, "%d", txpower);
    }

    ESP_LOGI(TAG, "[%u] MAC: %s | Nombre: %s | CoD: 0x%x | Tipo: %s | TxPower: %s dBm | RSSI: %d dBm", 
            (unsigned) p_dev->rssi[0].timestamp, bda_str, p_dev->bdname, p_dev->cod, p_dev->major_device_class, txpower_str, p_dev->rssi[0].rssi);


    actualizar_datos_classic(p_dev);    
}

/**
 * @brief Funcion de callback
 */
void bt_app_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param)
{
    switch (event) {
    case ESP_BT_GAP_DISC_RES_EVT: { //device discovery result event
        update_device_info(param);
        break;
    }
    case ESP_BT_GAP_DISC_STATE_CHANGED_EVT: { // discovery state changed event
        if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STOPPED) { //disc_st_chg = discovery state changed parameter struct
            ESP_LOGI(TAG, "Device CLASSIC discovery stopped.");
            esp_bt_gap_start_discovery(ESP_BT_INQ_MODE_GENERAL_INQUIRY, DURACION_INQUIRY, 0); //vuelve a arrancar
            print_devices_registrados(); //imprime devices encontrados hasta el momento
        } else if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STARTED) {
            ESP_LOGI(TAG, "Discovery CLASSIC started.");
        }
        break;
    }
    default: {
        ESP_LOGI(TAG, "event: %d", event);
        break;
    }
    }
    return;
}

void bt_app_gap_start_up(void)
{
    char *dev_name = "ESP32 - HACKERMAN";
    esp_err_t set_dev_name_ret;

    set_dev_name_ret = esp_bt_dev_set_device_name(dev_name); //set name para BT Classic
    if(set_dev_name_ret != ESP_OK){
        ESP_LOGE(TAG, "set device name failed, error code = %x", set_dev_name_ret);
    }

    set_dev_name_ret = esp_ble_gap_set_device_name(dev_name); //set name para BLE
    if(set_dev_name_ret != ESP_OK){
        ESP_LOGE(TAG, "set device name failed, error code = %x", set_dev_name_ret);
    }

    //obtiene mac address de la placa ESP32 y la imprime.
    const uint8_t * bdaddr_esp32;
    bdaddr_esp32 = esp_bt_dev_get_address();
    if(bdaddr_esp32 != NULL){
        ESP_LOGI(TAG, "ESP32 MAC ADDRESS: %x:%x:%x:%x:%x:%x", bdaddr_esp32[0],bdaddr_esp32[1],bdaddr_esp32[2],
                                                                bdaddr_esp32[3],bdaddr_esp32[4],bdaddr_esp32[5]);
    }

    /* set discoverable and connectable mode, wait to be connected */
    esp_bt_gap_set_scan_mode(ESP_BT_CONNECTABLE, ESP_BT_GENERAL_DISCOVERABLE);

    /* register GAP callback function */
    esp_bt_gap_register_callback(bt_app_gap_cb);

    /* inititialize device information */
    bt_struct_devices_init(); //inicializa estructuras a cero

    esp_ble_gap_start_advertising(&adv_params);
    
    //segundo parametro es duración de la consulta en unidades de 1,28 segundos, que van desde 0x01 a 0x30.
    //tercer parametro es el Número de respuestas que se pueden recibir antes de que se detenga la Consulta, el valor 0 indica un número ilimitado de respuestas. 
    esp_bt_gap_start_discovery(ESP_BT_INQ_MODE_GENERAL_INQUIRY, DURACION_INQUIRY, 0);

    //Los valores de escaneo se establecen mediante la funcion esp_ble_gap_set_scan_params()
    esp_err_t scan_ret = esp_ble_gap_set_scan_params(&ble_scan_params);
    if (scan_ret){
        ESP_LOGE(TAG, "set scan params error, error code = %x", scan_ret);
    }
}

/**
 * @brief Funcion de callback, se llama siempre que se encuentra un dispositivo, cuando se setean los parametros 
 * y cuando comienza y termina el tiempo de scanning.
 */
static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    uint8_t *adv_name = NULL;
    uint8_t adv_name_len = 0;
    uint8_t *adv_txpower = NULL;
    uint8_t adv_txpower_len = 0;
   // uint8_t *adv_dev_class = NULL;
   // uint8_t adv_dev_class_len = 0;
    char bda_str[18];

    ble_device_t *p_dev = &dev_ble_info;
    memset(p_dev, 0, sizeof(ble_device_t)); //borra posibles datos anteriores en la struct

    switch (event) {
    /*
     * Una vez que se establecen los parametros de escaneo, se activa el evento ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT
     * que es manejado por el controlador de eventos GAP esp_gap_cb().
     * Este evento se utiliza para iniciar el escaneo de servidores GATT cercanos
     * 
     * El escaneo comienza usando la funcion esp_ble_gap_start_scanning() la cual toma los parametros 
     * representando la duracion del escaneo continuo en segundos. Una vez que finaliza el período de 
     * escaneo, se activa un evento ESP_GAP_SEARCH_INQ_CMPL_EVT.
    */
    case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT: {
        //the unit of the duration is second
       // uint32_t duration = 30;
        esp_ble_gap_start_scanning(DURACION_SCANNING);
        ESP_LOGI(TAG, "BLE scanning start.");
        break;
    }
    case ESP_GAP_BLE_SCAN_START_COMPLETE_EVT:
        //scan start complete event to indicate scan start successfully or failed
        if (param->scan_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(TAG, "scan start failed, error status = %x", param->scan_start_cmpl.status);
            break;
        }
        ESP_LOGI(TAG, "scan ble start success");

        break;
    case ESP_GAP_BLE_SCAN_RESULT_EVT: {
        /*
         * Los resultados del escaneo se muestran tan pronto como llegan con el evento 
         * ESP_GAP_BLE_SCAN_RESULT_EVT, que incluye los siguientes parámetros:
         *
         *  struct ble_scan_result_evt_param {
                    esp_gap_search_evt_t search_evt;   ->Tipo de evento encontrado
                    esp_bd_addr_t bda;                 ->Direccion del disp BT
                    esp_bt_dev_type_t dev_type;        ->Tipo de dispositivo
                    esp_ble_addr_type_t ble_addr_type; ->Tipo de direccion de disp BT
                    esp_ble_evt_type_t ble_evt_type;   ->Tipo de evento de resultado de escaneo BLE
                    int rssi;                          ->RSSI
                    uint8_t  ble_adv[ESP_BLE_ADV_DATA_LEN_MAX + ESP_BLE_SCAN_RSP_DATA_LEN_MAX]; ->EIR recibido
                    int flag;                          -> Bit de flags de datos
                    int num_resps;                     -> Numero de resultados escaneados
                    uint8_t adv_data_len;              -> Adv data length 
                    uint8_t scan_rsp_len;              ->Scan response length
            } scan_rst; 
         *
         * Este evento tambien incluye una lista de subeventos.
         * Subeventos de ESP_GAP_BLE_SCAN_RESULT_EVT:
            typedef enum {
                ESP_GAP_SEARCH_INQ_RES_EVT             = 0,  Resultado de consulta por disp
                ESP_GAP_SEARCH_INQ_CMPL_EVT            = 1,  Resultado de consulta completa
                ESP_GAP_SEARCH_DISC_RES_EVT            = 2,  Resultado del descubrimiento para un dispositivo del mismo nivel
                ESP_GAP_SEARCH_DISC_BLE_RES_EVT        = 3,  Resultado del descubrimiento para el servicio basado en BLE GATT en un dispositivo similar
                ESP_GAP_SEARCH_DISC_CMPL_EVT           = 4,  Resultado del descubrimiento completo
                ESP_GAP_SEARCH_DI_DISC_CMPL_EVT        = 5,  Descubrimiento completo.
                ESP_GAP_SEARCH_SEARCH_CANCEL_CMPL_EVT  = 6,  Busqueda cancelada
            } esp_gap_search_evt_t;
         *
         * Estamos interesados ​​en el evento ESP_GAP_SEARCH_INQ_RES_EVT, que se llama cada vez 
         * que se encuentra un nuevo dispositivo. 
         * También estamos interesados ​​en el ESP_GAP_SEARCH_INQ_CMPL_EVT, 
         * que se activa cuando se completa la duración del escaneo 
         * y se puede usar para reiniciar el procedimiento de escaneo
         */
        esp_ble_gap_cb_param_t *scan_result = (esp_ble_gap_cb_param_t *)param;
        switch (scan_result->scan_rst.search_evt) {
        case ESP_GAP_SEARCH_INQ_RES_EVT:
             bda2str(scan_result->scan_rst.bda, bda_str, 18); //toma la MAC, la transforma a string y guarda en bda_str
             memcpy(p_dev->bda, scan_result->scan_rst.bda, ESP_BD_ADDR_LEN); //se obtiene la MAC address
            /*
             * Para obtener el nombre del dispositivo, utilizamos la función esp_ble_resolve_adv_data(), 
             * que toma los datos almacenados en scan_result-> scan_rst.ble_adv (es el EIR) 
             * el tipo de datos y la longitud, para extraer el valor del marco del paquete.
             * Luego se imprime el nombre del dispositivo.
            */
            //ble_adv es el EIR recibido
            adv_name = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv, ESP_BLE_AD_TYPE_NAME_CMPL, &adv_name_len);
            //se obtiene la potencia del dispositivo remoto emisor
            adv_txpower = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv, ESP_BLE_AD_TYPE_TX_PWR, &adv_txpower_len);

          //  adv_dev_class = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv, ESP_BLE_AD_MANUFACTURER_SPECIFIC_TYPE, &adv_dev_class_len);
             
           
            //se obtiene el tipo de MAC address recibida (publica o random)
            switch(scan_result->scan_rst.ble_addr_type){
            case BLE_ADDR_TYPE_PUBLIC:
                strcpy(p_dev->tipo_mac_addr, "PUBLICA");
                break;
            case BLE_ADDR_TYPE_RANDOM:
                strcpy(p_dev->tipo_mac_addr, "RANDOM");
                break;
            case BLE_ADDR_TYPE_RPA_PUBLIC:
                strcpy(p_dev->tipo_mac_addr, "RPA PUBLIC");
                break;
            case BLE_ADDR_TYPE_RPA_RANDOM:
            default:
                strcpy(p_dev->tipo_mac_addr, "RPA RANDOM");
                break;
            }

            //se obtiene el tipo de BT que tiene el device remoto
            switch(scan_result->scan_rst.dev_type){
            case ESP_BT_DEVICE_TYPE_BREDR:
                strcpy(p_dev->tipo_BT, "BR/EDR");
                break;
            case ESP_BT_DEVICE_TYPE_BLE:
                strcpy(p_dev->tipo_BT, "BLE");
                break;
            case ESP_BT_DEVICE_TYPE_DUMO:
            default:
                strcpy(p_dev->tipo_BT, "Dual Mode");
                break;
            }
            //---------------------------- UUID --------------------------------------
            char uuid_str[37];
            char num_celular[11];
            uuid_t uuid;
            memcpy(uuid, (scan_result->scan_rst.ble_adv + 6), 16);
            printf("UUID: %s\n", uuid2str(uuid, uuid_str, 37));
            //con la caracteristica son 10 digitos el numero de celular
            //strncpy(num_celular, uuid_str, 10);
            get_celular_from_uuid(uuid, num_celular, 11);
            printf("CELULAR: %s\n", num_celular);
            //------------------------------------------------------------------------

            if(adv_name == NULL){
                strcpy((char *)p_dev->bdname, "-"); //si no se pudo encontrar el nombre
                p_dev->bdname_len = adv_name_len;
            }else{ 
                memcpy(p_dev->bdname, (uint8_t *)adv_name, adv_name_len);
                p_dev->bdname[adv_name_len] = '\0';
                p_dev->bdname_len = adv_name_len;
            }

            if(adv_txpower == NULL){
                p_dev->txpower = 0; //si no se pudo determinar la potencia del Tx entonces es 0 por defecto.
            }else{
                p_dev->txpower = (int8_t) *adv_txpower;
            }

            char txpower_str[5];
            if(p_dev->txpower == 0){
                sprintf(txpower_str, "%s", "-");
            }else{
                sprintf(txpower_str, "%d", p_dev->txpower);
            }

            p_dev->rssi[0].rssi = scan_result->scan_rst.rssi; //se guarda directamente en la posicion 0 total se actualiza al final de esta funcion.
            p_dev->rssi[0].timestamp = time(NULL) + TIMESTAMP_INICIO;

            ESP_LOGI(TAG, "[%u] MAC: %s (%s) | Nombre: %s | Tipo BT: %s | TxPower: %s dBm | RSSI %d dBm", 
                    (unsigned) p_dev->rssi[0].timestamp, bda_str, p_dev->tipo_mac_addr, p_dev->bdname, p_dev->tipo_BT, txpower_str, p_dev->rssi[0].rssi);

            actualizar_datos_ble(p_dev);  
            break;
        case ESP_GAP_SEARCH_INQ_CMPL_EVT:
            ESP_LOGI(TAG, "BLE scanning stop.");
            //Una vez que finaliza el período de escaneo, se activa un evento ESP_GAP_SEARCH_INQ_CMPL_EVT.
            //esp_ble_gap_start_advertising(&adv_params);
            esp_ble_gap_start_scanning(DURACION_SCANNING); //se da comienzo a un nuevo escaneo
            break;
        default:
            break;
        }
        break;
    }

    case ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT:
        if (param->scan_stop_cmpl.status != ESP_BT_STATUS_SUCCESS){
            ESP_LOGE(TAG, "scan stop failed, error status = %x", param->scan_stop_cmpl.status);
            break;
        }
        ESP_LOGI(TAG, "stop scan successfully");
         ESP_LOGI(TAG, "ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT -------------------------------------------------------");
        break;
/*
    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
        if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS){
            ESP_LOGE(TAG, "adv stop failed, error status = %x", param->adv_stop_cmpl.status);
            break;
        }
        ESP_LOGI(TAG, "stop adv successfully");
         ESP_LOGI(TAG, "ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT -------------------------------------------------------");
        break;*/
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
        //advertising start complete event to indicate advertising start successfully or failed
        printf("ADVERTISING START\n");
        if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(TAG, "Advertising start failed\n");
        }
        break;
   /* case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
        printf("ADVERTISING STOP\n");
        if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(TAG, "Advertising stop failed\n");
        } else {
            ESP_LOGI(TAG, "Stop adv successfully\n");
        }
        break;*/
    default:
        break;
    }
}

/**
 * @brief funcion principal donde comienza la ejecución, inicializa y habilita el controlador bluetooth 
 * y el stack Bluedroid y chequea errores.
 */
void app_main(void)
{
    /* Initialize NVS — it is used to store PHY calibration data */
    esp_err_t ret = nvs_flash_init(); //Initialize the default NVS partition.
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );

    /* This function releases the BSS, data and other sections of the controller to heap. The total size is about 70k bytes. */
    //ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_BLE));

    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    if ((ret = esp_bt_controller_init(&bt_cfg)) != ESP_OK) {
        ESP_LOGE(TAG, "%s initialize controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bt_controller_enable(ESP_BT_MODE_BTDM)) != ESP_OK) {
        ESP_LOGE(TAG, "%s enable controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bluedroid_init()) != ESP_OK) {
        ESP_LOGE(TAG, "%s initialize bluedroid failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    if ((ret = esp_bluedroid_enable()) != ESP_OK) {
        ESP_LOGE(TAG, "%s enable bluedroid failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    //register the  callback function to the gap module
    ret = esp_ble_gap_register_callback(esp_gap_cb);
    if (ret){
        ESP_LOGE(TAG, "%s gap register failed, error code = %x\n", __func__, ret);
        return;
    }

    bt_app_gap_start_up();
}

/**
 * @brief funcion main principal, es el punto de entrada del programa.
 *
void app_main(void)
{
     Initialize NVS.
    * La función principal comienza inicializando la biblioteca de almacenamiento no volátil. 
    * Esta biblioteca permite guardar pares clave-valor en la memoria flash y algunos componentes, 
    * como la biblioteca Wi-Fi, la utilizan para guardar el SSID y la contraseña:
    *
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );

    //ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT)); //libera memoria del controlador del BT Classic


    
    * Inicializacion del controlador BT, crea primero una estructura de conf de controlador BT 
    * llamada esp_bt_controller_config_t con la conf predeterminada generada por la macro 
    * BT_CONTROLLER_INIT_CONFIG_DEFAULT()
    * El controlador BT implementa la interfaz de controlador de host (HCI) en el lado del controlador,
    * la capa de enlace (LL) y la capa fisica (PHY).
    * El controlador BT es invisible para las aplicaciones del usuario y se ocupa de las capas inferiores
    * de la pila BLE.
    * La configuracion del controlador incluye la configuracion del tamaño de pila del controlador BT,
    * la prioridad y la velocidad en baudios de HCI. Con la configuracion creada, el controlador BT
    * se inicializa y habilita con la funcion esp_bt_controller_init()
    *
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT(); //config por defecto
    ret = esp_bt_controller_init(&bt_cfg);
    if (ret) {
        ESP_LOGE(TAG, "%s initialize controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }
    
    * Siguiente, el controlador es habilitado en modo BLE
    * El controlador debe ser habilitado en ESP_BT_MODE_BTDM, si necesitas usar el dual mode (BLE + BT).
    * 
    * Existen cuatro modos de Bluetooth soportados:
    *   ESP_BT_MODE_IDLE: Bluetooth no corriendo
    *   ESP_BT_MODE_BLE: Modo BLE
    *   ESP_BT_MODE_CLASSIC_BT: Modo BL clasico
    *   ESP_BT_MODE_BTDM: Dual mode (BLE + BT Classic)
    *
    ret = esp_bt_controller_enable(ESP_BT_MODE_BTDM);
    if (ret) {
        ESP_LOGE(TAG, "%s enable controller failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    
    * Luego de la inicializacion del controlador BT, se inicializa el stack Bluedroid,
    * el cual incluye las definiciones y API comunes para BT clasico y BLE, se inicializa
    * y habilita por: 
    *   ret = esp_bluedroid_init();
    *   ret = esp_bluedroid_enable();
    *
    ret = esp_bluedroid_init();  //inicializa stack Bluedroid
    if (ret) {
        ESP_LOGE(TAG, "%s init bluetooth failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    ret = esp_bluedroid_enable();  //habilita stack Bluedroid
    if (ret) {
        ESP_LOGE(TAG, "%s enable bluetooth failed: %s\n", __func__, esp_err_to_name(ret));
        return;
    }

    
    * La función principal finaliza registrando los controladores de eventos GAP y GATT, 
    * así como el perfil de la aplicación y establece el tamaño máximo de MTU admitido.
    * 
    * Los manejadores de eventos GAP y GATT son las funciones que se utilizan para 
    * capturar los eventos generados por la pila BLE y ejecutar funciones para configurar 
    * los parámetros de la aplicación.
    * 
    * Además, los controladores de eventos también se utilizan para manejar eventos de 
    * lectura y escritura provenientes de la central. 
    * El controlador de eventos GAP se encarga de escanear y conectarse a los servidores y 
    * el controlador GATT administra los eventos que suceden después de que el cliente se ha 
    * conectado a un servidor, como la búsqueda de servicios y la escritura y lectura de datos.
    * 
    * Los controladores de eventos GAP y GATT se registran mediante:
    *   esp_ble_gap_register_callback();
    *   esp_ble_gattc_register_callback();
    * 
    * Las funciones esp_gap_cb() y esp_gattc_cb() maneja todos los eventos generados por el stack BLE.
    *
   
    //register the  callback function to the gap module
    ret = esp_ble_gap_register_callback(esp_gap_cb);
    if (ret){
        ESP_LOGE(TAG, "%s gap register failed, error code = %x\n", __func__, ret);
        return;
    }

    //se configura el nombre del dispositivo Bluetooth
    char *dev_name = "ESP32 BLE";
    esp_ble_gap_set_device_name(dev_name);

    //obtiene mac address PÚBLICA de la placa ESP32 y la imprime.
    const uint8_t * bdaddr_esp32;
    bdaddr_esp32 = esp_bt_dev_get_address();
    if(bdaddr_esp32 != NULL){
        ESP_LOGI(TAG, "ESP32 PUBLIC MAC ADDRESS: %x:%x:%x:%x:%x:%x", 
                 bdaddr_esp32[0],bdaddr_esp32[1],bdaddr_esp32[2],
                 bdaddr_esp32[3],bdaddr_esp32[4],bdaddr_esp32[5]);
    }

    bt_struct_devices_init(); //inicializa estructura a cero

    //Los valores de escaneo se establecen mediante la funcion esp_ble_gap_set_scan_params()
    esp_err_t scan_ret = esp_ble_gap_set_scan_params(&ble_scan_params);
    if (scan_ret){
        ESP_LOGE(TAG, "set scan params error, error code = %x", scan_ret);
    }
}*/
