#include <stdint.h>
#include <string.h>
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_bt.h"
#include "esp_bt_main.h"        //Inicializa y habilita el stack Bluedroid 
#include "esp_bt_device.h"
#include "esp_gap_bt_api.h"     //CLASSIC BLUETOOTH GAP API

#include <stdbool.h>
#include <stdio.h>
#include "esp_gap_ble_api.h"        //Implementa la conf GAP, como parametros de conexion
#include "esp_gattc_api.h"          //Implementa la conf de cliente GATT, como conexion de perifericos y busqueda de servicios
#include "esp_gatt_defs.h"
#include "esp_gatt_common_api.h"

#define TAG                 ""
#define OVERFLOW_TAG        "OVERFLOW"

#define TIMESTAMP_INICIO    1618949411
//#define DURACION_INQUIRY    ESP_BT_GAP_MAX_INQ_LEN  //Maximum inquiry duration, unit is 1.28s. El minimo es ESP_BT_GAP_MIN_INQ_LEN
#define DURACION_INQUIRY    10 //para BT Classic
#define DURACION_SCANNING   30 //duracion del escaneo en segundos para BLE
#define CANT_DEVICES        15 //cantidad de dispositivos distintos que puede encontrar y guardar info simultaneamente.
#define MAX_CANT_RSSI       20 //cantidad maxima de rssi que se guardan para un mismo device remoto, si se llena el array y hay mas mediciones, estas ultimas se pierden.

typedef struct {
    uint64_t timestamp; //tiempo en el que se mide el rssi
    int8_t rssi; // valor de rssi
} time_rssi_t;

/**
 * @struct estructura que contiene los datos de un dispositivo encontrado
 */
typedef struct __attribute__((packed)) {
    uint8_t bdname_len;
    uint8_t eir_len;
    time_rssi_t rssi[MAX_CANT_RSSI]; //estructura que contiene varios valores de rssi medidos y su timestamp correspondiente.
    //int8_t rssi; // valor de rssi
    uint32_t cod; //codigo de dispositivo
    uint8_t eir[ESP_BT_GAP_EIR_DATA_LEN];
    uint8_t bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1]; //nombre del dispositivo bluetooth
    esp_bd_addr_t bda; //MAC address
    char major_device_class[60]; //tipo de dispositivo BT
    int8_t txpower;
} classic_device_t;

/**
 * @struct estructura que contiene los datos de un dispositivo encontrado
 */
typedef struct __attribute__((packed)) {
    uint8_t bdname_len;
    uint8_t bdname[ESP_BT_GAP_MAX_BDNAME_LEN + 1]; //nombre del dispositivo bluetooth
    esp_bd_addr_t bda; //MAC address
    int8_t txpower;
    char tipo_mac_addr[11];
    char tipo_BT[10];
    time_rssi_t rssi[MAX_CANT_RSSI]; //estructura que contiene varios valores de rssi medidos y su timestamp correspondiente.
} ble_device_t;

/**
 * @brief union que contiene datos de o un dispositivo BLE o de un dispositivo Classic.
 */
typedef union __attribute__((packed)) {
    classic_device_t classic_data;
    ble_device_t ble_data;
} data_device_t;

/*
 * El cliente GATT normalmente escanea a sus vecinos e intenta conectarse a ellos, en caso de interes.
 * Sin embargo para realizar el escaneo, primero necesitamos establecer los parametros de configuracion.
 * Esto terminara luego del registro de las aplicaciones de perfiles, porque el registro 
 * una vez terminada, desencadena un evento ESP_GATTC_REG_EVT. 
 * La primera vez que se activa este evento, el controlador GATT lo captura y asigna la interfaz GATT al
 * perfil A, luego el evento lo reenvia al controlador de eventos GATT del perfil A.
 * En el controlador de eventos, el evento se usa para llamar la funcion esp_ble_gap_set_scan_params() 
 * que toma una instancia de la estructura ble_scan_params como parametro.
 * */
static esp_ble_scan_params_t ble_scan_params = {
    .scan_type              = BLE_SCAN_TYPE_ACTIVE,         //Tipo de escaneo - ACTIVO
    .own_addr_type          = BLE_ADDR_TYPE_PUBLIC,         //Tipo de direccion del propietario - DIRECCION PUBLICA
    .scan_filter_policy     = BLE_SCAN_FILTER_ALLOW_ALL,    //Politica de filtro de escaneo - ALL 
    .scan_interval          = 0x50,                         /*Intervalo de escaneo, esto se define como 
                                                             *el intervalo de tiempo desde que el controlador
                                                             *inicio su ultimo escaneo LE hasta que comienza
                                                              el siguiente escaneo LE - 100 ms (1.25 ms * 0x50) */
    .scan_window            = 0x30,                         /*Ventana de escaneo, la duracion del escaneo
                                                             *LE_Scan_Window debe ser menor o igual que 
                                                             *LE_Scan_Interval - 60 ms (1.25 ms * 0x30).*/
    .scan_duplicate         = BLE_SCAN_DUPLICATE_DISABLE
};

static classic_device_t dev_classic_info; //struct temporal que guarda los datos de 1 solo device de tipo BT Classic
static ble_device_t dev_ble_info; //struct temporal que guarda los datos de 1 solo device de tipo BLE
static data_device_t devices[CANT_DEVICES]; //arreglo de estructuras que contienen informacion, una por cada dispositivo.
static uint8_t index_write = 0; //index_write me indica la posicion del arreglo donde se debe escribir si se encuentra un nuevo device
static uint8_t limite = 0; //limite me marca cuántos lugares del arreglo estan ocupados


static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);
static char *bda2str(esp_bd_addr_t bda, char *str, size_t size);
static bool get_name_from_eir(uint8_t *eir, uint8_t *bdname, uint8_t *bdname_len);
static bool get_tx_power_from_eir(uint8_t *eir, int8_t *txpower, uint8_t *txpower_len);
void actualizar_datos_classic(classic_device_t *nuevos_datos);
void actualizar_datos_ble(ble_device_t *nuevos_datos);
static void update_device_info(esp_bt_gap_cb_param_t *param);
void bt_struct_devices_init(void);
void bt_app_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param);
void bt_app_gap_start_up(void);
static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);

void print_devices_registrados();

// ----------------------------- UUID ---------------------------------
typedef uint8_t uuid_t[16];
static char *uuid2str(uuid_t uuid, char *str, size_t size);
static char *get_celular_from_uuid(uuid_t uuid, char *str, size_t size);
// --------------------------------------------------------------------