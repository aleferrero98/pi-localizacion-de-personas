## Comandos

1. Copiamos example en otro directorio:
	    
        cd ~/esp
		cp -r $IDF_PATH/examples/get-started/hello_world .

2. Seteamos port en el proyecto
		
        cd ~/esp/hello_world
		idf.py set-target esp32
		idf.py menuconfig

3. Compilar
		
        idf.py build  

4. Compilar y cargar binario
		
        idf.py -p PORT [-b BAUD] flash

5. Ver puerto serie
		
        idf.py -p /dev/ttyUSB0 monitor

6. salir con *Ctrl + ]*.

paso **4)** y **5)** juntos -> *idf.py -p PORT flash monitor*