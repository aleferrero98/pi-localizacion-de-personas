# Appendix B. SampEn Code in R

# Step 1: read the data series
    #u <- read.csv("dataseries_dentro.csv", header=FALSE)
    #u <- u[,1] # tomo la 1er columna
    u <- read.csv("rssi_fuera_D4_61_9D_14_94_8F.csv", header=FALSE, sep = " ")
    u <- u[,4] # tomo la 4ta columna

# Step 2: set the SampEn parameters m and r
# optional: make the series stationary
# ------------------------------------------
    u <- (u-mean(u))/sd(u)
    m <- 2 # tamaño de los subvectores o longitud de los bloques comparados
    NSUM <- length(u) - m
    N <- length(u) # tamaño de la muestra

    #m <- 2
    Sigma = sd(u)
    r <- 0.1*Sigma # noise filter

# initiate variables to zero
    pos <- rep(0,NSUM)
    mat <- rep(0,NSUM)

    ratio <- 0
    sampen <- 0

# ------- SAMPEN CALCULATION BLOCK --------------

for(i in 1:NSUM){ # Step 3: template vector

    posibles <- 0
    matches <- 0

    for(j in 1:NSUM){ # Step 4: look for matches in the whole series

        if(j != i){ #Open condition: avoid self matches

            # first possibility: it does not fit the condition for k < m
            for(k in 1:(m+1)){
                if(k<m){
                    dif <- abs(max(u[i+k-1] - u[j+k-1]))
                    if(dif > r){
                        break
                    }
                }

                # second possibility: check if it is a possible block or not, k = m
                if(k == m){
                    dif2 <- abs(max(u[i+k-1] - u[j+k-1]))
                    if(dif2 > r){
                        break
                    } else {
                        posibles <- posibles + 1
                        next
                    }
                }

                # third possibility: check if it is a match block or not, k = m+1
                if(k > m){
                    dif3 <- abs(max(u[i+k-1] - u[j+k-1]))
                    if(dif3 > r){
                        break
                    } else {
                        matches <- matches + 1
                        next
                    }
                }

            } #Close condition: avoid self-matches

        }
    }

    pos[i] <- posibles # save the probabilities for each template vector i
    mat[i] <- matches # save the probabilities for each template vector i
}


# Step 5: determine all total possibles and matches
# --------------------------------------------------
    posibles <- 0
    matches <- 0

    for(i in 1:NSUM){
        posibles <- posibles + pos[i] #posibles
        matches <- matches + mat[i] #matches
    }

# Step 6: SampEn calculation
# --------------------------------------------------
    ratio <- matches/posibles
    sampen = -log(ratio, base = exp(1)) #sampen, usa el logaritmo natural (ln)
    print(c(r/Sigma,sampen))

    cat("N =", N, "\n")
    cat("r/Sigma =", r/Sigma, "\n")
    cat("r =", r, "\n")
    cat("m =", m, "\n")
    cat("Entropía Aproximada:", sampen, "\n")

# ------- END SAMPEN CALCULATION BLOCK --------------
