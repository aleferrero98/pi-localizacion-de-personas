package com.example.peopletrackingapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Clase que permite el manejo de una Base de datos SQLite.
 */
public class DbOpenHelper extends SQLiteOpenHelper {

    //VER SI NO ES NOT NULL!!!
    private static final String LUGARES_TABLE_CREATE = "CREATE TABLE lugares_visitados" +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, lugar TEXT, telefono TEXT, email TEXT, fecha INTEGER)";
    private static final String DB_NAME = "lugares_visitados.sqlite";
    private static final int DB_VERSION = 1;

    public DbOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    /**
     * Crea las tablas de la base de datos SQLite
     * @param db objeto base de datos
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LUGARES_TABLE_CREATE);
    }

    /**
     * Se ejecutará cada vez que cambiamos la versión de la base de datos y se usará
     * para migrar los datos de la base de datos anterior a la nueva versión.
     * @param db objeto base de datos
     * @param oldVersion version anterior
     * @param newVersion version nueva
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

