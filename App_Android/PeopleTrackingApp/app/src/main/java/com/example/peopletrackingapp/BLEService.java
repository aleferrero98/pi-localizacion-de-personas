package com.example.peopletrackingapp;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.BeaconTransmitter;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.example.peopletrackingapp.QrScannerActivity.CHANNEL_ID;
import static org.altbeacon.beacon.BeaconTransmitter.SUPPORTED;

/**
 * Clase que implementa el Foreground Service.
 * Ejecuta un servicio en background que emite beacons y realiza escaneos periódicamente.
 */
public class BLEService extends Service {

    // para el scanner BLE
    private final String TAG = "INFO - BLE Service";
    private static final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter adapter;
    private BluetoothLeScanner scanner;
    private List<ScanFilter> filters = null;
    private ScanSettings scanSettings;
    private String[] namesToFilter = null;
    private String[] macAddresses = null;

    // para la transmision de Beacons
    private Beacon beacon;
    private BeaconParser beaconParser;
    private BeaconTransmitter beaconTransmitter;
    private BackgroundPowerSaver backgroundPowerSaver;
    private String numCelular;

    // para la ejecucion periodica
    private Handler handler;
    private Runnable runnableCode;
    private final int PERIODO_SCAN = 300000; //5 min entre un escaneo y el siguiente (en miliseg)

    private boolean detectados = true; //se setea en true siempre que se encuentre alguna ESP32(por nombre o por MAC)
    //cuenta la cantidad de veces que no son detectados ninguno de los ESP32 cuando llega a su limite finaliza la app
    private int contaNoDetectados = 0;
    private final int LIMITE_NO_DETECTADOS = 2;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate() {
        super.onCreate();
        initAdapter(); //inicializa el adaptador bluetooth
        configurarEscaneo();
        // Simply constructing this class and holding a reference to it in your custom Application class
        // enables auto battery saving of about 60%
        //backgroundPowerSaver = new BackgroundPowerSaver(this);  //TODO VER SI HAY QUE USAR

        // Create the Handler object (on the main thread by default)
        handler = new Handler();
        // Define el bloque de codigo a ser ejecutado
        runnableCode = new Runnable() {
            @Override
            public void run() {
                // Do something here on the main thread
                Log.d("Handlers", "Called on main thread " + detectados);
                if(detectados == true){
                    detectados = false;
                    contaNoDetectados = 0; //se resetea el contador
                    //comenzarEscaneo();
                } else {
                    contaNoDetectados += 1;
                    if(contaNoDetectados == LIMITE_NO_DETECTADOS){
                        finalizarServicio();
                    }
                }
                // Repeat this the same runnable code block again
                // 'this' is referencing the Runnable object
                handler.postDelayed(this, PERIODO_SCAN); //en milisegundos
            }
        };
    }

    /**
     * Finaliza la ejecución del Foreground Service y de la app en general.
     * Detiene la emisión de beacons y los escaneos.
     */
    public void finalizarServicio(){
        Toast.makeText(this, "Finalizando App", Toast.LENGTH_SHORT).show();
        // Removes pending code execution
        handler.removeCallbacks(runnableCode);
        stopTransmitBeacon();
        scanner.stopScan(scanCallback);
        stopForeground(true); //elimina la notificacion del servicio en la barra de notificaciones
        stopSelf();
        Log.d(TAG, "Finalizando App");
    }

    /**
     * Método que se ejecuta al hacer un startService en QrScannerActivity.
     * Obtiene los nombres y MAC a filtrar y el número de celular del cliente.
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        namesToFilter = intent.getStringArrayExtra("nombres");
        macAddresses = intent.getStringArrayExtra("macAddress");
        numCelular = intent.getStringExtra("numCelular");
        initBeacon(numCelular); //establece el formato del beacon a transmitir
        setFilters(); //se configuran los filtros

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Servicio de emisión de beacons")
                .setContentText("El proceso se está ejecutando")
                .setSmallIcon(R.drawable.ic_android) //TODO CAMBIAR ICONO!!!!
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        //do heavy work on a background thread
        //stopSelf();
        startTransmitBeacon(); //comienza a transmitir
       // comenzarEscaneo();
        handler.post(runnableCode); // Start the initial runnable task by posting through the handler
        comenzarEscaneo();

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //---------------------------- BLE Scanner ---------------------------------
    /**
     * Habilita el adaptador bluetooth y instancia un scanner BLE.
     */
    public void initAdapter(){
        Log.d(TAG, "Init Adapter");
        adapter = BluetoothAdapter.getDefaultAdapter();
       /* if (!adapter.isEnabled()) {
            //Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            //startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }*/
        scanner = adapter.getBluetoothLeScanner();
    }

    /**
     * Setea los filtros por nombres y por direcciones MAC del BLE Scanner.
     */
    public void setFilters() {
        filters = new ArrayList<>();
        //agrego nombres a filtrar
        if (namesToFilter != null) {
            for (String name : namesToFilter) {
                ScanFilter filter = new ScanFilter.Builder()
                        .setDeviceName(name)
                        .build();
                filters.add(filter);
            }
        }
        //agrego direcciones mac a filtrar
        if (macAddresses != null) {
            for (String address : macAddresses) {
                ScanFilter filter = new ScanFilter.Builder()
                        .setDeviceAddress(address.toUpperCase()) //se convierte MAC en mayusculas para evitar error
                        .build();
                filters.add(filter);
            }
        }
    }

    /**
     * Configura los parámetros de escaneo del BLE Scanner.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void configurarEscaneo(){
        Log.d(TAG, "Set Escaneo");
        scanSettings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
                .setNumOfMatches(ScanSettings.MATCH_NUM_ONE_ADVERTISEMENT)
                .setReportDelay(0L)
                .build();
    }

    /**
     * Comienza el escaneo. Si hay algún error se lo notifica al cliente.
     */
    public void comenzarEscaneo(){
        Log.d(TAG, "Comenzar escaneo");
        if (scanner != null) {
            scanner.startScan(filters, scanSettings, scanCallback);
            Toast.makeText(this, "scan started", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "scan started");
        }  else {
            Toast.makeText(this, "could not get scanner object", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "could not get scanner object");
        }
    }

    /**
     * Callback que se accede cuando se recibe algún dato al escanear por bluetooth.
     */
    private final ScanCallback scanCallback = new ScanCallback() {
        /**
         * Al detectar algun dispositivo de los especificados en el filtro, se setea el flag de detectados en true.
         * @param callbackType
         * @param result
         */
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            Log.d(TAG, "Scan Result");
            BluetoothDevice device = result.getDevice();
            Log.d(TAG, "Nombre: " + device.getName() + " | MAC: " + device.getAddress() + " | RSSI: " + result.getRssi());
            detectados = true;
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            // Ignore for now
            Log.d(TAG, "Batch scan results");
        }

        /**
         * Por si falla el escaneo.
         * @param errorCode
         */
        @Override
        public void onScanFailed(int errorCode) {
            // Ignore for now
            Log.d(TAG, "Scan failed");
        }
    };
    //---------------------------------------------------------------------

    //------------------------- Beacon Transmision ---------------------------
    /**
     * Inicializa el beacon y el beaconTransmitter.
     * Genera un beacon con el formato open-source de AltBeacon.
     * Establece el UUID del beacon como el nro de celular del usuario y completa los digitos
     * restantes como todas 'a'(para un facil reconocimiento por parte de las ESP32).
     * @param celular número de celular ingresado por el usuario en el activity Main.
     */
    public void initBeacon(String celular){
        String numero, uuid;
        numero = celular.substring(0, 8) + "-" + celular.substring(8);
        uuid = numero + "aa-aaaa-aaaa-aaaaaaaaaaaa";
        beacon = new Beacon.Builder()
                .setId1(uuid)
                .setId2("1")
                .setId3("2")
                .setManufacturer(0x0118) // Radius Networks(ALTbeacon).  Change this for other beacon layouts
                .setTxPower(-59)
                .setDataFields(Arrays.asList(new Long[] {0l}))
                //.setBluetoothAddress("40:30:20:20:30:40")
                //.setBluetoothName("ALE")
                .build();
        //Configures whether a the bluetoothAddress (mac address) must be the same for two Beacons to be configured equal
        Beacon.setHardwareEqualityEnforced(true);

        beaconParser = new BeaconParser()
                .setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25");
        beaconTransmitter = new BeaconTransmitter(getApplicationContext(), beaconParser);
        //setea el modo de emision de beacons
        beaconTransmitter.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED); //3 Hz
        Log.d(TAG, "BT: " + beacon.getBluetoothAddress() + " | " + beacon.getBluetoothName());
    }

    /**
     * Comienza a transmitir beacons. En caso de que no sea posible transmitir la app finalizará.
     * Si ya se está transmitiendo, se mostrará un mensaje de error.
     */
    public void startTransmitBeacon(){
        if(BeaconTransmitter.checkTransmissionSupported(getApplicationContext()) != SUPPORTED){
            Toast.makeText(this, "Transmisión de Beacons no soportada", Toast.LENGTH_SHORT).show();
            finalizarServicio();
        } else if(!beaconTransmitter.isStarted()){
            Toast.makeText(this, "Start Transmit Beacons", Toast.LENGTH_SHORT).show();
            beaconTransmitter.startAdvertising(beacon);
        } else {
            Toast.makeText(this, "Ya se está transmitiendo!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Detiene la transmisión de beacons, en caso de que se estuviese transmitiendo.
     */
    public void stopTransmitBeacon(){
        if(BeaconTransmitter.checkTransmissionSupported(getApplicationContext()) != SUPPORTED){
            Toast.makeText(this, "Transmisión de Beacons no soportada", Toast.LENGTH_SHORT).show();
        } else if(beaconTransmitter.isStarted()) {
            Toast.makeText(this, "Stop Transmit Beacons", Toast.LENGTH_SHORT).show();
            beaconTransmitter.stopAdvertising();
        } else {
            Toast.makeText(this, "NO se está transmitiendo!", Toast.LENGTH_SHORT).show();
        }
    }

}