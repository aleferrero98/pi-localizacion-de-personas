package com.example.peopletrackingapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Clase principal, contiene la vista que se muestra al inicio de la app.
 *
 */
public class MainActivity extends AppCompatActivity {

    private String numeroTelefono = null;
    private final String TAG = "INFO-MainActivity";
    private Intent qrActivity = null;
    private Intent numTelActivity = null;

    private AlertDialog cuadroBienvenida;  //dialog que se muestra 1 vez al iniciar la app
    private AlertDialog cuadroNumeroNull; //se muestra cuando el numero de telefono es null
    private AlertDialog cuadroNoLugaresVisit; //se muestra cuando no hay registro de lugares visitados en los ultimos 15 dias
    private AlertDialog cuadroLugaresVisit; //muestra el nombre y fecha en que se visito cada lugar en los ultimos 15 dias

    private final int CANT_SEG_DIA = 86400; //cantidad de segundos en un dia
    //limite de dias (expresado en seg) en los que una persona presenta sintomas luego de haberse contagiado
    private final int UMBRAL_CONTAGIO = 15*CANT_SEG_DIA; //15 dias (en seg)

    /**
     * Vista que se ejecuta al iniciar la app.
     * Muestra un mensaje de información acerca de la finalidad de la aplicación.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!getCheckBoxStatus()){ //chequea si se tiene que mostrar el mensaje de bienvenida
            printMensajeBienvenida();
        }
    }

    /**
     * Imprime un mensaje de bienvenida al iniciar la app en el cual comenta cuál es la
     * finalidad de la misma.
     */
    public void printMensajeBienvenida(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_app_presentation, null);
        CheckBox mCheckBox = mView.findViewById(R.id.checkBox);

        builder.setTitle("Bienvenidos a PeopleTrackingApp");
        builder.setMessage("Esta aplicación fue diseñada para rastrear a las personas que sean positivos de Covid-19. \n" +
                "Es muy importante que otorgue los permisos que le solicita la aplicación para que la misma pueda funcionar correctamente y que no apague el Bluetooth mientras permanece en el establecimiento. \n" +
                "La App fue diseñada para minimizar el consumo al máximo y dejará de ejecutarse cuando haya abandonado el lugar. \n" +
                "#LoCombatimosEntreTodos");
        // builder.setIcon(R.drawable.ic_app_peopletracking_background);
        //en caso de que el nro de ceular sea nulo se pasa a la vista para que lo ingrese
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(mCheckBox.isChecked()){ //si esta presionado se guarda true
                    storeCheckBoxStatus(true);
                } else {
                    storeCheckBoxStatus(false);
                }
                numeroTelefono = getNumTelefono();
                if(numeroTelefono == null || numeroTelefono == ""){
                    numTelActivity = new Intent(getApplicationContext(), NumeroTelefonoActivity.class);
                    startActivity(numTelActivity);
                }
            }
        });

        builder.setView(mView);
        cuadroBienvenida = builder.create();
        cuadroBienvenida.show();
    }

    /**
     * Lee desde memoria el último valor almacenado para el numero de telefono del usuario.
     * @return String con el numero de telefono.
     */
    public String getNumTelefono(){
        SharedPreferences preferences = getSharedPreferences("nroCelular", Context.MODE_PRIVATE);
        return preferences.getString("nroCelular","");
    }

    /**
     * Obtiene el valor almacenado en memoria para el checkbox 'no volver a mostrar' del mensaje de
     * bienvenida.
     * @return true si se ha marcado previamente el checkBox y se ha guardado ese valor.
     */
    private boolean getCheckBoxStatus(){
        SharedPreferences preferences = getSharedPreferences("CheckItem", Context.MODE_PRIVATE);
        return preferences.getBoolean("CheckItem", false);
    }

    /**
     * Almacena en memoria el estado del checkBox 'no volver a mostrar'.
     * @param isChecked true si fue activado, false en caso contrario.
     */
    private void storeCheckBoxStatus(boolean isChecked){
        SharedPreferences preferencias = getSharedPreferences("CheckItem", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putBoolean("CheckItem", isChecked);
        editor.commit();
    }

    /**
     * Convierte un timestamp a formato fecha y hora (segun zona horaria del dispositivo).
     * @param timestamp String con el timestamp a convertir.
     * @return String con la representacion de la fecha y hora, con formato dd/MM/yyyy HH:mm.
     */
    public String convertFechaHora(String timestamp){
        long ts;
        String fechaHora;

        try {
            ts = Long.parseLong(timestamp); //convierto string a long
        } catch (Exception e){
            return ""; //Valor default en caso de no poder convertir a Long
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy 'a las' HH:mm", Locale.getDefault());
        fechaHora = dateFormat.format(new Date(ts*1000)); //timestamp en mseg
        //Log.d(TAG, fechaHora);
        return fechaHora;
    }

    /**
     * Obtiene el timestamp actual del sistema.
     * @return long con el timestamp actual
     */
    public long obtenerTimestampActual(){
        long timeLong = System.currentTimeMillis()/1000;
        return timeLong;
    }

    /**
     * Muestra un cuadro de texto con todos los lugares visitados (nombre y fecha) dentro de los 15 dias pasados.
     * Solicita al usuario enviar un correo electronico a cada lugar indicando que es covid positivo.
     * @param lugaresVisitados arreglo que contiene informacion de cada lugar visitado en los ultimos 15 dias
     */
    public void mostrarLugaresVisitados(ArrayList<HashMap<String, String>> lugaresVisitados){
        String msj = "Usted ha visitado los siguientes lugares en los últimos " + UMBRAL_CONTAGIO/CANT_SEG_DIA + " dias.\n";
        for(HashMap<String, String> item : lugaresVisitados) {
            msj += " * " + item.get("lugar") + " (" + convertFechaHora(item.get("fecha")) + " hs)\n";
        }
        msj += "¿Desea enviar un correo electrónico para advertirle sobre su contagio?";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Lugares Visitados");
        builder.setMessage(msj);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //mensaje: Se notifica que el número 3519876543 asistió al establecimiento
                // y presenta test con resultado positivo al SARS-CoV-2.
                String mensajeAvisoCovid;
                ArrayList<String> emailDestinatarios = new ArrayList<>(); //lista que contiene todos los email a los que se debe notificar
                //obtiene el numero de celular del usuario
                numeroTelefono = getNumTelefono();

                mensajeAvisoCovid = "Se notifica que el número "; //se envia el mismo correo a todos los lugares visitados
                mensajeAvisoCovid += numeroTelefono;//numero usuario
                mensajeAvisoCovid += " asistió al establecimiento y presenta test con resultado positivo al SARS-CoV-2.";

                for(HashMap<String, String> item : lugaresVisitados){
                    if(!emailDestinatarios.contains(item.get("email"))){ //no se agrega el mismo correo 2 veces
                        emailDestinatarios.add(item.get("email"));
                    }
                }

                //se convierte el ArrayList a un String[]
                sendEmail(emailDestinatarios.toArray(new String[emailDestinatarios.size()]), mensajeAvisoCovid);
                Log.d(TAG, mensajeAvisoCovid);
               // Toast.makeText(getApplicationContext(), "Mensaje enviado exitosamente", Toast.LENGTH_SHORT).show();

            }
        });
        builder.setNegativeButton("Cancelar", null);
        cuadroLugaresVisit = builder.create();
        cuadroLugaresVisit.show();
    }

    /**
     * Busca en la lista de lugares visitados si se visitó el mismo lugar varias veces,
     * para enviar un solo SMS al lugar con todos los dias y hora en los que se asistió.
     * El lugar de referencia es el primero de la lista.
     * @param lugaresVisitados ArrayList de HashMap con los datos de cada lugar visitado.
     * @param index indice del elemento a comparar
     * @return ArrayList con todas las fechas en las que visitó tal lugar.
     */
    public ArrayList<String> buscarMismoLugar(ArrayList<HashMap<String, String>> lugaresVisitados, int index){
        HashMap<String, String> lugarComparar = lugaresVisitados.get(index); //es con quien se va a comparar
        ArrayList<String> fechasVisita = new ArrayList<>();
        for(int i = index+1; i < lugaresVisitados.size(); i++) {
            if(lugaresVisitados.get(i) != null) {
                if ((lugarComparar.get("lugar").equals(lugaresVisitados.get(i).get("lugar"))) &&
                        (lugarComparar.get("telefono").equals(lugaresVisitados.get(i).get("telefono")))) { //si los lugares y el telefono son iguales
                    fechasVisita.add(lugaresVisitados.get(i).get("fecha")); //tomo la fecha y lo elimino de la lista
                    Log.d(TAG, lugaresVisitados.get(i).get("telefono"));
                    lugaresVisitados.set(i, null); //lo setea a null para no volver a ver los repetidos
                }
            }
        }

        return fechasVisita;
    }

    public void sendEmail(String[] TO, String emailMessage) {
        Log.i(TAG, "Enviar email");
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO); //destinatarios
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Notificación test positivo al SARS-CoV-2.");
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailMessage); //cuerpo del correo


        if(emailIntent.resolveActivity(getPackageManager()) != null){ //chequea si el telefono posee una app que soporte esta accion
            startActivity(emailIntent);  //Sends the Intent
        }else{ //si no encuentra una app le solicita al usuario que seleccione una
            try {
                startActivity(Intent.createChooser(emailIntent, "Enviar correo electrónico..."));
                //finish();
                Log.i(TAG, "Finalizo envio de email");
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "No hay cliente de email instalado.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // Funcionalidad de los botones
    /**
     * Funcionalidad del botón siguiente, chequea que se haya ingresado el numero de telefono,
     * en caso contrario imprime un mensaje de error.
     * Pasa a la vista siguiente (escaneo de código QR).
     * @param view
     */
    public void BtnSiguiente(View view){
        //obtiene el nro de telefono de memoria
        numeroTelefono = getNumTelefono();

        if(numeroTelefono == null || numeroTelefono == "") {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error");
            builder.setMessage("Debe ingresar su número de teléfono antes de seguir.");
            builder.setPositiveButton("Aceptar", null);
            cuadroNumeroNull = builder.create();
            cuadroNumeroNull.show();

        }else{
            qrActivity = new Intent(this, QrScannerActivity.class);
            qrActivity.putExtra("nroCelular", numeroTelefono); //se le pasa el nro de celular a la otra activity
            startActivity(qrActivity); //pasa al activity del scanner Qr
        }
    }

    /**
     * Obtiene de la base de datos los numero de telefono de los lugares asistidos en los ultimos 15 dias
     * y envia un mensaje a cada uno avisando que la persona es covid positivo.
     * Imprime un cuadro de texto para mostrarle al usuario qué lugares asistió en la ultima semana y
     * cuáles serán notificados.
     * @param view
     */
    public void BtnCovidPositivo(View view){
        //lista que contiene los datos (lugar, telefono, email, fecha) del lugar que el usuario visitó en los ultimos 15 dias
        ArrayList<HashMap<String, String>> lugaresVisitados = new ArrayList<>();

        //Accedemos a la base de datos para obtener los lugares visitados en los ultimos 15 dias
        DbOpenHelper dbHelper = new DbOpenHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase(); //se abre para lectura
        if (db != null) {
            //Consultamos los datos
            //SELECT lugar, telefono, email, fecha FROM lugares_visitados WHERE fecha>(HOY-15) AND fecha <= AHORA order by fecha
            String[] CAMPOS = {"lugar", "telefono", "email", "fecha"};
            long ahora = obtenerTimestampActual();
            String condicionWhere = "fecha>" + (ahora - UMBRAL_CONTAGIO) + " AND fecha<=" +  ahora;
            Cursor cursor = db.query("lugares_visitados", CAMPOS, condicionWhere, null, null, null, "fecha", null);

            if ((cursor != null) && (cursor.getCount() > 0)) {
                HashMap<String, String> datosLugar;
                cursor.moveToFirst();
                do {
                    //Asignamos el valor en nuestras variables para usarlos
                    datosLugar = new HashMap<String, String>();
                    datosLugar.put("lugar", cursor.getString(cursor.getColumnIndex("lugar")));
                    datosLugar.put("telefono", cursor.getString(cursor.getColumnIndex("telefono")));
                    datosLugar.put("email", cursor.getString(cursor.getColumnIndex("email")));
                    datosLugar.put("fecha", cursor.getString(cursor.getColumnIndex("fecha")));
                    lugaresVisitados.add(datosLugar);

                    Log.d(TAG, cursor.getString(cursor.getColumnIndex("lugar")) + " " +
                            cursor.getString(cursor.getColumnIndex("telefono")) + " " +
                            cursor.getString(cursor.getColumnIndex("email")) + " " +
                            cursor.getString(cursor.getColumnIndex("fecha")));
                } while (cursor.moveToNext()); //se recorre fila por fila
                mostrarLugaresVisitados(lugaresVisitados); //se muestran los lugares y se solicita enviar un correo electronico

            } else if(cursor.getCount() == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Info");
                builder.setMessage("No hay registros de lugares visitados en los últimos " + UMBRAL_CONTAGIO/CANT_SEG_DIA + " dias.");
                builder.setPositiveButton("Aceptar", null);
                cuadroNoLugaresVisit = builder.create();
                cuadroNoLugaresVisit.show();
            }

            //Cerramos el cursor y la conexion con la base de datos
            cursor.close();
            db.close();
        }
    }

    /**
     * Pasa a la activity NumeroTelefono para que el usuario pueda modificar su numero de celular.
     * @param view
     */
    public void BtnNumeroTelefono(View view){
        numTelActivity = new Intent(this, NumeroTelefonoActivity.class);
        startActivity(numTelActivity);
    }
}
