package com.example.peopletrackingapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Segunda vista, se accede a ella cuando el nro ingresado sea correcto y se presione el botón siguiente.
 * En ella se puede escanear un código QR, volver a la vista anterior y finalizar.
 */
public class QrScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private final String TAG = "QR-Activity";
    private static final int PERMISSIONS_REQUEST_CAMARA = 6;
    private static final int PERMISSIONS_REQUEST_LOCATION = 7;
    private static final int REQUEST_ENABLE_BT = 1;
    private ZXingScannerView escanerView;
    private String resultadoEscaneo = null;
    public static final String CHANNEL_ID = "exampleServiceChannel";

    private String[] namesToFilter = null; //array de nombres a filtrar (en el escaneo bluetooth)
    private String[] macAddresses = null; //array de direcciones MAC a filtar.
    private String[] datosBar = null; //nombre, número de teléfono y email del bar.

    private String nroCelular; //nro de celular del usuario

    private AlertDialog cuadroNoScan; //dialog que se muestra cuando no se escaneó el QR o cuando faltan datos al finalizar el scan.

    private AlertDialog cuadroResultScan; //dialog que muestra el nombre y nro del bar como resultado del escaneo

    private AlertDialog cuadroPasosFinalizados; //dialog que se muestra cuando finalizó la configuración y comienza la emision de beacons.
    /**
     * Método que se ejecuta al crear esta vista.
     * Obtiene el nro de celular de la vista Main y crea el canal de notificaciones para el Foreground Service
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_scanner);
        nroCelular = getIntent().getStringExtra("nroCelular"); //recibe el num de telefono

        createNotificationChannel();
    }

    /**
     * Chequea si la app posee los permisos necesarios para utilizar la cámara, si no se los solicita al usuario
     * mostrándo un mensaje de por qué es importante que se los otorgue.
     */
    public void checkPermisosCamara(){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {// Marshmallow+
            //se chequea si la app ya posee el permiso o no
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                    //se muestra una UI que describe por qué la función que el usuario quiere habilitar necesita un permiso en particular.
                    showExplanation("Permiso necesario",
                                 "Es importante que otorgue permisos a la app para que pueda utilizar la cámara",
                                          Manifest.permission.CAMERA, PERMISSIONS_REQUEST_CAMARA);
                } else { // No se necesita dar una explicación al usuario, sólo pedimos el permiso.
                    //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMARA);
                    requestPermission(Manifest.permission.CAMERA, PERMISSIONS_REQUEST_CAMARA);
                }
            } else { //permiso ya asignado
                startScanQR();
            }
        } else { //versiones viejas, no hace falta el permiso
            startScanQR();
        }
    }

    /**
     * Muesta un cuadro de dialogo en donde se explica por qué es importante otorgar un permiso específico a la app.
     * @param title titulo del cuadro de dialogo
     * @param message mensaje explicativo sobre la importancia del permiso solicitado
     * @param permission nombre del permiso solicitado
     * @param permissionRequestCode codigo que identifica la solicitud de un permiso
     */
    private void showExplanation(String title,
                                 String message,
                                 final String permission,
                                 final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                        //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMARA);
                    }
                });
        builder.create().show();
    }

    /**
     * Inicia un activity en donde solicita mas especificamente el permiso, con botones donde solicita una respuesta explicita.
     * @param permissionName nombre del permiso solicitado
     * @param permissionRequestCode codigo que identifica la solicitud de un permiso
     */
    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }

    /**
     * Chequea si la app posee el permiso de Ubicacion (necesario para el scanner BLE en background)
     * ACCESS_FINE_LOCATION es mas amplio y preciso que ACCESS_COARSE_LOCATION.
     * Se chequea ACCESS_FINE_LOCATION ya que el ACCESS_BACKGROUND_LOCATION no me imprime el Dialog
     * al momento de solicitar el permiso.
     */
    public void checkPermisoUbicacion(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Solicita permiso ubicacion");
            requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, PERMISSIONS_REQUEST_LOCATION);
        } else { //si ya esta habilitado se chequea el Bluetooth
            checkBluetoothActivado();
        }
    }

    /**
     * Método de callback que se invoca cuando el usuario otorga o no los permisos solicitados para la
     * ejecución de la app.
     * Si el permiso para utilizar la cámara fue aceptado, se procede a realizar el escaneo.
     * @param requestCode codigo del permiso solicitado
     * @param permissions
     * @param grantResults
     */
    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CAMARA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permiso Camara aceptado!", Toast.LENGTH_SHORT).show();
                    startScanQR();
                } else {
                    Toast.makeText(this, "Permiso Camara denegado!", Toast.LENGTH_SHORT).show();
                }
                break;

            case PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permiso Ubicacion aceptado!", Toast.LENGTH_SHORT).show();
                    checkBluetoothActivado(); //se procede a chequear el Bluetooth
                } else {
                    Toast.makeText(this, "Permiso Ubicacion denegado!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /**
     * Activa la cámara para realizar el escaneo del código QR.
     */
    public void startScanQR(){
        escanerView = new ZXingScannerView(this);
        setContentView(escanerView);
        escanerView.setResultHandler(this);
        escanerView.startCamera();
    }

    /**
     * Divide un String en varios String usando el delimitador pasado como argumento.
     * Para cada substring, elimina los \n, espacios, \t, \r al comienzo y final del mismo.
     * No toma los string vacios.
     * @param cadena String a dividir.
     * @param delimitador String que contiene el token o expresión regular que indica cómo se debe dividir la cadena.
     * @return el array de Strings generado al dividir el String cadena.
     */
    public String[] dividirString(String cadena, String delimitador){
        String[] partes;
        List<String> datosNoVacios = new ArrayList<>();

        partes = cadena.split(delimitador); //los strings estan separados por === y \n
        for(String p : partes){ //quito los \n, espacios, \t, \r
            if(!p.trim().equals("")){ //no se toman los string vacios
                datosNoVacios.add(p.trim());
            }
        }
        return datosNoVacios.toArray(new String[0]);
    }

    /**
     * Método que se invoca al escanear un código QR.
     * Obtiene los datos escaneados y separa los nombres de las direcciones MAC.
     * Imprime los datos del bar (nombre, teléfono y email) obtenidos del QR.
     * @param result resultado del escaneo.
     */
    @Override
    public void handleResult(Result result) {
        resultadoEscaneo = result.getText();

        escanerView.stopCamera();
        setContentView(R.layout.activity_qr_scanner); //volvemos a la vista anterior

        String[] partes;
        //separamos nombres de las direcciones mac y el nombre y telefono del bar (1ero aparecen el nombre y telefono del bar)
        partes = dividirString(resultadoEscaneo, "===");

        //el texto del codigo QR debe consistir de 3 partes (datos bar | nombre ESP32 | MAC addresses)
        if(partes.length != 3){
            printSimpleAlertDialog("Error", "Formato de Código QR incorrecto.");
            Log.e("handleResult", "Formato de Código QR incorrecto - Error al parsear texto");
            return;
        }

        datosBar = dividirString(partes[0], "\\n"); //separamos el nombre del bar con el numero de telefono y email del mismo
        namesToFilter = dividirString(partes[1], "\\n"); //separamos los nombres entre si
        macAddresses = dividirString(partes[2], "\\n"); //separamos MAC addr entre si

        if(!(checkDatosBar(datosBar) && checkNombresNodosEscaner(namesToFilter) && checkFormatMACAddress(macAddresses))){
            Log.e("handleResult", "Formato de Código QR incorrecto - Error al chequear los datos del QR");
            printSimpleAlertDialog("Error", "Formato de Código QR incorrecto. \n" +
                    "No se pudieron obtener los datos.");
            return;
        }

        printDatosBar(datosBar);
    }

    /**
     * Los datos del bar serán correctos si estan los 3 datos y al menos 1 de los 3 es correcto.
     * @param datosBar array de String con los datos del bar.
     * @return true si al menos 1 dato de los 3 presentes es correcto.
     */
    private Boolean checkDatosBar(String[] datosBar){
        if(datosBar != null && datosBar.length == 3) { //deben ser 3 datos (nombre del bar, nro de telefono y email)
            if(!checkTelephoneNumber(datosBar[1])){ //los datos incorrectos se setean a un '-'
                datosBar[1] = "-";
                Log.e("checkDatosBar", "Numero de telefono incorrecto");
            }
            if(!checkEmail(datosBar[2])){
                datosBar[2] = "-";
                Log.e("checkDatosBar", "Formato de email incorrecto");
            }
            return true;
        }
        return false;
    }

    /**
     * Chequea que se haya suministrado en el QR el nombre de los nodos que escanean.
     * @param namesToFilter nombres obtenidos del QR.
     * @return true si se ha suministrado al menos un nombre.
     */
    private Boolean checkNombresNodosEscaner(String[] namesToFilter){
        return namesToFilter != null && namesToFilter.length > 0;
    }

    /**
     * Chequea que todas las mac address propuestas tengan el formato correcto.
     * Al menos 1 mac address debe ser especificada.
     * @param macAddresses Array de mac address
     * @return true si hay al menos una mac address y TODAS las especificadas poseen un formato correcto.
     */
    private Boolean checkFormatMACAddress(String[] macAddresses){
        if(macAddresses != null && macAddresses.length > 0){
            Boolean check = true;
            for(String mac : macAddresses){
                check &= checkMACAddress(mac);
            }
            return check;
        }
        return false;
    }

    /**
     * Chequea si el formato del email es correcto.
     * @param email email a chequear.
     * @return true si es correcto.
     */
    private Boolean checkEmail(String email){
        Pattern pattern = Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
        Matcher matcher = pattern.matcher(email);
        return matcher.find();
    }

    /**
     * Chequea si el numero de telefono son 10 digitos numericos.
     * @param number numero a chequear.
     * @return true si es correcto, falso en caso contrario.
     */
    private Boolean checkTelephoneNumber(String number){
        Pattern pattern = Pattern.compile("[0-9]{10}");
        Matcher matcher = pattern.matcher(number);
        return matcher.find();
    }

    /**
     * Chequea si el formato de la MAC address es correcto.
     * @param macAddr MAC address a chequear.
     * @return true si es correcto.
     */
    private Boolean checkMACAddress(String macAddr){
        Pattern pattern = Pattern.compile("([0-9A-F]{2}:){5}[0-9A-F]{2}");
        Matcher matcher = pattern.matcher(macAddr);
        return matcher.find();
    }

    /**
     * Chequea que se haya escaneado en el código QR el nombre, numero de telefono y email del Bar.
     * Todos los datos deben estar, si no hay alguno se debe indicar con un '-' en el QR
     * pero no puede quedar vacio.
     * Imprime un mensaje mostándole tal información al cliente o un mensaje de error si hubo alguno.
     */
    public void printDatosBar(String[] datosBar){
        Toast.makeText(this, "Código QR escaneado correctamente.", Toast.LENGTH_SHORT).show();
        insertarDatosBDD(datosBar[0], datosBar[1], datosBar[2], obtenerTimestampActual()); //guardo en la BDD

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Información del establecimiento asistido");
        builder.setMessage( "Lugar: " + datosBar[0] + "\n" +
                            "Teléfono: " + datosBar[1] + "\n" +
                            "Email: " + datosBar[2] + "\n\n" +
                            "Es muy importante que guarde estos datos, ya que los puede necesitar en un futuro.\n" +
                            "#NosCuidamosEntreTodos");
        builder.setPositiveButton("Agregar contacto", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                guardarContacto();
            }
        });
        builder.setNegativeButton("Cancelar", null);
        cuadroResultScan = builder.create();
        cuadroResultScan.show();
    }

    public void printSimpleAlertDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage( message);
        builder.setPositiveButton("Aceptar", null);
        cuadroResultScan = builder.create();
        cuadroResultScan.show();
    }

    /**
     * Obtiene el timestamp actual del sistema.
     * @return long con el timestamp actual
     */
    public long obtenerTimestampActual(){
        long timeLong = System.currentTimeMillis()/1000;
        return timeLong;
    }

    /**
     * Genera la vista para guardar un contacto en la agenda, con el nombre y numero del establecimiento.
     */
    public void guardarContacto(){
        // Creates a new Intent to insert a contact
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        // Sets the MIME type to match the Contacts Provider
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        /*
         * Inserts new data into the Intent. This data is passed to the
         * contacts app's Insert screen
         */
        intent.putExtra(ContactsContract.Intents.Insert.NAME, datosBar[0]); //guarda el nombre del establecimiento como contacto

        // Inserts a phone number
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, datosBar[1]) //guarda el numero de telefono
                .putExtra(ContactsContract.Intents.Insert.PHONE_TYPE,
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);

        //inserta el email
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, datosBar[2])
                .putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE,
                ContactsContract.CommonDataKinds.Email.TYPE_WORK);

        if(intent.resolveActivity(getPackageManager()) != null){ //chequea si el telefono posee una app que soporte esta accion
            startActivity(intent);  //Sends the Intent
        }else{ //si no encuentra una app le solicita al usuario que seleccione una
            try {
                startActivity(Intent.createChooser(intent, "Guardar contacto..."));
                Log.i(TAG, "Finalizo guardar contacto");
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "No hay app que soporte esta acción. Por favor ingrese el contacto manualmente.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Crea el canal para el Foreground Service.
     */
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { //si es mayor a Android Oreo+
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Example Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    /**
     * Inserta los datos de un lugar en la base de datos 'lugares_visitados'
     * @param lugar String con el nombre del lugar visitado
     * @param telefono String numero de telefono
     * @param email String email del lugar
     * @param fecha long con el timestamp del momento en que se realizo la visita
     */
    public void insertarDatosBDD(String lugar, String telefono, String email, long fecha){
        DbOpenHelper dbHelper = new DbOpenHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase(); //se abre BDD para escritura
        if (db != null) {
            // Insert con ContentValues
            ContentValues cv = new ContentValues();
            cv.put("lugar", lugar);
            cv.put("telefono", telefono);
            cv.put("email", email);
            cv.put("fecha", fecha);
            db.insert("lugares_visitados", null, cv);
        }
    }

    // Funcionalidad de los botones
    /**
     * Funcionalidad del botón escanear.
     * Chequea los permisos de la cámara y, si se cumplen, comienza el escaneo QR
     * @param view
     */
    public void BtnEscanearQR(View view){
        checkPermisosCamara();
    }

    /**
     * Funcionalidad del botón volver. Vuelve a la vista anterior (Main).
     * @param view
     */
    public void BtnVolver(View view){
        finish(); //finaliza el activity actual, vuelve al anterior
    }

    /**
     * Funcionalidad del botón finalizar.
     * Si se pudieron escanear correctamente los nombres y direcciones MAC entonces comienza el servicio en background.
     * Caso contrario se imprime un mensaje para que se vuelva a escanear.
     * @param view
     */
    public void BtnFinalizar(View view){
        if(namesToFilter == null || macAddresses == null || datosBar == null) { //si no se escaneó se muestra un dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error");
            builder.setMessage("Debe escanear el código QR antes de finalizar.");
            builder.setPositiveButton("Aceptar", null);
            cuadroNoScan = builder.create();
            cuadroNoScan.show();
        } else {
            checkPermisoUbicacion();
           // checkBluetoothActivado();
           // startService();
        }
    }

    /**
     * Chequea si el adaptador bluetooth está habilitado, si no lo está le solicita al usuario que lo active.
     */
    public void checkBluetoothActivado(){
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if(adapter == null){ //el dispositivo no soporta Bluetooth
            Toast.makeText(this, "ERROR: Bluetooth no soportado.", Toast.LENGTH_LONG).show();
        } else if (!adapter.isEnabled()) { //Bluetooth no esta habilitado
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else { //Bluetooth soportado y habilitado
            printConfiguracionTerminada();
            startService(); //comienza servicio
        }
    }

    /**
     * Genera un cuadro de texto que le indica al usuario que ya termino la configuracion
     * y que puede minimizar la app.
     */
    public void printConfiguracionTerminada(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Configuración finalizada");
        builder.setMessage("¡Excelente! Ha finalizado la etapa de configuración correctamente. Ya puede minimizar la App y recuerde no apagar el Bluetooth.");
        builder.setPositiveButton("Ok, entendido", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                QrScannerActivity.this.moveTaskToBack(true); //se minimiza la app al pulsar Aceptar
            }
        });
        cuadroPasosFinalizados = builder.create();
        cuadroPasosFinalizados.show();
    }

    /**
     * Callback que se ejecuta cuando la activity iniciada retorna un resultado.
     * La Activity es llamada con startActivityForResult.
     * @param requestCode
     * @param resultCode
     * @param result
     */
    @SuppressLint("MissingSuperCall")
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == REQUEST_ENABLE_BT) {  // Match the request code
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Bluetooth encendido", Toast.LENGTH_SHORT).show();
                printConfiguracionTerminada();
                startService(); //arranca el servicio solo si esta el Bluetooth encendido
            }
        }
    }

    /**
     * Arranca el servicio en background (Foreground Service en clase BLEService).
     * Le transfiere los nombres y direcciones MAC de los ESP32 y el nro de celular del usuario.
     */
    public void startService() { //arranca servicio en background
        Intent serviceIntent = new Intent(this, BLEService.class);
        serviceIntent.putExtra("nombres",  namesToFilter); //se le pasan los arrays al Foreground Service
        serviceIntent.putExtra("macAddress",  macAddresses);
        serviceIntent.putExtra("numCelular", nroCelular);
        ContextCompat.startForegroundService(this, serviceIntent);
        Toast.makeText(this, "Start Foreground Service", Toast.LENGTH_SHORT).show();
    }

}