package com.example.peopletrackingapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Vista que permite la modificación del numero de telefono del usuario.
 * Contiene una caja de texto para que el usuario inserte su número de celular.
 */
public class NumeroTelefonoActivity extends AppCompatActivity {

    private EditText contNumTelefono;
    private String numeroTelefono = null;
    private final String TAG = "INFO-NumeroTelefono";
    //private Intent qrActivity = null;

    private AlertDialog cuadroBienvenida;  //dialog que se muestra 1 vez al iniciar la app

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numero_telefono);

        contNumTelefono = findViewById(R.id.cajaTelefono); //se asocia con el cuadro de entrada de la vista principal

        //busco en memoria el nro de celular guardado previamente y lo cargo en el cuadro de texto
        SharedPreferences preferences = getSharedPreferences("nroCelular", Context.MODE_PRIVATE);
        contNumTelefono.setText(preferences.getString("nroCelular",""));
    }

    /**
     * Chequea si el numero de telefono posee 10 digitos (tiene que incluir si o si la caracteristica, asi sea 351 Cba).
     * @param numero string numero de celular
     * @return True si posee la cantidad de digitos correctos, False en caso contrario.
     */
    public boolean checkNumTelefono(String numero){
        if(numero.length() == 10){
            return true;
        } else {
            return false;
        }
    }

    // Funcionalidad de los botones
    /**
     * Funcionalidad del botón guardar, chequea que el número ingresado sea de la cantidad de
     * digitos correctos, en caso contrario imprime un mensaje de error.
     * Si el número es correcto, se guarda y se vuelve a la vista principal.
     * @param view
     */
    public void BtnGuardar(View view){
        if(checkNumTelefono(String.valueOf(contNumTelefono.getText()))){ //si el numero es correcto
            numeroTelefono = String.valueOf(contNumTelefono.getText()); //obtengo el nro de telefono del cuadro de texto

            //guardo el nro de telefono en una variable en disco
            SharedPreferences preferencias = getSharedPreferences("nroCelular", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferencias.edit();
            editor.putString("nroCelular", numeroTelefono);
            editor.commit();
            Toast.makeText(this, "Datos guardados correctamente", Toast.LENGTH_SHORT).show();

            finish(); //vuelvo a la vista anterior

            //Log.d(TAG, String.valueOf(contNumTelefono.getText()));
        } else if(contNumTelefono.getText().toString().isEmpty()) { //verifica si esta el EditText vacio
            contNumTelefono.setError("¡Debe ingresar su número de teléfono!");
        } else { //si el numero no posee la cantidad de digitos correcta
            contNumTelefono.setError("El número debe contener 10 digitos");
        }
    }

    /**
     * Vuelve a la pantalla de inicio sin guardar los cambios.
     * @param view
     */
    public void BtnCancelar(View view){
        finish(); //vuelvo a la vista anterior (Main) sin guardar.
    }
}