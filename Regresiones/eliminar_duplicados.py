import os

if __name__ == "__main__":
    path = str(input("Inserte path al archivo:"))
    f_entrada = open(path)
    f_resultado = open("output.csv","w")
    linea_previa = ""
    for linea in f_entrada:
        if(linea != linea_previa):
            f_resultado.write(linea)
        linea_previa = linea 
    f_entrada.close()
    f_resultado.close()
    os.rename("output.csv", path)