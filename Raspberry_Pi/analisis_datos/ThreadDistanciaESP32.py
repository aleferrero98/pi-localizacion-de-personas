from sqlite3.dbapi2 import Timestamp
from threading import Thread
import BaseDeDatosDistancia as bdd
import sys
sys.path.append("../")
import BaseDeDatos as bdd_rssi
from ConstantesPath import ConstantesPath

"""Clase que se encarga del calculo de las distancias entre una ESP32 y 
los diversos dispositivos que detecto respecto a sus timestamps"""

class ThreadDistanciaESP32(Thread):
    """Clase que funciona como hilo, donde recibe el nombre de la ESP32 y calcula las distancias
    almacenandolas en la base de datos, especificamente en la tabla de DIST_DISPOSITIVOS
    """
  
    def __init__(self, nameEsp32, path_bdd_rssi, path_bdd_distancias, A, n):
        """ Inicializa atributos y se conecta a las bases de datos.

        :param nameEsp32: Nombre de ESP32
        :type nameEsp32: str
        :param path_bdd_rssi: path de bdd de RSSI
        :type path_bdd_rssi: str
        :param path_bdd_distancias: path de bdd de Distancias
        :type path_bdd_distancias: str
        """
        Thread.__init__(self)  
        self.nameESP32 = nameEsp32
        self.base_datos_distancias = bdd.BaseDeDatosDistancia(path_bdd_distancias)
        self.base_datos_rssi = bdd_rssi.BaseDeDatos(path_bdd_rssi)
        self.A = A
        self.n = n
       
    def run(self):
        """ Metodo principal del hilo ThreadDistanciaESP32.

        Se encarga de calcular los rssi suavizados para los distintos dispositivos y las distancias
        respecto a la ESP32 en los distintos timestamp 
        """
        #Se obtienen las direcciones MAC para el cliente ESP32
        macs = self.base_datos_rssi.obtenerMacs(self.nameESP32)     
        #Se recorren las direcciones MAC y se calcula el RSSI suavizado para cada RSSI.
        for mac in macs:
            # Se obtiene un arreglo con los pares RSSI,TIMESTAMP de una MAC
            # rssi es una lista de tuplas: [(RSSI, TIMESTAMP),...]
            rssi = self.base_datos_rssi.ejecutarSelect("SELECT RSSI, TIMESTAMP FROM MEDICIONES_RSSI WHERE ID_CLIENTE='" + str(self.nameESP32) +"' and MAC='" + str(mac[0]) + "'") 
            
            # Calculo el RSSI suavizado
            # rssi_suavizado es una lista de tuplas: [(RSSI, TIMESTAMP),...]
            rssi_suavizado = self.suavizarRSSI(rssi)
            
            # Obtengo lista distancias con cada RSSI suavizado
            # registros es una lista de tuplas que contiene todas las filas a insertar en la base de datos
            # es de la forma [(Name_Esp32, mac, distancia, timestamp),...]
            registros = []
            for i in range(0, len(rssi_suavizado)):
                dist = self.calcular_distancia(rssi_suavizado[i][0])
                timestamp = rssi_suavizado[i][1]
                registros.append((self.nameESP32, mac[0], dist, timestamp))
                
            # Insertan varias filas en la base de datos
            self.base_datos_distancias.insertarRegistros(registros)

        self.base_datos_distancias.finalizarConexion()
        self.base_datos_rssi.finalizarConexion()



    def calcular_distancia(self, rssi):
        """ Calcula la distancia en base al RSSI obtenido, el RSSI de referencia y el PATH_LOSS

        :param rssi: RSSI con el que se desea calcular la distancia
        :type rssi: int
        :returns: Distancia a cierto RSSI
        :rtype: float
        """
        distancia = 10**((self.A-rssi)/(10*self.n))
        distancia = round(distancia, 3) # redondea a 3 decimales
        return distancia

    def suavizarRSSI(self, mediciones):
        """ Calcula el RSSI suavizado para las distintas mediciones

        Genera otra lista de RSSI suavizado ya que va acumulando el suavizado para las diferentes mediciones
        :param mediciones: lista de mediciones de RSSI
        :type mediciones: list
        :returns: Lista con los RSSI suavizados
        :rtype: list
        """
        rssi_suavizado = []
        rssi = 0
  
        for medicion in mediciones:
            if(rssi == 0):
                rssi = float(medicion[0])
            else:
                rssi = ConstantesPath.ALFA * float(medicion[0]) + (1 - ConstantesPath.ALFA) * rssi
                rssi = round(rssi, 3) # redondea a 3 decimales
            rssi_suavizado.append((rssi, medicion[1]))
        return rssi_suavizado


if(__name__ == "__main__"):
    hilo = ThreadDistanciaESP32("ESP32 - Bart", ConstantesPath.BDD_RSSI, ConstantesPath.BDD_DISTANCIAS, ConstantesPath.RSSI_REFERENCE_INDOOR, ConstantesPath.PATH_LOSS_INDOOR)
    hilo.start()
    hilo.join()