#!/usr/bin/python3
#-*- coding: utf-8 -*-

import sqlite3
from sqlite3 import Error

"""Clase que permite el manejo de una Base de datos para guardar distancias de los dispositivos
a los diferentes ESP32"""

class BaseDeDatosDistancia:
    """ Clase Base de datos que permite el manejo de la misma, es de tipo SQLite3. 

    Permite la creacion y manejo de una tabla:
    - DIST_DISPOSITIVOS: va a permitir almacenar las distancias de los dispositivos encontrados respecto a las 
    ESP32
    """

    def __init__(self, path):
        """ Inicializador de instancias de la clase BaseDeDatosDistancia.

        Establece una conexión y crea las tablas.
        :param path: ruta a la base de datos (archivo.db)
        :type path: str
        """
        #print("Creando base de datos..")
        try:
            self.conexionBdd = sqlite3.connect(path, check_same_thread=False) #path a la base de datos
            #print("Conexión establecida, Base de datos creada en memoria.")
        except Error:
            print(Error)
        self.conexionBdd.execute("PRAGMA foreign_keys = 1") #para habilitar las foreign keys
        self.cursorBdd = self.conexionBdd.cursor()
        self.crearTablaDistancias()
    
    def crearTablaDistancias(self):
        """ Crea tabla de distancias de los dispositivos con respecto a las ESP32.

        La primary key debe ser un ID, para que se permitan filas con el mismo NAME_ESP32, MAC_DISP y TIMESTAMP. 
        """
        self.cursorBdd.execute('''CREATE TABLE IF NOT EXISTS DIST_DISPOSITIVOS 
                                  (ID integer PRIMARY KEY autoincrement, 
                                  NAME_ESP32 text, MAC_DISP text not null, DISTANCIA real, TIMESTAMP integer)''')
        self.conexionBdd.commit() #confirmamos los cambios a realizar 

    def finalizarConexion(self):
        """ Cierra la conexion con la BDD. """
        self.conexionBdd.close()
 

    def insertarDistancia(self, distancia, mac, timestamp, nameEsp32):
        """ Inserta una nueva entrada en la tabla de DIST_DISPOSITIVOS

        La entrada va a consistir en una distancia en la que se encuentra un dispositivo y una ESP32 con
        el timestamp.
        Inserta una sola fila.

        :param distancia: distancia a insertar en la tabla
        :type distancia: int
        :param mac: direccion MAC del dispositivo
        :type mac: str
        :param timestamp: tiempo en que se encontraron a dicha distancia
        :type timestamp: int
        :param nameEsp32: nombre de la ESP32
        :type nameEsp32: str
        """
        
        #Inserto
        comando = 'INSERT INTO DIST_DISPOSITIVOS(NAME_ESP32, MAC_DISP, DISTANCIA, TIMESTAMP) VALUES(?, ?, ?, ?)'
        self.cursorBdd.execute(comando, (nameEsp32,mac,distancia,timestamp))
        self.conexionBdd.commit() #confirmamos los cambios a realizar   

    def insertarRegistros(self, registros):
        """ Inserta de a varios registros(filas) en la tabla DIST_DISPOSITIVOS en una única consulta.

        :param registros: lista de tuplas donde cada tupla corresponde a una fila de la base de datos.
        :type registros: list
        """
        comando = 'INSERT INTO DIST_DISPOSITIVOS(NAME_ESP32, MAC_DISP, DISTANCIA, TIMESTAMP) VALUES(?, ?, ?, ?)'
        #insert multiple records in a single query
        self.cursorBdd.executemany(comando, registros)
        #print('We have inserted', self.cursorBdd.rowcount, 'records to the table.')
        self.conexionBdd.commit() #confirmamos los cambios a realizar         


    def checkEntradaDistancia(self, mac, timestamp, nameEsp32):
        """ Chequea si existe una entrada en la tabla DIST_DISPOSITIVOS para un dispositivo
        en un cierto timestamp.

        :param mac: direccion MAC del dispositivo
        :type mac: str
        :param timestamp: timestamp del dispositivo
        :type timestamp: int
        :param nameEsp32: nombre de ESP32 
        :type nameEsp32: str
        """
        ret = self.leerDatos('DIST_DISPOSITIVOS', mac, timestamp, nameEsp32)
        if(ret == []):
            return False
        else: 
            return True

    def leerDatos(self, tabla, mac, timestamp, nameEsp32):
        """Obtiene de la base de datos los datos guardados para un dispositivo en una tabla.
        
        :param tabla: nombre de la tabla
        :type tabla: str
        :param mac: direccion MAC (primary key)
        :type mac: str
        :param timestamp: timestamp de la mac
        :type timestamp: int
        :param nameEsp32: nombre de la ESP32
        :type nameEsp32: str
        :returns: Lista con los datos obtenidos
        :rtype: list
        """
        if(not(self.tablaExists(tabla))): #si no existe una tabla con ese nombre se lanza una excepcion
            raise Exception("No existe una tabla llamada '" + str(tabla) + "'")
        
        #Esto deberia retornar DISTANCIAS
        comando = "SELECT * FROM " + str(tabla) + " WHERE MAC_DISP='" + str(mac) + "' AND TIMESTAMP='" + str(timestamp) + "' AND NAME_ESP32='" + str(nameEsp32) + "'"
        #print(comando)
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        #for row in datos:
         #   print(row)
        return datos

    def actualizarDatos(self, tabla, atributo, nuevoValor, mac, timestamp, nameEsp32):
        """ Actualiza los datos de una entrada en una tabla de la base de datos. 
        
        :param tabla: nombre de la tabla
        :type tabla: str
        :param atributo: atributo a actualizar en la tabla (Columna)
        :type atributo: str
        :param nuevoValor: valor el atributo a actualizar
        :type atributo: int
        :param mac: direccion MAC del dispositivo el cual vamos a actualizar datos
        :type mac: str
        :param timestamp: Timestamp de la medicion del dispositivo del cual vamos a actualizar datos
        :type timestamp: int
        :param nameEsp32: identificador de la ESP32 cliente del dispositivo sobre el cual vamos a actualizar datos
        :type nameEsp32: str
        """
        if(not(self.tablaExists(tabla))): #si no existe una tabla con ese nombre se lanza una excepcion
            raise Exception("No existe una tabla llamada '" + str(tabla) + "'")
        
        #print(nuevoValor)
        comando = "UPDATE " + str(tabla) + " SET " + str(atributo) + "='" + str(nuevoValor) + "' WHERE MAC_DISP='" + str(mac) + "' AND TIMESTAMP='" + str(timestamp) + "' AND NAME_ESP32='" + str(nameEsp32) + "'"       
        #print(comando)
        self.cursorBdd.execute(comando)
        self.conexionBdd.commit()
        
    def tablaExists(self, nombreTabla):
        """ Chequea si esxiste una tabla en la BDD con el nombre indicado.

        :param nombreTabla: nombre de la tabla a verificar
        :type nombreTabla: str
        :returns: True si existe una tabla con dicho nombre, False en caso contrario.
        :rtype: bool
        """
        self.cursorBdd.execute("SELECT name from sqlite_master WHERE type='table' AND name='" + str(nombreTabla) + "'")
        ret = self.cursorBdd.fetchall()
        if(ret == []):
            return False
        else: 
            return True

    def obtenerMacESP32(self, NameEsp32):
        """ Obtiene las direcciones MAC detectadas de una ESP32 determinada

        :param NameEsp32: nombre de la ESP32
        :type NameEsp32: str
        :returns: Lista de las direcciones MAC detectadas por la ESP32 (lista de tuplas)
        :rtype: list
        """
        comando = "SELECT distinct MAC_DISP FROM DIST_DISPOSITIVOS WHERE NAME_ESP32='" + str(NameEsp32) +"'"
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        return datos
    
    def obtenerMacs(self):
        """ Obtiene todas las direcciones MAC(sin repetir) de la tabla DIST_DISPOSITIVOS

        :returns: Lista de las direcciones MAC(lista de tuplas)
        :rtype: list
        """
        comando = "SELECT distinct MAC_DISP FROM DIST_DISPOSITIVOS"
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        return datos
    
    def obtenerESP32Name(self, mac):
        """ Obtiene los nombres de las ESP32 que detectaron una MAC determinada

        :param mac: direccion MAC del dispositivo
        :type mac: str
        :returns: Lista de los nombres de ESP32 que detectaron el dispositivo (lista de tuplas)
        :rtype: list
        """
        comando = "SELECT distinct NAME_ESP32 FROM DIST_DISPOSITIVOS WHERE MAC_DISP='" + str(mac) +"'"
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        return datos
    
    def obtenerDistanciaAndTimestamp(self, mac):
        """ Obtiene la distancia, Esp32 cliente y timestamp para un dispositivo determinado

        :param mac: direccion MAC del dispositivo
        :type mac: str
        :returns: Lista de tuplas, cada entrada con la distancia, nombre ESP32 y timestamp para un dispositivo
        :rtype: list
        """
        comando = "SELECT DISTANCIA,NAME_ESP32,TIMESTAMP FROM DIST_DISPOSITIVOS WHERE MAC_DISP='" + str(mac) +"'"
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        return datos
    
    def ejecutarSelect(self, comando):
        """ Ejecuta un SELECT en la base de datos y devuelve la lista con los resultados.

        :param comando: comando select a ejecutar.
        :type comando: str.
        :returns: Lista con el resultado del SELECT.
        :rtype: list.
        """
        if(str(comando).startswith("select") or str(comando).startswith("SELECT")):
            self.cursorBdd.execute(str(comando))
            datos = self.cursorBdd.fetchall()
            return datos
        else:
            raise Exception("El comando '" + str(comando) + "' no es una instrucción SELECT" )