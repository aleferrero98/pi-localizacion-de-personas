import BaseDeDatosDistancia as bdd_distancias
from threading import Thread,Lock
import math
import sys
from ConstantesPath import ConstantesPath
sys.path.append("../")
import BaseDeDatos as bdd

"""Clase que se encarga determinar la cercania de un dispositivo infectado
respecto a otro posiblemente infectado"""

class ThreadCercania(Thread):
    """Clase que funciona como hilo, donde recibe un arreglo de coordenadas del dispositivo
    infectado y uno del dispositivo posiblemente infectado y determina si puede existir riesgo
    de contagio
    """
    def __init__(self, coordenadasInfectado, coordenadasMac, infectados, mutex, macPosibleInfectado):
        Thread.__init__(self)
        #lista de coordenadas del disp infectado
        self.coordenadasInfectado = coordenadasInfectado
        #lista de coordenadas del disp posiblemente infectado
        self.coordenadasMac = coordenadasMac
        #Base de datos de distancias y de rssi
        self.base_datos_distancias = bdd_distancias.BaseDeDatosDistancia(ConstantesPath.BDD_DISTANCIAS)
        self.base_datos_rssi = bdd.BaseDeDatos(ConstantesPath.BDD_RSSI)
        #lista de infectados 
        self.infectados = infectados
        #mutex para la lista de infectados
        self.mutex = mutex
        #mac del posiblemente infectado
        self.macPosibleInfectado = macPosibleInfectado

    def run(self):
        """Esta funcion se encarga de determinar la cercania del dispositivo con el dispositivo infectado
        Si encuentra 3 match de una distancia menor a dos metros, almacena el numero de telefono o MAC 
        y lo guarda en un arreglo global.
        """
        #print("Longitud de arreglo de coordenadas infectado1: ", len(self.coordenadasInfectado[0]))
        #print("Longitud de arreglo de coordenadas infectado2: ", len(self.coordenadasInfectado[1]))
        #print("Longitud de arreglo de coordenadas Mac: ", len(self.coordenadasMac))

        #Forma coordenadas -> [[(x,y,timestamp),...,(x,y,timestamp)],...,[(x,y,timestamp),...,(x,y,timestamp)]]
        matchs = 0
        for mac_infectado in self.coordenadasInfectado:
            for dato in mac_infectado:
                for coord in self.coordenadasMac:
                    if(abs(dato[2] - coord[2]) < ConstantesPath.UMBRAL_TIMESTAMP ):
                        #print("Esta calculando")
                        distancia = self.calcDistanciaEntre2(coord[0],dato[0],coord[1],dato[1]) 
                        if(distancia <= ConstantesPath.DISTANCIA_CONTAGIO):
                            if(matchs < ConstantesPath.CANT_MATCH):
                                matchs = matchs + 1
                            else:
                                print("Entro")
                                #matchs = 0
                                telefono2 = self.base_datos_rssi.obtenerTelefono(self.macPosibleInfectado)
                                #print("Tel:", telefono2)
                                self.mutex.acquire()
                                try:
                                    if(telefono2[0][0] != ''):
                                        self.infectados.append(telefono2[0][0])
                                    else:
                                        self.infectados.append(self.macPosibleInfectado)
                                finally:
                                    self.mutex.release()
                                return
        
    def calcDistanciaEntre2(self,x1, y1, x2, y2):
        """Calcula la distancia entre dos dispositivos dadas sus coordenadas X e Y

        :param x1: coordenada X1
        :type x1: str
        :param y1: coordenada Y1
        :type y1: str
        :param x2: coordenada X2
        :type x2: str
        :param y2: coordenada Y2
        :type y2: str
        :returns: distancia entre dos dispositivos
        :rtype: float
        """
        return math.sqrt((x2-x1)**2 + (y2-y1)**2)
