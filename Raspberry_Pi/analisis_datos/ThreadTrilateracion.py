from threading import Thread
import trilateracion as t
import sqlite3
import random
import os
import signal
from threading import Thread
from subprocess import check_output
from ConstantesPath import ConstantesPath
import math
import sys
import time
import BaseDeDatosDistancia as bdd_distancias
sys.path.append("../")
import BaseDeDatos as bdd

"""Clase que se encarga de establecer un hilo para determinar coordenadas de un dispositivo
en base a la ventana de tiempo recibida.
"""
class ThreadTrilateracion(Thread):
    """Este hilo se encarga de calcular las de un dispositivo dentro de un rango de timestamp, este rango
    proviene del tiempo en que dispositivo infectado fue detectado en el sistema.
    """
    def __init__(self, coordenadas, mac, minVentanaGlobal, maxVentanaGlobal, coordenadasCalculadas, mutex):
        Thread.__init__(self) 
        #Arreglo de coordenadas configuradas 
        self.coordenadas = coordenadas
        #Mac de posible infectado sobre la cual se va a calcular trilateracion
        self.mac = mac
        #Arreglo de ventana de nombres ESP32 que detectaron el dispositivo
        self.nameVentana = []
        #Arreglo de coordenadas ajustadas al eje de referencia
        self.coordESP32Ordenadas = [] 
        #Desplazamientos en X e Y para ajuste de coordenadas
        self.coordX = 0 
        self.coordY = 0 
        #Base de datos de distancias utilizadas
        self.base_datos_distancias = bdd_distancias.BaseDeDatosDistancia(ConstantesPath.BDD_DISTANCIAS)
        #Referencia a trilateracion
        self.trilateracion = t.Trilateracion()
        #Ventanas minima y maxima de timestamp del dispositivo infectado
        self.minVentanaGlobal = minVentanaGlobal
        self.maxVentanaGlobal = maxVentanaGlobal
        #Arreglo para almacenar las coordenadas del dispositivo posiblemente infectado
        self.listaCoordenadas = [] 
        #Arreglo de coordenadas global donde se almacenara la lista de coordenadas del disp 
        self.coordenadasCalculadas = coordenadasCalculadas
        #Mutex para escribir en el arreglo global de coordenadas calculadas
        self.mutex = mutex
        #Arreglo ESP32 centro
        self.arrCentro = []
        #Arreglo ESP32 punto aislado
        self.arrPuntoAislado = []
        #Arreglo ESP32 mismo eje
        self.arrMismoEje = []
        #ESP32 centro
        self.centro = []
        #ESP32 Punto aislado
        self.puntoAislado = []
        #ESP32 Mismo eje
        self.puntoAislado = []
    
    def run(self):
        """ Esta funcion se encarga de obtener las coordenadas X,Y que cumplen las condiciones de umbral
        y ventana de tiempo.
        
        Esta funcion se encarga de obtener las ventanas de tiempo del dispositivo posiblemente infectado,
        siempre que cumpla con las ventanas de tiempo del dispositivo infectado. 
        En base a esa ventana crea ventanas del tamaño del umbral definido hace el siguiente procedimiento:
            1-Determinar Zona: Conjunto de ESP32 que detectaron el dispositivo en dicha ventana pequeña
                            con sus mediciones.
            2-Ajustar Sistema de coordenadas: En base al sistema configurado, desplaza las coordenadas 
                            para aplicar trilateracion.
            3-Aplicar Coordenadas: Calculo de coordenadas X,Y
        """
        #Obtengo ventana de dispositivo dentro de ventana global
        minTimestamp = self.base_datos_distancias.ejecutarSelect("SELECT min(TIMESTAMP) FROM DIST_DISPOSITIVOS WHERE MAC_DISP='"\
                                                                    + str(self.mac) +"' AND TIMESTAMP>="\
                                                                    + str(self.minVentanaGlobal))
        maxTimestamp = self.base_datos_distancias.ejecutarSelect("SELECT max(TIMESTAMP) FROM DIST_DISPOSITIVOS WHERE MAC_DISP='"\
                                                                    + str(self.mac) +"' AND TIMESTAMP<="\
                                                                    + str(self.maxVentanaGlobal))
        
        ventanaTiempo = [ minTimestamp[0][0] , minTimestamp[0][0] + ConstantesPath.UMBRAL_TIMESTAMP ]
        
        while(ventanaTiempo[1] <= maxTimestamp[0][0]):
            #Obtengo las ESP32 que detectaron al disp en esa ventana de tiempo
            self.nameVentana = self.base_datos_distancias.ejecutarSelect("SELECT distinct NAME_ESP32 FROM DIST_DISPOSITIVOS WHERE TIMESTAMP >= "\
                                                    +str(ventanaTiempo[0])+" AND TIMESTAMP < "\
                                                    +str(ventanaTiempo[1])+" ORDER BY NAME_ESP32 ASC")
            
            print("NAME VENTANA: ", self.nameVentana)

            cantClientesMac = len(self.nameVentana)
            
            #Si la cantidad de ESP32 que detectaron al dispositivo en la ventana fue >3
            #Se eligen aquellas 3 que tomaron la mayor cantidad de mediciones
            # si es menor que 3 no se puede calcular la trilateracion
            if(cantClientesMac >= ConstantesPath.LIMITES_TRILATERACION):
                if(cantClientesMac > ConstantesPath.LIMITES_TRILATERACION):
                    #CORREGIR ADAPTADO AL CASO DE 3, ES MAS FACIL
                    self.determinarTrianguloTrilateracion(ventanaTiempo, self.nameVentana, self.mac)

                ##Obtengo lista de [(Nombre - Distancia - Timestamp),...,] para la MAC
                self.ajustarSistemaCoordenadas(self.nameVentana)
                self.determinarZona(ventanaTiempo)
                self.aplicarCoordenadas(self.mac, self.nameVentana)
            #Calculamos nueva ventana de tiempo
            ventanaTiempo = [ventanaTiempo[1], ventanaTiempo[1] + ConstantesPath.UMBRAL_TIMESTAMP]
        
        #Para el caso donde la ventana de tiempo sea mas grande que el ultimo rango de tiempo, entonces
        #se lo hace una iteracion mas
        if(ventanaTiempo[0] < maxTimestamp[0][0]):
            ventanaTiempo[1] = maxTimestamp[0][0]
            #Obtengo las ESP32 que detectaron al disp en esa ventana de tiempo
            self.nameVentana = self.base_datos_distancias.ejecutarSelect("SELECT distinct NAME_ESP32 FROM DIST_DISPOSITIVOS WHERE TIMESTAMP >= "\
                                                    +str(ventanaTiempo[0])+" AND TIMESTAMP < "\
                                                    +str(ventanaTiempo[1])+" ORDER BY NAME_ESP32 ASC")

            print("NAME VENTANA 2: ", self.nameVentana)

            cantClientesMac = len(self.nameVentana)
            
            if(cantClientesMac >= ConstantesPath.LIMITES_TRILATERACION):
                if(cantClientesMac > ConstantesPath.LIMITES_TRILATERACION):
                    #CORREGIR ADAPTADO AL CASO DE 3, ES MAS FACIL
                    self.determinarTrianguloTrilateracion(ventanaTiempo, self.nameVentana, self.mac)

                ##Obtengo lista de [(Nombre - Distancia - Timestamp),...,] para la MAC
                self.ajustarSistemaCoordenadas(self.nameVentana)
                self.determinarZona(ventanaTiempo)
                self.aplicarCoordenadas(self.mac, self.nameVentana)

        #Tendriamos una lista con las coordenadas de la MAC
        self.mutex.acquire()
        try:
            self.coordenadasCalculadas[self.mac] = self.listaCoordenadas
        finally:
            self.mutex.release()

    def determinarZona(self, ventanaTiempo):
        """Esta funcion se encarga de buscar las ESP32 y las mediciones en la ventana temporal.

        :param ventanaTiempo: ventana de tiempo del dispositivo posiblemente infectado 
        :type ventanaTiempo: list
        :returns: lista de mediciones con timestamp, distancia y ESP32
        :rtype: list
        """

        self.centro = list(filter(lambda x:x[0]==0 and x[1]==0, self.coordESP32Ordenadas)) # (x,y) = (0,0)
        self.mismoEje = list(filter(lambda x:(x[0]==0) ^ (x[1]==0), self.coordESP32Ordenadas)) # (x,y) = (x,0) ó (0,y)
        self.puntoAislado = list(filter(lambda x: (x!=self.centro[0]) and (x!=self.mismoEje[0]), self.coordESP32Ordenadas))

        #Buscar las ESP32 que lo detectaron con el menor Timestamp dentro del umbral
        #arrCentro -> [(ESP_NAME,DIST,TIMESTAMP),...,(ESP32_NAME,DIST,TIMESTAMP)]
        self.arrCentro = self.base_datos_distancias.ejecutarSelect("SELECT NAME_ESP32, DISTANCIA, TIMESTAMP FROM DIST_DISPOSITIVOS WHERE "\
                                                    "MAC_DISP='" + str(self.mac) + "' AND "\
                                                    "NAME_ESP32='" + str(self.centro[0][2]) +"' AND TIMESTAMP >= "\
                                                    +str(ventanaTiempo[0])+" AND TIMESTAMP < "\
                                                    +str(ventanaTiempo[1])+" ORDER BY TIMESTAMP")
        
        #Buscar las ESP32 que lo detectaron con el menor Timestamp dentro del umbral
        #arrPuntoAislado -> [(ESP_NAME,DIST,TIMESTAMP),...,(ESP32_NAME,DIST,TIMESTAMP)]
        self.arrPuntoAislado = self.base_datos_distancias.ejecutarSelect("SELECT NAME_ESP32, DISTANCIA, TIMESTAMP FROM DIST_DISPOSITIVOS WHERE "\
                                                    "MAC_DISP='" + str(self.mac) + "' AND "\
                                                    "NAME_ESP32='" + str(self.puntoAislado[0][2]) +"' AND TIMESTAMP >= "\
                                                    +str(ventanaTiempo[0])+" AND TIMESTAMP < "\
                                                    +str(ventanaTiempo[1])+" ORDER BY TIMESTAMP")
        
        #Buscar las ESP32 que lo detectaron con el menor Timestamp dentro del umbral
        #arrMismoEje -> [(ESP_NAME,DIST,TIMESTAMP),...,(ESP32_NAME,DIST,TIMESTAMP)]
        self.arrMismoEje = self.base_datos_distancias.ejecutarSelect("SELECT NAME_ESP32, DISTANCIA, TIMESTAMP FROM DIST_DISPOSITIVOS WHERE "\
                                                    "MAC_DISP='" + str(self.mac) + "' AND "\
                                                    "NAME_ESP32='" + str(self.mismoEje[0][2]) +"' AND TIMESTAMP >= "\
                                                    +str(ventanaTiempo[0])+" AND TIMESTAMP < "\
                                                    +str(ventanaTiempo[1])+" ORDER BY TIMESTAMP")

    def ajustarSistemaCoordenadas(self,nombresESPDetectadas):
        """Esta funcion ajusta el sistema de coordenadas de las 3 ESP32 recibidas

        :param nombresESPDetectadas: Nombre de ESP32
        :type nombresESPDetectadas: list
        """
        coordESP32 = []  #[(X, Y, nombre), (X, Y, nombre), (X, Y, nombre)]
        self.coordESP32Ordenadas = [] #[(X, Y, nombreOrigen), (X, Y, nombreMismoEje), (X, Y, nombrePtoAislado)]
        
        # se obtienen las coordenadas de las ESP32 que detectaron
        for i in range(len(nombresESPDetectadas)):
            for j in range(len(self.coordenadas)):
                if(nombresESPDetectadas[i][0] == self.coordenadas[j][0]):
                    ##Almaceno las coordenadas
                    coordESP32.append((self.coordenadas[j][1][0],self.coordenadas[j][1][1],nombresESPDetectadas[i][0])) 

        #Comparo las primeras dos coordenadas para ver si hay coincidencia en X o Y
        #Coincide primero con segundo
        if(coordESP32[0][0] == coordESP32[1][0] or coordESP32[0][1] == coordESP32[1][1]):
            modulo1 = self.calcularModulo(coordESP32[0][0],coordESP32[0][1])
            modulo2 = self.calcularModulo(coordESP32[1][0],coordESP32[1][1])
            if(modulo1 < modulo2):
                self.coordX = coordESP32[0][0]
                self.coordY = coordESP32[0][1]
            else: 
                self.coordX = coordESP32[1][0]
                self.coordY = coordESP32[1][1] 
        #Coincide primero con tercero
        elif(coordESP32[0][0] == coordESP32[2][0] or coordESP32[0][1] == coordESP32[2][1]):
            modulo1 = self.calcularModulo(coordESP32[0][0],coordESP32[0][1])
            modulo2 = self.calcularModulo(coordESP32[2][0],coordESP32[2][1])
            if(modulo1 < modulo2):
                self.coordX = coordESP32[0][0]
                self.coordY = coordESP32[0][1]
            else: 
                self.coordX = coordESP32[2][0]
                self.coordY = coordESP32[2][1] 
        #Coincide segundo con tercero
        elif(coordESP32[1][0] == coordESP32[2][0] or coordESP32[1][1] == coordESP32[2][1]):
            modulo1 = self.calcularModulo(coordESP32[1][0],coordESP32[1][1])
            modulo2 = self.calcularModulo(coordESP32[2][0],coordESP32[2][1])
            if(modulo1 < modulo2):
                self.coordX = coordESP32[1][0]
                self.coordY = coordESP32[1][1]
            else: 
                self.coordX = coordESP32[2][0]
                self.coordY = coordESP32[2][1]
        # se guardan las coordenadas y nombre de las 3 ESP32 desplazadas al origen
        #ALMACENO PRIMERO
        self.coordESP32Ordenadas.append((coordESP32[0][0]-self.coordX,coordESP32[0][1]-self.coordY,coordESP32[0][2])) 
        #ALMACENO SEGUNDO
        self.coordESP32Ordenadas.append((coordESP32[1][0]-self.coordX,coordESP32[1][1]-self.coordY,coordESP32[1][2]))
        #ALMACENO TERCERO
        self.coordESP32Ordenadas.append((coordESP32[2][0]-self.coordX,coordESP32[2][1]-self.coordY,coordESP32[2][2])) 
        
        print("COORD ESP32 ORD: ", self.coordESP32Ordenadas)

    def calcularModulo(self,x,y):
        """Funcion que calcula el modulo entre dos puntos

        :param x: valor del primer punto
        :type x: float
        :param y: valor del segundo punto
        :type y: float
        :returns: modulo entre dos puntos
        :rtype: float
        """
        return math.sqrt(math.pow(x,2) + math.pow(y,2))

    def aplicarCoordenadas(self, mac, esp32_name_macs): #NO SE USAN LOS ARGUMENTOS??
        """Obtiene coordenadas para las ESP32 y las almacena en un arreglo.

        Esta funcion recibe las 3 ESP32 con la lista de informacion de las mediciones, 
        determina si cumple el umbral de timestamp y aplica la trilateracion para obtener 
        las coordenadas X,Y.
        
        :param mac: Mac del dispositivo posiblemente infectado 
        :type mac: str
        :param names_esp32: nombres de las ESP32
        :type names_esp32: list
        :param esp32_dist_times_mac: lista de mediciones de las ESP32 
        :type esp32_dist_times_mac: list
        """

        #print("Arreglo de centro: ", self.centro[0][2])
        #print("Arreglo de punto aislado: ", self.puntoAislado[0][2])
        #print("Arreglo de mismo eje: ", self.mismoEje[0][2])

        #arreglos -> [(ESP_NAME,DIST,TIMESTAMP),...,(ESP32_NAME,DIST,TIMESTAMP)]
        for centro in self.arrCentro:
            for mismoEje in self.arrMismoEje:
                if(abs(centro[2] - mismoEje[2]) < ConstantesPath.UMBRAL_TIMESTAMP):
                    for puntoAislado in self.arrPuntoAislado:
                        if(abs(centro[2] - puntoAislado[2]) < ConstantesPath.UMBRAL_TIMESTAMP):
                            #Obtengo nombre de las ESP32 que cumplieron la condicion de timestamp
                             #nombres_Dist = [(centro[0],mismoEje[0],puntoAislado[0])]
                             #Obtengo las coordenadas de las ESP32 mediante trilateracion
                             coordenada = self.obtenerCoordenadas(self.mismoEje, self.puntoAislado, centro[1], mismoEje[1], puntoAislado[1])
                             #print(coordenada)
                             #Calculo un promedio del timestamp de los 3 
                             timestamp = self.obtenerTimestampPromedio(centro[2], mismoEje[2], puntoAislado[2])
                             #Almaceno ene arreglo de coordenadas
                             #Forma arreglo -> [(x,y,timestamp),...(x,y,timestamp)]
                             self.listaCoordenadas.append((coordenada[0], coordenada[1], timestamp))
    
    def obtenerCoordenadas(self, mismoEje, puntoAislado, distA, distB, distC):
        """Obtienen las coordenadas del dispositivo 

        Recibe el dispositivo a determinar coordenadas, y retorna las coordenadas X,Y
        en base a los ESP32 seteados
        :param mismoEje: ESP32 que se encuentra en el mismo eje que la de referencia.
        :type mismoEje: list
        :param puntoAislado: ESP32 que se encuentra en X,Y distinto a de referencia y mismo eje.
        :type puntoAislado: list
        :param distA: distancia A
        :type distA: float
        :param distB: distancia B
        :type distB: float
        :param distC: distancia C
        :type distC: float
        :returns: coordenadas X,Y
        :rtype: list
        """

        cx = puntoAislado[0][0]
        cy = puntoAislado[0][1]
        #mismo eje: (NOMBRE, X, Y)
        if(mismoEje[0][1] == 0): #Coincidencia en X
            #print("Coincidencia en X")
            d = mismoEje[0][0]
            coordenadasObtenidas = self.trilateracion.localizacionY(distA,distB,distC,d,cx,cy)
            return (round(coordenadasObtenidas[0] + self.coordX,3), round(coordenadasObtenidas[1] + self.coordY,3))
        else:
            #print("Coincidencia en Y")
            d = mismoEje[0][1] #Coincidencia en Y
            coordenadasObtenidas = self.trilateracion.localizacionX(distA,distB,distC,d,cx,cy)
            return (round(coordenadasObtenidas[0] + self.coordX,3),round(coordenadasObtenidas[1] + self.coordY,3))
    

    def obtenerTimestampPromedio(self, timestampA, timestampB, timestampC):
        """Obtiene el timestamp promedio entre tres timestamps

        :param timestampA: primer timestamp
        :type timestampA: int
        :param timestampB: segundo timestamp
        :type timestampB: int
        :param timestampC: tercer timestamp
        :type timestampC: int
        :returns: timestamp promedio
        :rtype: int
        """
        return int((timestampA + timestampB + timestampC)/3)

    def determinarTrianguloTrilateracion(self, ventanaTiempo, namesESP32, mac):
        """Esta funcion se encarga de determinar las tres ESP32 con mayor cantidad de mediciones

        :param ventanaTiempo: ventana de tiempo
        :type ventanaTiempo: list
        """
        medicionesESP32 = []
        nombres = []
        for i in range(len(namesESP32)):
            cantidad = self.base_datos_distancias.ejecutarSelect("SELECT count(*) FROM DIST_DISPOSITIVOS WHERE TIMESTAMP >= "\
                        + str(ventanaTiempo[0]) +" AND TIMESTAMP < "\
                        + str(ventanaTiempo[1]) +" AND NAME_ESP32='" \
                        + str(namesESP32[i][0]) + "' AND MAC_DISP='" + mac + "' ORDER BY TIMESTAMP") #Y NO SE ESPECIFICA LA MAC del celular objetivo???
            medicionesESP32.append((cantidad,namesESP32[i][0]))
            nombres.append(namesESP32[i])
        
        # se eliminan las ESP32 que menos mediciones tienen
        while(len(nombres)>3):
            min = (sys.maxint,'')
            for i in range(len(medicionesESP32)):
                if(medicionesESP32[i][0] < min[0]):
                    min = medicionesESP32[i]
            nombres.remove(min)

        namesESP32 = nombres