from math import *
from threading import Thread

#DOCUMENTAR

class Trilateracion:
    #ar es la distancia del dispositivo a la Rpi A, br distancia a la Rpi B, cr...
    def __init__(self):
       # self.figura = None
        self.AX = 0
        self.AY = 0
        self.BX = 0
        self.BY = 0
        self.CX = 0
        self.CY = 0   

#    def imprimir_coordenadas(self):
#        print("AX: ",self.AX, " AY: ",self.AY)
#        print("BX: ",self.BX, " BY: ",self.BY) 
#        print("CX: ",self.CX, " CY: ",self.CY) 
#    
#    def setear_distancias(self, ar1, br1, cr1):    
#        try:
#            self.ar = float(ar1)
#            self.br = float(br1)
#            self.cr = float(cr1)
#        except:
#            print ("No seleccionaste las fuerzas de senal")
#            return
#
#        print ("El dispositivo capta una senial de: ")
#        print ("Antena A %s" %self.ar)
#        print ("Antena B %s" %self.br)
#        print ("Antena C %s" %self.cr)

    def localizacionX(self, distancia_a, distancia_b, distancia_c, d, cx, cy):
        """se localiza el dispositivo por medio de las
    fuerzas de las seniales captadas y de la ubicacion de
    las antenas (2 antenas estan en el mismo EJE X)
        """

        r1 = distancia_a
        r2 = distancia_b
        r3 = distancia_c
        x2 = d
        x3 = cx
        y3 = cy
        
        x = (r1**2 - r2**2 + x2**2)/(2*x2)
        y = (r1**2 - r3**2 + x3**2 + y3**2 - (2*x3*x))/(2*y3)
        #z = sqrt(r1**2 - x**2 - y**2)

        condicion = r1**2 - x**2 - y**2
        if(condicion < 0):
                z = 0
                #print("Z es imaginario")
        elif(condicion == 0):
                z = 0
        else:
                z = [sqrt(condicion),-sqrt(condicion)]
                #print("Hay dos posibles soluciones para el plano Z las cuales son: %s" % z)
                z = 0
        #print ("Tu estas ubicado en -> (%s,%s,%s)" % (x, y, z))

        return (x,y)

    def localizacionY(self, distancia_a, distancia_b, distancia_c, d, cx, cy):
        """ dos antenas en el mismo EJE Y.
        """

        r1 = distancia_a
        r2 = distancia_b
        r3 = distancia_c
        y2 = d
        x3 = cx
        y3 = cy

        y = (r1**2 - r2**2 + y2**2)/(2*y2)
        x = (r1**2 - r3**2 + x3**2 + y3**2 - (2*y3*y))/(2*x3)
        #z = sqrt(r1**2 - x**2 - y**2)

        condicion = r1**2 - x**2 - y**2
        if(condicion< 0):
                z = 0
                #print("Z es imaginario")
        elif(condicion == 0):
                z = 0
        else:
                z = [sqrt(condicion),-sqrt(condicion)]
                #print("Hay dos posibles soluciones para el plano Z las cuales son: %s" % z)
                z = 0
        #print ("Tu estas ubicado en -> (%s,%s,%s)" % (x, y, z))

        return (x,y)

    #Funcion que grafica las 3 antenas(rpi) con sus radios de alcance al dispositivo y
    #la posicion marcada con un punto de los dispositivos 
    #(x,y): es la posicion en X e Y del dispositivo
    #a: es la figura, se vuelve a pasar para que tengan efectos los cambios
    #ar: radio (distancia al dispositivo) de la Rpi A
    #br: radio (distancia al dispositivo) de la Rpi B
    #cr: radio (distancia al dispositivo) de la Rpi C
    # def graficar(self, x1, y1, x2, y2, ar, br, cr, a):
    #     if (a is not None):
    #         a.remove()
    #     ion()
    #     show()
    #     a = subplot(111, aspect='equal')
    #     e = Ellipse((self.AX,self.AY), ar*2, ar*2, 0)
    #     e.set_clip_box(a.bbox)
    #     e.set_color('green')
    #     e.set_alpha(0.1)
    #     a.add_patch(e)
    #     a.annotate("Antena A",
    #             xy=(self.AX,self.AY), xycoords='data',
    #             xytext=(self.AX-3, self.AY+3), textcoords='data',
    #             arrowprops=dict(arrowstyle="->",
    #                             connectionstyle="arc3"), 
    #             )
    #     a.plot(self.AX,self.AY, "g^", mew=2, ms=12)
    #     a.add_artist(e)
    #     e = Ellipse((self.BX,self.BY), 2*br,2*br, 0)
    #     e.set_clip_box(a.bbox)
    #     e.set_color('red')
    #     e.set_alpha(0.1)
    #     a.add_patch(e)
    #     a.annotate("Antena B",
    #             xy=(self.BX, self.BY), xycoords='data',
    #             xytext=(self.BX+3, self.BY+3), textcoords='data',
    #             arrowprops=dict(arrowstyle="->",
    #                             connectionstyle="arc3"))
    #     a.plot(self.BX,self.BY, "r^", mew=2, ms=12)
    #     a.add_artist(e)
    #     e = Ellipse((self.CX,self.CY), 2*cr, 2*cr, 0)
    #     e.set_clip_box(a.bbox)
    #     e.set_color('blue')
    #     e.set_alpha(0.1)
    #     a.add_patch(e)
    #     a.annotate("Antena C",
    #             xy=(self.CX, self.CY), xycoords='data',
    #             xytext=(self.CX+3.5, self.CY-6), textcoords='data',
    #             arrowprops=dict(arrowstyle="->",
    #                             connectionstyle="arc3"))
    #     a.plot(self.CX, self.CY, "b^", mew=2, ms=12)
    #     a.add_artist(e)
    #     a.plot(x1, y1, "k.", mew=2, ms=5)
    #     a.plot(x2, y2, "k.", mew=2, ms=5)
    #     xlim(-10, 10)
    #     ylim(-10, 10)
    #     draw()
    #     pause(0.001)
    #     return a

