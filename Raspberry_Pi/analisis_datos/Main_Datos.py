import trilateracion as t
import sqlite3
import random
import os
import signal
from threading import Thread,Lock
from subprocess import check_output
from ConstantesPath import ConstantesPath
import sys
import BaseDeDatosDistancia as bdd_distancias
from ThreadDistanciaESP32 import ThreadDistanciaESP32
from ThreadTrilateracion import ThreadTrilateracion
from ThreadCercania import ThreadCercania
sys.path.append("../")
import BaseDeDatos as bdd

#-----------------------------------COMIENZA PROGRAMA------------------------------------
class Main_Datos:
    """ Clase Main de Datos que permite el manejo de las mediciones realizadas por las ESP32 

        Esta clase se encarga de tomar los datos de las mediciones realizadas por las ESP32 y
        realizar un tratamiento de las mismas, derivando a la obtencion de:
        - Distancias de ESP32 a dispositivos para los diferentes timestamps
        - Coordenadas del dispositivo infectado con respecto a aquellos que estuvieron en dicho timestamp
        - Distancias de los dispositivos detectados en el rango de timestamp respecto al infectado
        - Notificar a aquellos dispositivos detectados que estuvieron en 3 o mas mediciones a menos de 2 metros
          del dispositivo infectado
    """

    def __init__(self):
        """ Inicializador de instancias de la clase Main_Datos.

        Se encarga de inicializar las tablas de mediciones RSSI y las de distancias.
        La de mediciones RSSI se utilizara para obtener acceso a las mediciones RSSI de las ESP32
        a los distintos dispositivos.
        La de distancias se utilizara para almacenar los resultados del algoritmo utilizado en esta clase, 
        permitiendo obtener las coordenadas de los dispositivos y las distancias entre ellos.
        """
        #Creo objetos BDD
        self.base_datos_rssi = bdd.BaseDeDatos(ConstantesPath.BDD_RSSI)
        self.base_datos_distancias = bdd_distancias.BaseDeDatosDistancia(ConstantesPath.BDD_DISTANCIAS)
        #Creo lista de coordenadas y nombres de las ESP32 configuradas
        self.coordenadas = []
        #Arreglo de hilos de ESP32 para detectar las distancias respecto a los dispositivos
        self.hilos_esp32 = []
        #Numero de telefono del infectado        
        self.telefono = ''
        #Arreglo de hilos de coordenadas que se encargaran de calcular coordenadas de dispositivos
        self.hilosCoordenadas = []
        #Diccionario de coordenadas de dispositivos que se encuentran en rango de timestamp
        self.coordenadasCalculadas = { }
        #Mutex para escribir en el diccionario de coordenadas 
        self.mutex = Lock()
        #Mac del dispositivo infectado
        self.macInfectada = []
        #Arreglo de hilos para determinar la cercania de disp detectados respecto al infectado
        self.hilosCercanias = []
        #Mutex para escribir sobre el arreglo de infectados
        self.mutexIfectados = Lock()
        #Arreglo de infectados
        self.infectados = []
        #Arreglo de MACS posiblemente infectadas
        self.macsDetectadas = []
        #Arreglo de ESP32 dentro
        self.esp32_indoor = []                 # <- DICCIONARIO?
        #Arreglo de ESP32 fuera
        self.esp32_outdoor = []
        #Arreglo de mac infectadas para lanzar hilos
        self.macInfectadasArr = []
    
    def main(self):
        """ Esta funcion es el main, se encarga de lanzar los hilos para determinar distancias, 
        coordenadas y cercania entre dispositivos
        """

        # Obtiene los nombres de ESP32 utilizadas
        self.nombres_esp32 = self.base_datos_rssi.ejecutarSelect("SELECT distinct ID_CLIENTE from INFO_DISPOSITIVOS")
        
        print("Las ESP32 clientes son las siguientes:")
        for esp32 in self.nombres_esp32:
            print(esp32[0])

        #Configuracion de coordenadas de las ESP32
        self.confCoordenadasInput(self.nombres_esp32)
        
        # se obtiene la cant de filas de cada tabla para saber si DIST_DISPOSITIVOS esta actualizada
        cant_registros_bdd_rssi = self.base_datos_rssi.ejecutarSelect("SELECT count(*) from MEDICIONES_RSSI")
        cant_registros_bdd_distancias = self.base_datos_distancias.ejecutarSelect("SELECT count(*) from DIST_DISPOSITIVOS")

        cant_registros_bdd_rssi = cant_registros_bdd_rssi[0][0] # tomo el 1er elemento
        cant_registros_bdd_distancias = cant_registros_bdd_distancias[0][0]

        if(cant_registros_bdd_rssi != cant_registros_bdd_distancias): # se actualiza la BDD distancias si no lo está
            ### ARRANCAN HILOS -> 1 HILO POR CLIENTE 
            # Estos hilos calculan distancias de cada dispositivo respecto a la ESP32 cliente
            for cliente in self.esp32_indoor:
                hilo = ThreadDistanciaESP32(cliente, ConstantesPath.BDD_RSSI, ConstantesPath.BDD_DISTANCIAS, ConstantesPath.RSSI_REFERENCE_INDOOR, ConstantesPath.PATH_LOSS_INDOOR)
                self.hilos_esp32.append(hilo)
                hilo.start()

            for cliente in self.esp32_outdoor:
                hilo = ThreadDistanciaESP32(cliente, ConstantesPath.BDD_RSSI, ConstantesPath.BDD_DISTANCIAS, ConstantesPath.RSSI_REFERENCE_OUTDOOR, ConstantesPath.PATH_LOSS_OUTDOOR)
                self.hilos_esp32.append(hilo)
                hilo.start()

            #Espera que terminen los hilos para avanzar
            for hilo in self.hilos_esp32:
                hilo.join()
        else:
            print("\x1b[3;36m>> Base de datos de distancias ya se encuentra actualizada <<\x1b[0;37m")


        #Recibo MAC del contagiado 
        self.macInfectada = self.IngresarDispositivo()
        #HACER ESTO UNA VEZ POR DIA, UNA PERSONA VA VARIOS DIAS AL BAR

        cmdMinTimestamp = "SELECT MIN(TIMESTAMP) FROM DIST_DISPOSITIVOS WHERE (MAC_DISP='" + str(self.macInfectada[0][0]) + "'"
        cmdMaxTimestamp = "SELECT MAX(TIMESTAMP) FROM DIST_DISPOSITIVOS WHERE (MAC_DISP='" + str(self.macInfectada[0][0]) + "'"
        for i in range(len(self.macInfectada)-1):
            cmdMinTimestamp = cmdMinTimestamp + " OR MAC_DISP='" + str(self.macInfectada[i+1][0]) + "'"
            cmdMaxTimestamp = cmdMaxTimestamp + " OR MAC_DISP='" + str(self.macInfectada[i+1][0]) + "'"
        cmdMinTimestamp = cmdMinTimestamp + ") AND TIMESTAMP>1000000000"
        cmdMaxTimestamp = cmdMaxTimestamp + ") AND TIMESTAMP>1000000000"

        #print(cmdMinTimestamp)
        #print(cmdMaxTimestamp)

        #Obtengo intervalo de tiempo en que estuvo presente el dispositivo infectado
        minTimestampMAC1 = self.base_datos_distancias.ejecutarSelect(cmdMinTimestamp)
        maxTimestampMAC1 = self.base_datos_distancias.ejecutarSelect(cmdMaxTimestamp)

        #Obtengo las MACS que estuvieron en ese intervalo de tiempo
        macs = self.base_datos_distancias.ejecutarSelect("SELECT distinct MAC_DISP FROM DIST_DISPOSITIVOS WHERE TIMESTAMP>="\
                                                    +str(minTimestampMAC1[0][0])+" AND TIMESTAMP<="\
                                                    +str(maxTimestampMAC1[0][0]))
        for mac in macs:
            self.macsDetectadas.append(mac[0])    
        
        #Aplico trilateracion -> Solo para MACS detectadas en dicho intervalo de tiempo
        self.AplicarTrilateracion(self.macsDetectadas, minTimestampMAC1[0][0], maxTimestampMAC1[0][0])

        #print("MACS antes de sacar: ", self.macsDetectadas)

        #Eliminamos de las MACS detectadas el dispositivo infectado
        # macInfectadasArr contiene todas las coordenadas de la/las mac infectadas
        for infectados in self.macInfectada:
            self.macInfectadasArr.append(self.coordenadasCalculadas[infectados[0]])
            self.macsDetectadas.remove(infectados[0])

        #print(self.macInfectadasArr)

        #Lanzamiento de hilos que calculan la distancia de los dispositivos detectados a la MAC infectada
        #Se buscan los numeros de telefono de los que estan a menos de dos metros
        #print("MACS: ", self.macsDetectadas)
        for mac in self.macsDetectadas:
            print("emitio MAC")
            hilo = ThreadCercania(self.macInfectadasArr,self.coordenadasCalculadas[mac], self.infectados, self.mutexIfectados, mac)
            self.hilosCercanias.append(hilo)
            hilo.start()

        for hilo in self.hilosCercanias:
            hilo.join()

        #Notificacion a dispositivos posiblemente infectados
        for infectado in self.infectados:
            self.alerta_contagio(infectado)
 
    def confCoordenadasInput(self,nombres_esp32):
        """ Configura las coordenadas de las ESP32 instaladas 

        Parte de la idea es que una de ellas debe estar en el punto de referencia (0,0),
        luego de que la segunda esta en el mismo eje X que la de referencia y las
        demas, independientemente de la cantidad deben especificarse en (X,Y)
        
        :param nombres_esp32: nombres de las ESP32
        :type nombres_esp32: str
        """
        print("\nAhora se le solicitará el nombre de cada Nodo junto con sus coordenadas en un espacio bidimensional. \n" +
            "La ubicación de cada nodo debe ser tal que se forme una topología como la que se muestra a continuación, \n" +
            "extendiéndose a una mayor cantidad si es necesario para cubrir toda la superficie del lugar. \n" +
            "Uno de los nodos en algún extremo será considerado el nodo origen(0,0), \ny los restantes se ubicarán formando zonas triangulares.")
        file = open('esquema_nodos.txt', 'r')
        for linea in file:
            print(linea, end='')
        file.close()

        for cliente_esp32 in nombres_esp32:
            name_esp32 = cliente_esp32[0]
            print("Ingrese coordenadas X e Y de " + name_esp32)
            x = float(input("Coordenada X: "))
            y = float(input("Coordenada Y: "))
            self.coordenadas.append((name_esp32,(x,y)))
            position = str(input("¿El nodo '" + str(name_esp32) + "' se encuentra en un ambiente interior? [y/n]:" ))
            if(position == 'y'):
                self.esp32_indoor.append(name_esp32)
            else:
                self.esp32_outdoor.append(name_esp32)
                
  #      #Comprueba lo que ingreso en la configuracion - QUEDA COMPROBAR ERRORES
  #      esp_32_origen = str(input("Ingrese el nombre de aquella ESP-32 que sera el punto de referencia (0,0): "))
  #      esp_32_origen_position = str(input("Ingrese 1 si la ESP32 se encuentra dentro, en caso contrario 0: "))
  #      while(True):
  #          if(not esp_32_origen or not esp_32_origen_position):
  #              esp_32_origen = str(input("Ingrese el nombre de aquella ESP-32 que sera el punto de referencia (0,0): "))
  #              esp_32_origen_position = str(input("Ingrese 1 si la ESP32 se encuentra dentro, en caso contrario 0: "))
  #          elif(((esp_32_origen,) in nombres_esp32) and (esp_32_origen_position=='0' or esp_32_origen_position=='1')): 
  #              self.coordenadas.append((esp_32_origen,(0,0)))
  #              if(esp_32_origen_position == '1'):
  #                  self.esp32_indoor.append(esp_32_origen)
  #              else:
  #                  self.esp32_outdoor.append(esp_32_origen)
  #              break
  #          else:
  #              print("El nombre es incorrecto o ingreso mal los valores de posicion de la ESP32")
  #              esp_32_origen = ''
  #      
  #      esp_32_x = str(input("Ingrese el nombre de aquella ESP-32 que estara en el mismo eje X que "+ esp_32_origen+ ": "))
  #      esp_32_x_position = str(input("Ingrese 1 si la ESP32 se encuentra dentro, en caso contrario 0: "))
  #      while(True):
  #          if(not esp_32_x or not esp_32_x_position):
  #              esp_32_x = str(input("Ingrese el nombre de aquella ESP-32 que estara en el mismo eje X que "+ esp_32_origen+ ": "))
  #          elif((esp_32_x,) in nombres_esp32 and esp_32_x != esp_32_origen and (esp_32_origen_position=='0' or esp_32_origen_position=='1')): 
  #              x = float(input("Coordenada X: "))
  #              self.coordenadas.append((esp_32_x,(x,0)))
  #              if(esp_32_x_position == '1'):
  #                  self.esp32_indoor.append(esp_32_x)
  #              else:
  #                  self.esp32_outdoor.append(esp_32_x)
  #              break
  #          else:
  #              print("El nombre es incorrecto o ingreso mal los valores de posicion de la ESP32")
  #              esp_32_x = ''


    def AplicarTrilateracion(self, macs, timestampMinGlobal, timestampMaxGlobal):
        """ Esta funcion lanza un hilo para cada MAC dentro del intervalo de timestamp del infectado 
        y dichos hilos calculan trilateracion

        Se lanza un hilo por cada MAC dentro del intervalo de tiempo del dispositivo infectado, 
        y estos calculan las coordenadas que cumplan con el umbral de timestamp, agregandolas
        al arreglo global de coordenadas. 

        :param macs: lista de direcciones MAC de dispositivos detectados en rango de timestamp de infectado
        :type macs: list
        :param timestampMinGlobal: Timestamp minimo del dispositivo infectado
        :type timestampMinGlobal: int
        :param timestampMaxGlobal: Timestamp maximo del dispositivo infectado
        :type timestampMaxGlobal: int
        """

        for mac in macs:
            hilo = ThreadTrilateracion(self.coordenadas, mac, timestampMinGlobal, timestampMaxGlobal, self.coordenadasCalculadas, self.mutex)
            self.hilosCoordenadas.append(hilo)
            hilo.start()
        
        for hilo in self.hilosCoordenadas:
            hilo.join()

     #   hilo = ThreadTrilateracion(self.coordenadas, "75:ce:44:f0:d7:c4", timestampMinGlobal, timestampMaxGlobal, self.coordenadasCalculadas, self.mutex)
     #   hilo.start()
     #   hilo.join()
   
    def alerta_contagio(self,infectado):
        """Alerta sobre supuesto contagiado de covid debido a su cercania mas de dos metros

        Esta funcion recibe el telefono o MAC del dispositivo posiblemente infectado e
        imprime la notificacion.

        :param infectado: numero de telefono o MAC del detectado
        :type infectado: str
        """
        print("###POSIBLE CASO COVID###")
        print("El dispositivo " + infectado + " debe ser notificado")

    def IngresarDispositivo(self):
        """ Determina la MAC del dispositivo ingresado por usuario

        Consulta a la BDD si existe una MAC para un telefono determinado por el usuario
        :returns: Mac del dispositivo
        :rtype: str
        """
        #QUEDA COMPROBAR ERRORES
        telefono = ''
        while(True):
            if(not telefono):
                telefono = str(input("Ingrese telefono del contagiado: "))
            elif(self.base_datos_rssi.checkTelefono(telefono)):
                self.telefono = telefono
                macs = self.base_datos_rssi.obtenerMacTel(telefono)
                print(macs)
                return macs
            else:
                print("El telefono es incorrecto o no se encuentra en la BDD")
                telefono = ''


if(__name__ == "__main__"):
    hiloPrincipal = Main_Datos()

    hiloPrincipal.main()