class ConstantesPath:
    BDD_RSSI = "../dispositivos_detectados.db"
    BDD_DISTANCIAS = "./distancias_dispositivos.db"
    ALFA = 0.1
    RSSI_REFERENCE_INDOOR = -49.52 
    PATH_LOSS_INDOOR = 3.28
    RSSI_REFERENCE_OUTDOOR = -65.17 
    PATH_LOSS_OUTDOOR = 1.68   
    LIMITES_TRILATERACION = 3
    UMBRAL_TIMESTAMP = 10
    DISTANCIA_CONTAGIO = 2
    CANT_MATCH = 3