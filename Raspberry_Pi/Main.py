#!/usr/bin/python3
#-*- coding: utf-8 -*-

"""Contiene el metodo principal y la clase Main como punto de entrada en la ejecucion del programa."""

from BaseDeDatos import BaseDeDatos, paths
from Servidor import Servidor
import socket
import time

# ----------------------------- CONSTANTES --------------------------------------
class ctes:
    NUM_CLIENTES = 1
    PORT = 3333
    IPv4 = "192.168.1.5"
    # cantidad de dias a partir de los cuales los datos de la BDD no tienen utilidad (15 dias en seg)
    UMBRAL_DATOS_GUARDADOS = 86400 * 15  
# -------------------------------------------------------------------------------

class Main:
    """ Clase principal.
    """
    def __init__(self):
        """ Crea el socket y espera por conexiones del cliente.
            Tambien borra de la BDD las mediciones antiguas.
        """
        baseDatos = BaseDeDatos(paths.BDD)
        timeActual = int(time.time())
        # elimina los datos antiguos de la BDD
        baseDatos.borrarDatosAntiguos(timeActual - ctes.UMBRAL_DATOS_GUARDADOS)
        baseDatos.finalizarConexion()

        print("Creando hilos servidores...")
        self.sock = self.crearSocket(ctes.PORT)
        self.hilos = self.esperarConexiones(self.sock)

    def crearSocket(self, port):
        """ Crea el socket servidor con la ip local del host y puerto especificado.

        :param port: puerto de escucha del servidor.
        :type port: int
        :returs: socket servidor.
        :rtype: socket
        """
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Bind the socket to the port
        serverAddress = (ctes.IPv4, port)
        print("\x1b[1;33m" + 'starting up on {} port {}'.format(*serverAddress) + "\x1b[0;37m")
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(serverAddress)
        # Espera por 3 conexiones, el resto las rechaza
        sock.listen(3)
        return sock

    def esperarConexiones(self, server):
        """ Espera por conexiones de N clientes. Crea un hilo para cada nueva conexion entrante.

        :param server: socket servidor.
        :type server: socket
        :returns: lista de hilos Servidores.
        :rtype: list
        """
    # bucle para atender cliente
        clientes = ctes.NUM_CLIENTES  
        hilos = []
        while(clientes > 0):
            # Se espera a un cliente
            socketCliente, datosCliente = server.accept()
            print("\x1b[1;32m" + "conectado " + str(datosCliente) + "\x1b[0;37m")
            # Se crea la clase con el hilo
            hilos.append(Servidor(socketCliente, datosCliente, paths.BDD))
            clientes -= 1 
        for item in hilos: #se arrancan todos los hilos juntos
            item.start()
        return hilos

    def finalizarRecepcion(self):
        """ Cierra la conexion de cada hilo servidor con su cliente.
        """
        for item in self.hilos:
            item.finalizarRecepcion()

    def esperarHilos(self):
        """ Espera (join) por la finalizacion de todos los hilos servidores. 
        """
        for item in self.hilos: #espera por la finalizacion de todos los hilos servidores
            item.join()
        
if(__name__ == "__main__"):
    # crea BDD en disco
    #baseDatos = BaseDeDatos('dispositivos_detectados.db')
    #baseDatos.crearTablas()
    #baseDatos.finalizarConexion()

    main = Main()

    ret = str(input("Finalizar conexion? [y/n]"))
    if(ret == 'y'):
        main.finalizarRecepcion()

    main.esperarHilos()
    