#!/usr/bin/python3
#-*- coding: utf-8 -*-

"""Contiene las clases Servidor y DispositivoBluetooth, que se utilizan para la recepción y
guardado de los datos enviados por el cliente (ESP32)."""

from threading import Thread
from BaseDeDatos import BaseDeDatos
import time
import struct

class DispositivoBluetooth:
    """ Clase que guarda los datos de un dispositivo bluetooth detectado.
    
    Los datos son recibidos y decodificados en la clase Servidor. 
    """
    def __init__(self):
        """ Inicializa los campos a un valor por defecto.
        """
        self.nameLength = 0
        self.name = "-"
        self.macAddrStr = ""
        self.rssi = {} 
        self.txpower = 0
        self.cod = 0
        self.majorDevClass = ""
        self.tipoMacAddr = ""
        self.tipoBT = ""
        self.numCelular = ""


class Servidor(Thread):
    """ Es un Thread que se dedica a atender a un cliente específico.

    Recibe continuamente datos de dispositivos bluetooth detectados, enviados por un cliente.
    Decodifica y guarda en la Base de datos tales datos.
    """

    def __init__(self, sockCliente, datosCliente, pathBDD):
        """ Inicializa atributos y se conecta a la Base de datos.

        :param sockCliente: socket cliente del cual recibe los datos.
        :type sockCliente: socket
        :param datosCliente: IPv4 y puerto del socket cliente
        :type datosCliente: tuple
        :param pathBDD: ruta hacia la base de datos.
        :type pathBDD: str
        """
        print("Creando hilo servidor...")
        # LLama al constructor padre, para que se inicialice de forma correcta la clase Thread.
        Thread.__init__(self)  
        self.sockCliente = sockCliente 
        self.datosCliente = datosCliente 
        self.baseDeDatos = BaseDeDatos(pathBDD)
        self.nombreCliente = ""
        self.finalizar = False

    def finalizarRecepcion(self):
        """ Finaliza conexion con el cliente y sale del bucle de recepcion.
        """
        self.finalizar = True

    def run(self):
        """ Metodo principal del hilo Servidor.

        Recibe el nombre del cliente y envía el timestamp actual.
        Recibe continuamente mediciones tomadas por el cliente, las decodifica y guarda en la base de datos,
        hasta que se finalice la conexion.
        """
        print("Run hilo servidor...")
        self.nombreCliente = self.sockCliente.recv(128)  #recibe el nombre de la ESP32 cliente
        self.nombreCliente = self.nombreCliente.decode().split("\x00", 1)[0]
        print("\x1b[1;32m" + "Atendiendo a cliente '" + str(self.nombreCliente) + "'..." + "\x1b[0;37m")

        timestamp = int(time.time()) #envia timestamp actual
        # para enviar el num por socket se convierte en str y luego en bytes
        self.sockCliente.send(bytes(str(timestamp), 'utf8'))
        print("timestamp: " + str(timestamp))

        # ESP32 es little endian
        fmt = "<c249s6s18s" + 20*"Qb" + "bI60s11s10s11s"  # es como se debe decodificar el paquete que llega
        fmt_size = struct.calcsize(fmt)  # calcula el tamaño total que debe tener la estructura (551 bytes)
        datoNext = b''  #byte vacio
        cantMediciones = 0

        while(not(self.finalizar)):
            # Espera por datos
            data = self.sockCliente.recv(fmt_size*10) #tam buffer maximo = 10 estructuras
            data = datoNext + data   # se le concatena al principio trozos de paquetes incompletos del recv anterior
            resto = len(data) % fmt_size   # fmt_size = 551 bytes
            if(resto != 0):
                datoNext = data[-resto :]   #guardo el ultimo trozo de paquete incompleto para la siguiente iteracion
                data = data[: -resto]       #elimina el paquete que esta incompleto de los datos a guardar en esta iteracion
            else:
                datoNext = b''

            if(len(data) < fmt_size):   #no se pueden tomar los datos si el tamaño es menor a 1 paquete
                #print("MENOS DE 551 bytes: ---------------------------------------------------", len(data))
                continue

            cantPaquetes = len(data) // fmt_size   #cantidad de paquetes completos
            for i in range(0,cantPaquetes):
                device = DispositivoBluetooth()
                # DECODIFICAN DATOS
                tuplaDatos = struct.unpack(fmt, data[i*fmt_size:((i+1)*fmt_size)])  #se separan los bytes de datos en una tupla
                #print(tuplaDatos)
                #print("TAM data: " + str(len(data)))
                #print("CANT PAQ: " + str(len(data)/fmt_size))

                device.nameLength = int.from_bytes(tuplaDatos[0], "little", signed="False") # para convertir de bytes a int (sin signo)
                try:
                    device.name = tuplaDatos[1].decode().split("\x00", 1)[0] # para convertir de bytes a string
                    device.macAddrStr = tuplaDatos[3].decode().split("\x00", 1)[0] #se decodifican los bytes y se eliminan los '\x00'
                except UnicodeDecodeError:
                    print(UnicodeDecodeError)

                for i in range(4,44,2):
                    timestamp = tuplaDatos[i]
                    rssi = tuplaDatos[i+1]
                    if(timestamp == 0): break  #los que son ceros no los debe tomar
                    #los timestamp que no son actuales (por retardo en la conexion con el socket servidor) no los considera
                    if(timestamp < 100000000): continue 
                    device.rssi[timestamp] = rssi

                device.txpower = tuplaDatos[44]
                device.cod = hex(tuplaDatos[45])
                try:
                    device.majorDevClass = tuplaDatos[46].decode().split("\x00", 1)[0]
                    device.tipoMacAddr = tuplaDatos[47].decode().split("\x00", 1)[0]
                    device.tipoBT = tuplaDatos[48].decode().split("\x00", 1)[0]
                    device.numCelular = tuplaDatos[49].decode().split("\x00", 1)[0]
                except UnicodeDecodeError:
                    print(UnicodeDecodeError)

                #print("DEVICE: " + str(device.nameLength) +" | "+ device.name +" | "+ device.macAddrStr +" | "+ str(device.txpower) +" | "+ str(device.cod) +" | "+ device.majorDevClass +" | "+ device.tipoMacAddr +" | "+ device.tipoBT, device.rssi)
                print("DEVICE: " + device.macAddrStr +" | "+ device.name +" | " + device.numCelular +" |", device.rssi)
                cantMediciones += 1
                print("cant mediciones:", cantMediciones)
                #print(device.rssi)

                # GUARDAN DATOS EN LA BDD
                if(self.baseDeDatos.checkEntrada(device.macAddrStr, self.nombreCliente)):   #si ya existe una entrada actualizo, si no inserto
                    #Actualizo
                    self.baseDeDatos.actualizarDatos('INFO_DISPOSITIVOS', 'NAME', device.name, device.macAddrStr, self.nombreCliente)
                    self.baseDeDatos.actualizarDatos('INFO_DISPOSITIVOS', 'MAJOR_DEVICE_CLASS', device.majorDevClass, device.macAddrStr, self.nombreCliente)
                    self.baseDeDatos.actualizarDatos('INFO_DISPOSITIVOS', 'TX_POWER', device.txpower, device.macAddrStr, self.nombreCliente)
                    self.baseDeDatos.actualizarDatos('INFO_DISPOSITIVOS', 'COD', device.cod, device.macAddrStr, self.nombreCliente)
                    self.baseDeDatos.actualizarDatos('INFO_DISPOSITIVOS', 'TIPO_MAC_ADDR', device.tipoMacAddr, device.macAddrStr, self.nombreCliente)
                    self.baseDeDatos.actualizarDatos('INFO_DISPOSITIVOS', 'TIPO_BT', device.tipoBT, device.macAddrStr, self.nombreCliente)
                    self.baseDeDatos.actualizarDatos('INFO_DISPOSITIVOS', 'NUM_CELULAR', device.numCelular, device.macAddrStr, self.nombreCliente)
                else:
                    #Inserto
                    self.baseDeDatos.insertarDatos((device.macAddrStr, device.name, device.majorDevClass, device.txpower, device.cod, device.tipoMacAddr, device.tipoBT, device.numCelular, self.nombreCliente), 'INFO_DISPOSITIVOS')
                
                for clave in device.rssi.keys(): #timestamp y rssi inserta siempre
                    self.baseDeDatos.insertarDatos((clave, device.rssi[clave], device.macAddrStr, self.nombreCliente), 'MEDICIONES_RSSI')

            if(self.finalizar): #sale del bucle si se ha indicado que cierre la conexion con el cliente
                break
            self.sockCliente.send("OK".encode()) # envia ACK al cliente

        # Contestacion y cierre de conexion cuando lo indique el servidor
        self.sockCliente.send("FIN".encode())
        self.sockCliente.close()
        self.baseDeDatos.finalizarConexion()
        print("\x1b[1;31m" + ">>> Finaliza recepción de datos del cliente '" + self.nombreCliente + "'") 
        print("\x1b[3;37m" + "desconectado " + str(self.datosCliente) + "\x1b[0;37m")

