#!/usr/bin/python3
#-*- coding: utf-8 -*-

# Install
# pip install tabulate
# pip install gTTS
# pip install playsound

"""Contiene las clases Servidor y DispositivoBluetooth, que se utilizan para la recepción y
guardado de los datos enviados por el cliente (ESP32)."""

from threading import Thread
import time
import struct
import numpy as np
import select
import socket
from tabulate import tabulate
from gtts import gTTS
from playsound import playsound


class DispositivoBluetooth:
    """ Clase que guarda los datos de un dispositivo bluetooth detectado.
    
    Los datos son recibidos y decodificados en la clase Servidor. 
    """
    def __init__(self):
        """ Inicializa los campos a un valor por defecto.
        """
        self.nameLength = 0
        self.name = "-"
        self.macAddrStr = ""
        self.rssi = {} 
        self.txpower = 0
        self.cod = 0
        self.majorDevClass = ""
        self.tipoMacAddr = ""
        self.tipoBT = ""
        self.numCelular = ""

class Servidor(Thread):
    """ Es un Thread que se dedica a atender a un cliente específico.

    Recibe continuamente datos de dispositivos bluetooth detectados, enviados por un cliente.
    Decodifica y guarda en la Base de datos tales datos.
    """

    def __init__(self, sockCliente, datosCliente, nroCelular, distanciasCalibracion, cantMuestras, tipoRegresion):
        """ Inicializa atributos del socket y para la calibración.

        :param sockCliente: socket cliente del cual recibe los datos.
        :type sockCliente: socket
        :param datosCliente: IPv4 y puerto del socket cliente
        :type datosCliente: tuple
        """
        print("Creando hilo servidor...")
        # LLama al constructor padre, para que se inicialice de forma correcta la clase Thread.
        Thread.__init__(self)  
        self.sockCliente = sockCliente 
        self.datosCliente = datosCliente 
        self.nombreCliente = ""
        self.finalizar = False
        self.nroCelular = nroCelular
        self.distanciasCalibracion = distanciasCalibracion
        self.cantMuestras = cantMuestras
        self.tipoRegresion = tipoRegresion


    def finalizarRecepcion(self):
        """ Finaliza conexion con el cliente y sale del bucle de recepcion.
        """
        self.finalizar = True

    def filtrarAtipicos(self, valores_rssi):
        """ Filtra los valores de RSSI que estén dentro de un desvio estandar
        y luego calcula el promedio de los filtrados.
        
        Se le pasan directamente la lista con los valores a filtrar.
        :param valores_rssi: valores a filtrar
        :type valores_rssi: list
        :returns: promedio de los valores filtrados.
        """

        lim_inferior = np.mean(valores_rssi) - np.std(valores_rssi)
        lim_superior = np.mean(valores_rssi) + np.std(valores_rssi)
        cant_datos_atipicos = 0
        rssi_filtrado = []

        for rssi in valores_rssi:
            if(lim_inferior < rssi and rssi < lim_superior):
                rssi_filtrado.append(rssi)
            else:
                cant_datos_atipicos += 1

        promedio_final = np.mean(rssi_filtrado)
        print("promedio: ", promedio_final)
        print("datos atipicos descartados: ", cant_datos_atipicos)

        return round(promedio_final, 3)

    def ajustarRegresionLog(self, valores_x, valores_y):
        """ Ajusta una curva logarítmica (en base 10) al conjunto de datos.
        
        Imprime la ecuación de la regresión y el valor de R cuadrado.  

        :param valores_x: lista con los valores de la variable independiente(X).
        :type valores_x: list
        :param valores_y: lista con los valores de la variable dependiente(Y)
        :type valores_y: list
        """
        x = valores_x
        y = valores_y

        #fit the model
        fit = np.polyfit(np.log10(x), y, deg=1)
        term_ind = round(fit[1], 3)
        factor_log = round(fit[0], 3)

        #determinamos valor de R cuadrado
        correlation_matrix = np.corrcoef(np.log10(x), y)
        correlation_xy = correlation_matrix[0,1]
        r_squared = round(correlation_xy**2, 5)

        #print("Ecuación: Y = " + str(term_ind) + " + (" + str(factor_log) + ") * log10(X)")
        #print("R cuadrado:", r_squared)

        table = [["Ecuación", "RSSI = " + str(term_ind) + " + (" + str(factor_log) + ") * log10(Distancia)"], ["R cuadrado", r_squared]]
        print(tabulate(table, [], tablefmt="grid"))


    def ajustarRegresionPotencial(self, valores_x, valores_y):
        """ Ajusta una curva potencial al conjunto de datos. (y = A * x^B)
        
        Imprime la ecuación de la regresión y el valor de R cuadrado.  

        :param valores_x: lista con los valores de la variable independiente(X).
        :type valores_x: list
        :param valores_y: lista con los valores de la variable dependiente(Y)
        :type valores_y: list
        """
        x = np.log(valores_x) # se toma el log natural para transformar a regresion lineal
        y = np.log(valores_y)

        #fit the model
        fit = np.polyfit(x, y, deg=1)
        y_intercept = fit[1]
        pendiente = round(fit[0], 5)

        #determinamos valor de R cuadrado
        correlation_matrix = np.corrcoef(x, y)
        correlation_xy = correlation_matrix[0,1]
        r_squared = round(correlation_xy**2, 5)

        term_ind = round(np.exp(y_intercept), 5) # se obtiene el valor de A
        exponente = pendiente  # B

        print("Ecuación: Y = " + str(term_ind) + " * X^" + str(exponente))
        print("R cuadrado:", r_squared)

        return (term_ind, exponente)


    def run(self):
        """ Metodo principal del hilo Servidor.

        Recibe el nombre del cliente y envía el timestamp actual.
        Recibe continuamente mediciones tomadas por el cliente y las decodifica,
        hasta que finalice la conexion.
        """

        self.nombreCliente = self.sockCliente.recv(128)  #recibe el nombre de la ESP32 cliente
        self.nombreCliente = self.nombreCliente.decode().split("\x00", 1)[0]
        print("\x1b[1;32m" + "Atendiendo a cliente '" + str(self.nombreCliente) + "'..." + "\x1b[0;37m")

        timestamp = int(time.time()) #envia timestamp actual
        # para enviar el num por socket se convierte en str y luego en bytes
        self.sockCliente.send(bytes(str(timestamp), 'utf8'))

        # ESP32 es little endian
        fmt = "<c249s6s18s" + 20*"Qb" + "bI60s11s10s11s"  # es como se debe decodificar el paquete que llega
        fmt_size = struct.calcsize(fmt)  # calcula el tamaño total que debe tener la estructura (551 bytes)
        datoNext = b''  #byte vacio
        cantMediciones = 0
        indice = 0  # indica la distancia actual dentro de la lista distanciasCalibracion
        distanciaActual = self.distanciasCalibracion[indice] # distancia actual en la que se deben estar tomando las mediciones
        mediciones = {} # diccionario que contiene una lista de RSSI por cada distancia de calibracion
        mediciones[distanciaActual] = [] # key: distancia, value: [rssi,...]

        timestampUmbral = int(time.time())

        while(not(self.finalizar)):
            # Espera por datos
            data = self.sockCliente.recv(fmt_size) #tam buffer maximo = 10 estructuras
            data = datoNext + data   # se le concatena al principio trozos de paquetes incompletos del recv anterior
            resto = len(data) % fmt_size   # fmt_size = 551 bytes
            if(resto != 0):
                datoNext = data[-resto :]   #guardo el ultimo trozo de paquete incompleto para la siguiente iteracion
                data = data[: -resto]       #elimina el paquete que esta incompleto de los datos a guardar en esta iteracion
            else:
                datoNext = b''

            if(len(data) < fmt_size):   #no se pueden tomar los datos si el tamaño es menor a 1 paquete
                #print("MENOS DE 551 bytes: ---------------------------------------------------", len(data))
                continue

            cantPaquetes = len(data) // fmt_size   #cantidad de paquetes completos
            for i in range(0, cantPaquetes):
                device = DispositivoBluetooth()
                # DECODIFICAN DATOS
                tuplaDatos = struct.unpack(fmt, data[i*fmt_size:((i+1)*fmt_size)])  #se separan los bytes de datos en una tupla

                device.nameLength = int.from_bytes(tuplaDatos[0], "little", signed="False") # para convertir de bytes a int (sin signo)
                try:
                    device.name = tuplaDatos[1].decode().split("\x00", 1)[0] # para convertir de bytes a string
                    device.macAddrStr = tuplaDatos[3].decode().split("\x00", 1)[0] #se decodifican los bytes y se eliminan los '\x00'
                except UnicodeDecodeError:
                    print(UnicodeDecodeError)

                for i in range(4,44,2):
                    timestamp = tuplaDatos[i]
                    rssi = tuplaDatos[i+1]
                    if(timestamp == 0): break  #los que son ceros no los debe tomar
                    #los timestamp que no son actuales (por retardo en la conexion con el socket servidor) no los considera
                    if(timestamp < 100000000): continue 
                    device.rssi[timestamp] = rssi

                device.txpower = tuplaDatos[44]
                device.cod = hex(tuplaDatos[45])
                try:
                    device.majorDevClass = tuplaDatos[46].decode().split("\x00", 1)[0]
                    device.tipoMacAddr = tuplaDatos[47].decode().split("\x00", 1)[0]
                    device.tipoBT = tuplaDatos[48].decode().split("\x00", 1)[0]
                    device.numCelular = tuplaDatos[49].decode().split("\x00", 1)[0]
                except UnicodeDecodeError:
                    print(UnicodeDecodeError)

                if(device.numCelular != self.nroCelular): # tomamos mediciones unicamente del celular utilizado en la calibracion
                    continue  # siguiente paquete
                
                timestampRecibidos = list(device.rssi.keys()) # se toman la lista de timestamps de las mediciones realizadas para el celular especifico
                # si el 1er timestamp (mas antiguo) no supera el umbral no se considera ninguna medicion de las recibidas en el mismo paquete
                if(timestampRecibidos[0] < timestampUmbral): 
                    continue

                print("DEVICE: " + device.macAddrStr +" | "+ device.name +" | " + device.numCelular +" |", device.rssi)
                valores_rssi = device.rssi.values() # obtiene los valores del diccionario (son uno o varios rssi)
                
                cantMediciones += len(valores_rssi)  # en un mismo paquete pueden venir varios RSSI del mismo celular
                print("cant mediciones:", cantMediciones)

                mediciones[distanciaActual] += valores_rssi  # concatena los valores de rssi antiguos con los nuevos
                print("med", mediciones)
                print("med2", mediciones[distanciaActual])

                while(cantMediciones > self.cantMuestras): # en caso de que nos excedamos en la cant de muestras, se eliminan las ultimas
                    mediciones[distanciaActual].pop()
                    cantMediciones -= 1
                
                if(cantMediciones == self.cantMuestras): # fin de una etapa
                    indice += 1

                    if(indice >= len(self.distanciasCalibracion)): # fin obtencion de datos
                        # se filtran datos atipicos y se calcula la ecuacion de la curva de regresion y su R2
                        for clave in mediciones.keys():
                            mediciones[clave] = self.filtrarAtipicos(mediciones[clave])
                 
                        print("Datos de entrada a la regresion:", mediciones)
                        
                        if(self.tipoRegresion == "log"): # usar logarithmic regression
                            self.ajustarRegresionLog(list(mediciones.keys()), list(mediciones.values()))
                        
                        elif(self.tipoRegresion == "power"): # usar power regression
                            rssi = list(mediciones.values())
                            distancia = list(mediciones.keys())
                            RSSI_1m = float(input("Ingrese el RSSI promedio medido a 1m de distancia:"))
                            #se divide cada valor de rssi por el rssi a 1m de distancia
                            ratio = list(map(lambda x: x/RSSI_1m, rssi))
                            (A, B) = self.ajustarRegresionPotencial(ratio, distancia)

                            ratio_1m_medido = ratio[distancia.index(1.0)]
                            predicted_distance = A*(ratio_1m_medido**B) #distancia predicha para el rssi a 1m 
                            C = round(1 - predicted_distance, 5)  #C = valor real - valor predicho (a 1m)
                            print("A:", A, "B:", B, "C:", C)
                            print("Ecuación: Distancia = " + str(A) + " * (RSSI/rssi_1m)^" + str(B) + " + " + str(C))

                        self.finalizar = True
                    else:
                    # si se llega a la cant de muestras se debe indicar al usuario que desplace el dispositivo de calibracion
                        distanciaActual = self.distanciasCalibracion[indice]
                        print("proxima distancia:", distanciaActual)
                        cantMediciones = 0
                        mediciones[distanciaActual] = []
                        
                        print("\x1b[1;33m" + "\nIncremente la distancia a " + str(distanciaActual) + " metros.")
                        # se reproduce un sonido indicandole al usuario que debe aumentar la distancia de medicion un paso
                        entrada = "Etapa finalizada. Incremente la distancia a " + str(distanciaActual) + " metros."
                        sp = gTTS(text=str(entrada), lang='es', slow=False)
                        sp.save('/tmp/texto.mp3') # guarda archivo mp3 de forma temporal
                        playsound('/tmp/texto.mp3') # reproduce sonido
                        
                        input("\x1b[3;37m" + "Presione una tecla para continuar..." + "\x1b[0;37m")
                        timestampUmbral = int(time.time()) # timestamp a partir del cual las mediciones son validas

                      #  self.emptySocket(self.sockCliente, fmt_size)

            if(self.finalizar): #sale del bucle si se ha indicado que cierre la conexion con el cliente
                break
            self.sockCliente.send("OK".encode()) # envia ACK al cliente

        # Contestacion y cierre de conexion cuando lo indique el servidor
        self.sockCliente.send("FIN".encode())
        self.sockCliente.close()
        print("\x1b[1;31m" + "\n>>> Finaliza recepción de datos del cliente '" + self.nombreCliente + "'") 
        print("\x1b[3;37m" + "desconectado " + str(self.datosCliente))
        print("\x1b[1;32m" + "Calibración finalizada correctamente" + "\x1b[0;37m")
        entrada = "Calibración finalizada correctamente."
        sp = gTTS(text=str(entrada), lang='es', slow=False)
        sp.save('/tmp/texto.mp3') # guarda archivo mp3 de forma temporal
        playsound('/tmp/texto.mp3') # reproduce sonido

if(__name__ == "__main__"):
    #X = [0.2,0.5,1,1.5,2,2.5,3,3.6,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9]
    #Y = [-56.96154,-55.375,-67.44444,-64.48148,-73.35,-72.04167,-72.24138,-72.48,-75.375,-72.08333,-75.16129,-72.78571,-70,-78.18519,-79.35714,-77.72222,-79.47368,-75.51613,-73.14286]
   # Y = [-46.05263,-54.03333,-58,-62.23077,-63.78788,-60.84848,-61.58065,-75.21875,-81.7619,-74.65,-73.72727,-77.47059,-71.48276,-71.15385,-71.89474,-73.2,-72.77143,-73.82353,-74.37931]
    X=[0.1,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5]
    Y=[-52.71429,-73.86364,-65.57895,-62.09524,-64.125,-71.76,-69.5,-77.42105,-80.21429,-75.21429,-77.55]
    ajustarRegresionLog(X,Y)
    #rssi=[-56,-71,-56,-59,-58,-55,-71,-56,-59,-55,-58,-73,-58,-58,-74,-74,-56,-55,-56,-55,-60,-55,-58,-73,-59,-58,-55,-73,-56,-55,-74,-73,-59,-57,-59]
    #rssi=[-78,-78,-78,-81,-78,-79,-78,-78,-80,-80,-81,-80,-79,-79,-79,-79,-81,-80,-79,-79,-81,-80,-79,-80,-80,-79,-78,-78,-81,-80,-81,-80,-82,-81,-79]
    rssi=[-70,-75,-70,-77,-75,-76,-77,-70,-70,-70,-77,-70,-75,-70,-71,-70,-81,-75,-82,-70,-81,-79,-77,-70,-69,-69,-75,-76,-70,-76,-70,-76,-70,-70,-69]
    filtrarAtipicos(rssi)