#!/usr/bin/python3
#-*- coding: utf-8 -*-


"""Contiene el metodo principal y la clase Main como punto de entrada en la ejecucion del programa."""

from Servidor import Servidor
import socket
#import time

# ----------------------------- CONSTANTES --------------------------------------
class ctes:
    CANT_MAX_CALIBRACION = 3
    PORT = 4444
    IPv4 = "192.168.2.1" 
# -------------------------------------------------------------------------------

class Main:
    """ Clase principal.
    """
    def __init__(self, nroCelular, distanciasCalibracion, cantMuestras, cantCalibrar, tipoRegresion):
        """ Crea el socket y espera por conexiones del cliente.
        """

        print("Creando hilos servidores...")
        self.nroCelular = nroCelular
        self.distanciasCalibracion = distanciasCalibracion
        self.cantMuestras = cantMuestras
        self.cantCalibrar = cantCalibrar
        self.tipoRegresion = tipoRegresion

        self.sock = self.crearSocket(ctes.PORT)
        self.hilos = self.esperarConexiones(self.sock, self.nroCelular, self.distanciasCalibracion, self.cantMuestras, self.tipoRegresion)


    def crearSocket(self, port):
        """ Crea el socket servidor con la ip local del host y puerto especificado.

        :param port: puerto de escucha del servidor.
        :type port: int
        :returs: socket servidor.
        :rtype: socket
        """
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Bind the socket to the port
        serverAddress = (ctes.IPv4, port)
        print("\x1b[1;33m" + 'starting up on {} port {}'.format(*serverAddress) + "\x1b[0;37m")
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(serverAddress)
        # Espera por X conexiones, el resto las rechaza
        sock.listen(ctes.CANT_MAX_CALIBRACION)
        return sock

    def esperarConexiones(self, server, nroCelular, distanciasCalibracion, cantMuestras, tipoRegresion):
        """ Espera por conexiones de N clientes. Crea un hilo para cada nueva conexion entrante.

        :param server: socket servidor.
        :type server: socket
        :returns: lista de hilos Servidores.
        :rtype: list
        """
    # bucle para atender cliente
        clientes = cant_a_calibrar  
        hilos = []
        while(clientes > 0):
            # Se espera a un cliente
            socketCliente, datosCliente = server.accept()
            print("\x1b[1;32m" + "conectado " + str(datosCliente) + "\x1b[0;37m")
            # Se crea la clase con el hilo
            hilos.append(Servidor(socketCliente, datosCliente, nroCelular, distanciasCalibracion, cantMuestras, tipoRegresion))
            clientes -= 1 

        for item in hilos: #se arrancan todos los hilos juntos
            item.start()
        return hilos

    def finalizarRecepcion(self):
        """ Cierra la conexion de cada hilo servidor con su cliente.
        """
        for item in self.hilos:
            item.finalizarRecepcion()

    def esperarHilos(self):
        """ Espera (join) por la finalizacion de todos los hilos servidores. 
        """
        for item in self.hilos: #espera por la finalizacion de todos los hilos servidores
            item.join()
        
if(__name__ == "__main__"):

    #imprime texto aclaratorio con recomendaciones
    with open('Recomendaciones.txt', 'r') as recommendation_file:
        texto = recommendation_file.read()
    
    print('\n' + texto + '\n')

    try: # se obtienen los datos de un archivo de configuracion si existe tal archivo
        config_file = open('config.txt', 'r')
        for linea in config_file:
            linea = linea.strip() # elimina espacios al comienzo y al final
            if(linea.startswith('#') or (linea == '')): # la lineas que comienzan con '#' son ignoradas
                continue
            arg_config = linea.split('=')[0].strip()
            valor_config = linea.split('=')[1].strip()

            if(arg_config == 'distancias_calibracion'):
                distancias_calibracion = str(valor_config)
            elif(arg_config == 'cant_muestras'):
                cant_muestras = int(valor_config)
            elif(arg_config == 'nro_celular'):
                nro_celular = str(valor_config)
            elif(arg_config == 'cant_a_calibrar'):
                cant_a_calibrar = int(valor_config)
            elif(arg_config == 'tipo_regresion'):
                tipo_regresion = str(valor_config)

        config_file.close()

    except FileNotFoundError: # si no existe, la configuracion es ingresada por teclado
        # distancias de calibracion en metros de cada etapa separadas por espacios
        distancias_calibracion = str(input("Ingrese las distancias de calibración [en metros] separadas por un espacio:")) 
        # cantidad de muestras tomadas en cada etapa
        cant_muestras = int(input("Ingrese la cantidad de muestras por etapa:"))
        # nro de celular del dispositivo que se utiliza para calibrar
        nro_celular = str(input("Ingrese el numero de celular del dispositivo utilizado para calibrar:"))
        nro_celular = nro_celular.strip() # elimina espacios
        # cantidad de esp32 a calibrar
        cant_a_calibrar = int(input("Ingrese la cantidad de dispositivos a calibrar:"))
        # tipo de regresion a utilizar (log o power)
        tipo_regresion = str(input("Ingrese el tipo de regresion a utilizar [log/power]:"))
        tipo_regresion = tipo_regresion.strip()  # elimina espacios
        
    print(distancias_calibracion)
    distancias_calibracion = distancias_calibracion.split(sep=" ")
    while True:
        try:
            distancias_calibracion.remove(" ")  # remueve los espacios sobrantes
        except ValueError:
            # cada distancia se convierte de str a float
            distancias_calibracion = list(map(lambda x: float(x), distancias_calibracion))
            break
    distancias_calibracion.sort() # ordena de menor a mayor

    print(distancias_calibracion)
    tipo_regresion = tipo_regresion.lower() # convierte a minusculas
    if((tipo_regresion != 'log') and (tipo_regresion != 'power')):
        raise Exception("Tipo de regresión incorrecta. Sólo se admiten los tipos log y power.")

    main = Main(nro_celular, distancias_calibracion, cant_muestras, cant_a_calibrar, tipo_regresion)

    #ret = str(input("Finalizar conexion? [y/n]"))
    #if(ret == 'y'):
     #   main.finalizarRecepcion()

    main.esperarHilos()
    