#!/usr/bin/python3
#-*- coding: utf-8 -*-

"""Clase que permite el manejo de una Base de datos para guardar datos y mediciones de dispositivos detectados."""

import sqlite3
from sqlite3 import Error

# ----------------------------- PATH --------------------------------------
class paths:
    BDD = "dispositivos_detectados.db"
    #BDD = 'file:dispositivos_detectados.db?mode=memory&cache=shared'
    #BDD = 'file:dispositivos_detectados.db?cache=shared'
# -------------------------------------------------------------------------

class BaseDeDatos:
    """ Clase Base de datos que permite el manejo de la misma, es de tipo SQLite3. 

    No se utiliza un Monitor, ya que la concurrencia es manejada por SQLite al ser transaccional.
    """

    def __init__(self, path):
        """ Inicializador de instancias de la clase BaseDeDatos.

        Establece una conexión y crea las tablas.
        :param path: ruta a la base de datos (archivo.db)
        :type path: str
        """
        #print("Creando base de datos..")
        try:
            #self.conexionBdd = sqlite3.connect(path, check_same_thread=False, uri=True) #path a la base de datos
            self.conexionBdd = sqlite3.connect(path, check_same_thread=False) #path a la base de datos
            #print("Conexión establecida, Base de datos creada en memoria.")
        except Error:
            print(Error)
        self.conexionBdd.execute("PRAGMA foreign_keys = 1") #para habilitar las foreign keys
        self.cursorBdd = self.conexionBdd.cursor()
        self.crearTablas()

    def finalizarConexion(self):
        """ Cierra la conexion con la BDD. """
        self.conexionBdd.close()

    def crearTablas(self):
        """ Crea una tabla con atributos en la Base de datos.
        
        Crea, si no existen, dos tablas, una para guardar info de los dispositivos detectados
        y otra para contener los RSSI medidos a lo largo del tiempo. 
        """
        self.cursorBdd.execute('''CREATE TABLE IF NOT EXISTS INFO_DISPOSITIVOS (
                                MAC text not null, NAME text, MAJOR_DEVICE_CLASS text, 
                                TX_POWER integer, COD integer, TIPO_MAC_ADDR text, TIPO_BT text, NUM_CELULAR text,
                                ID_CLIENTE text not null,
                                primary key (MAC, ID_CLIENTE))''')  

        self.cursorBdd.execute('''CREATE TABLE IF NOT EXISTS MEDICIONES_RSSI (
                                TIMESTAMP integer, RSSI integer, MAC text not null, ID_CLIENTE text not null,
                                FOREIGN KEY (MAC, ID_CLIENTE) REFERENCES INFO_DISPOSITIVOS(MAC, ID_CLIENTE) ON DELETE CASCADE)''')
        self.conexionBdd.commit() #confirmamos los cambios a realizar 

    def getTablas(self):
        """ Lista las tablas que hay en la base de datos. 

        :returns: devuelve una lista con los nombres de las tablas de la BDD.
        :rtype: list
        """
        self.cursorBdd = self.conexionBdd.cursor()
        self.cursorBdd.execute('SELECT name from sqlite_master where type= "table"')
        listaTablas = self.cursorBdd.fetchall()
        return listaTablas

    def tablaExists(self, nombreTabla):
        """ Chequea si esxiste una tabla en la BDD con el nombre indicado.

        :param nombreTabla: nombre de la tabla a verificar
        :type nombreTabla: str
        :returns: True si existe una tabla con dicho nombre, False en caso contrario.
        :rtype: bool
        """
        self.cursorBdd.execute("SELECT name from sqlite_master WHERE type='table' AND name='" + str(nombreTabla) + "'")
        ret = self.cursorBdd.fetchall()
        if(ret == []):
            return False
        else: 
            return True

    def insertarDatos(self, datos, tabla):
        """ Inserta una nueva entrada (datos de un dispositivo) en una de las tablas.

        :param datos: lista con todos los datos a insertar.
        :type datos: list
        :param tabla: nombre de la tabla.
        :type tabla: str
        """
        if(tabla == 'INFO_DISPOSITIVOS'):
            comando = 'INSERT INTO INFO_DISPOSITIVOS(MAC, NAME, MAJOR_DEVICE_CLASS, TX_POWER, COD, TIPO_MAC_ADDR, TIPO_BT, NUM_CELULAR, ID_CLIENTE) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)'
        elif(tabla == 'MEDICIONES_RSSI'):
            comando = 'INSERT INTO MEDICIONES_RSSI(TIMESTAMP, RSSI, MAC, ID_CLIENTE) VALUES(?, ?, ?, ?)'
        else:
            raise Exception("No existe una tabla llamada '" + str(tabla) + "'")
            return #si no coincide con ninguna tabla retorna

        #print(comando)
        self.cursorBdd.execute(comando, datos)
        self.conexionBdd.commit() #confirmamos los cambios a realizar 

    def actualizarDatos(self, tabla, atributo, nuevoValor, macAddr, idCliente):
        """ Actualiza los datos de una entrada en una tabla de la base de datos. 
        
        :param tabla: nombre de la tabla.
        :type tabla: str
        :param atributo: nombre del atributo a actualizar.
        :type atributo: str
        :param nuevoValor: nuevo valor a insertar.
        :param macAddr: 1er primary key de la entrada a actualizar.
        :type macAddr: str
        :param idCliente: 2da primary key, es el identificador del cliente que envio la medicion.
        :type idCliente: str
        """
        if(not(self.tablaExists(tabla))): #si no existe una tabla con ese nombre se lanza una excepcion
            raise Exception("No existe una tabla llamada '" + str(tabla) + "'")
        
        #print(nuevoValor)
        comando = "UPDATE " + str(tabla) + " SET " + str(atributo) + "='" + str(nuevoValor) + "' WHERE MAC='" + str(macAddr) + "' AND ID_CLIENTE='" + str(idCliente) + "'"       
        #print(comando)
        self.cursorBdd.execute(comando)
        self.conexionBdd.commit()

    def borrarEntrada(self, tabla, macAddr, idCliente):
        """ Borra los datos de un dispositivo. Elimina una entrada de la tabla.

        :param tabla: nombre de la tabla.
        :type tabla: str
        :param macAddr: mac address de la entrada a eliminar.
        :type macAddr: str
        :param idCliente: identificador del cliente que envio la medicion que se desea eliminar.
        :type idCliente: str
        """
        if(not(self.tablaExists(tabla))): #si no existe una tabla con ese nombre se lanza una excepcion
            raise Exception("No existe una tabla llamada '" + str(tabla) + "'")

        comando = "DELETE FROM " + str(tabla) + " WHERE MAC='" + str(macAddr) + "' AND ID_CLIENTE='" + str(idCliente) + "'"       
       #print(comando)
        self.cursorBdd.execute(comando)
        self.conexionBdd.commit()

    def borrarDatosAntiguos(self, timestamp):
        """ Elimina todas las mediciones antiguas (menores a cierto timestamp) de la tabla MEDICIONES_RSSI.

        :param timestamp: timestamp de umbral, los registros que tengan un timestamp menor al mismo se eliminarán.
        :type timestamp: int
        """
        comando = "DELETE FROM MEDICIONES_RSSI WHERE TIMESTAMP < " + str(timestamp)       
        #print(comando)
        self.cursorBdd.execute(comando)
        self.conexionBdd.commit()


    def checkEntrada(self, macAddr, idCliente):
        """ Chequea si existe una entrada en la base de datos para un dispositivo.
            
        :param macAddr: mac address.
        :type macAddr: str
        :param idCliente: identificador del cliente.
        :type idCliente: str
        :returns: False si no existe entrada, True en caso contrario. 
        :rtype: bool
        """
        ret = self.leerDatos('INFO_DISPOSITIVOS', macAddr, idCliente)
        if(ret == []):
            return False
        else: 
            return True
    
    def checkTelefono(self, telefono):
        ret = self.leerTelefono('INFO_DISPOSITIVOS', telefono)
        if(ret == []):
            return False
        else: 
            return True

    def leerTelefono(self, tabla, telefono):
        """ Obtiene la informacion de un cierto numero de telefono

        :param tabla: tabla de consulta
        :type tabla: str
        :param telefono: numero de telefono
        :type telefono: str
        :returns: Lista con los datos obtenidos
        :rtype: list
        """
        if(not(self.tablaExists(tabla))): #si no existe una tabla con ese nombre se lanza una excepcion
            raise Exception("No existe una tabla llamada '" + str(tabla) + "'")

        comando = "SELECT * FROM " + str(tabla) + " WHERE NUM_CELULAR='" + str(telefono) + "'"
        #print(comando)
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        #for row in datos:
         #   print(row)
        return datos

    def obtenerMacTel(self, telefono):
        """ Obtiene la MAC de un cierto telefono

        :param telefono: numero de telefono
        :type telefono: str
        :returns: Lista con los datos obtenidos
        :rtype: list
        """
        comando = "SELECT distinct MAC FROM INFO_DISPOSITIVOS WHERE NUM_CELULAR='" + str(telefono) +"'"
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        return datos

    def obtenerTelefono(self,mac):
        """ Obtiene el telefono de una cierta direccion MAC

        :param mac: direccion MAC
        :type mac: str
        :returns: Lista con los datos obtenidos
        :rtype: list
        """
        if(self.checkTelefonoOfMac(mac)):
            comando = "SELECT distinct NUM_CELULAR FROM INFO_DISPOSITIVOS WHERE MAC='" + str(mac) +"'"
            self.cursorBdd.execute(comando)
            datos = self.cursorBdd.fetchall()
            return datos
        else:
            return ""

    def checkTelefonoOfMac(self, mac):
        ret = self.leerTelefonoOfMac('INFO_DISPOSITIVOS', mac)
        if(ret == []):
            return False
        else: 
            return True

    def leerTelefonoOfMac(self, tabla, mac):
        """ Obtiene la informacion de un cierto numero de telefono

        :param tabla: tabla de consulta
        :type tabla: str
        :param telefono: numero de telefono
        :type telefono: str
        :returns: Lista con los datos obtenidos
        :rtype: list
        """
        if(not(self.tablaExists(tabla))): #si no existe una tabla con ese nombre se lanza una excepcion
            raise Exception("No existe una tabla llamada '" + str(tabla) + "'")

        comando = "SELECT * FROM " + str(tabla) + " WHERE MAC='" + str(mac) + "'"
        #print(comando)
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        #for row in datos:
         #   print(row)
        return datos

    def leerDatos(self, tabla, macAddr, idCliente):
        """Obtiene de la base de datos los datos guardados para un dispositivo en una tabla.
        
        :param tabla: nombre de la tabla
        :type tabla: str
        :param macAddr: direccion MAC (primary key)
        :type macAddr: str
        :param idCliente: identificador de la ESP32 cliente que envio los datos (primary key)
        :type idCliente: str
        :returns: Lista con los datos obtenidos
        :rtype: list
        """
        if(not(self.tablaExists(tabla))): #si no existe una tabla con ese nombre se lanza una excepcion
            raise Exception("No existe una tabla llamada '" + str(tabla) + "'")

        comando = "SELECT * FROM " + str(tabla) + " WHERE MAC='" + str(macAddr) + "' AND ID_CLIENTE='" + str(idCliente) + "'"
        #print(comando)
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        #for row in datos:
         #   print(row)
        return datos

    def obtenerMacRSSI(self, NameEsp32):
        """ Obtiene las mediciones RSSI para cada MAC 
        """
        comando = "SELECT MAC,RSSI,TIMESTAMP FROM MEDICIONES_RSSI WHERE ID_CLIENTE='" + str(NameEsp32) +"'"
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        return datos

    def obtenerMacs(self, NameEsp32):
        comando = "SELECT distinct MAC FROM MEDICIONES_RSSI WHERE ID_CLIENTE='" + str(NameEsp32) +"'"
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        return datos

    def obtenerRSSIofMac(self, NameEsp32, mac):
        comando = "SELECT RSSI FROM MEDICIONES_RSSI WHERE ID_CLIENTE='" + str(NameEsp32) +"' and MAC='" + str(mac) + "'"
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        return datos

    def obtenerTimestampofMac(self, NameEsp32, mac):
        comando = "SELECT TIMESTAMP FROM MEDICIONES_RSSI WHERE ID_CLIENTE='" + str(NameEsp32) +"' and MAC='" + str(mac) + "'"
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        return datos
        
    def getAllDatos(self, macAddr, idCliente):
        """Obtiene todos los datos de una MAC address.

        Realiza un join de ambas tablas para extraer todos los datos que se registraron para dicha MAC, 
        filtrando tambien el cliente que envió tales datos.
        :param macAddr: mac address.
        :type macAddr: str.
        :param idCliente: identificador del cliente transmisor de los datos.
        :type idCliente: str
        :returns: Lista con todos los datos obtenidos.
        :rtype: list.
        """
        comando = "select * from INFO_DISPOSITIVOS join MEDICIONES_RSSI \
                   on INFO_DISPOSITIVOS.MAC=MEDICIONES_RSSI.MAC and INFO_DISPOSITIVOS.ID_CLIENTE=MEDICIONES_RSSI.ID_CLIENTE \
                   where INFO_DISPOSITIVOS.MAC='" + str(macAddr) + "' and INFO_DISPOSITIVOS.ID_CLIENTE='" + str(idCliente) + "'"
        #print(comando)
        self.cursorBdd.execute(comando)
        datos = self.cursorBdd.fetchall()
        #for row in datos:
        #   print(row)
        return datos

    def ejecutar(self, comando):
        """ Ejecuta el comando especificado en el argumento en la base de datos.  

        :param comando: comando a ejecutar.
        :type comando: str.
        """
        self.cursorBdd.execute(str(comando))

    def ejecutarSelect(self, comando):
        """ Ejecuta un SELECT en la base de datos y devuelve la lista con los resultados.

        :param comando: comando select a ejecutar.
        :type comando: str.
        :returns: Lista con el resultado del SELECT.
        :rtype: list.
        """
        if(str(comando).startswith("select") or str(comando).startswith("SELECT")):
            self.cursorBdd.execute(str(comando))
            datos = self.cursorBdd.fetchall()
            return datos
        else:
            raise Exception("El comando '" + str(comando) + "' no es una instrucción SELECT" )



# ----------------------------------------------------------------------------------

if(__name__ == '__main__'):
    bdd = BaseDeDatos(paths.BDD)
    #bdd.borrarEntrada('INFO_DISPOSITIVOS', '10:3f:4a:23:1a:69')
    dato1 = ['10:3f:4a:23:1a:69', 'Ale', 'Phone1', 5, 0x5a020c, None, None]
    dato2 = ['22:3f:4a:23:1a:33', 'Facu', 'Phone2', 3, 0x54444c, None, None]
    dato3 = ['ff:3f:4a:00:1a:69', 'Pepito', 'Phone', 7, 0x5a020c, None, None]
    #bdd.insertarDatos(dato1, 'INFO_DISPOSITIVOS')
    #bdd.insertarDatos(dato2, 'INFO_DISPOSITIVOS')
    #bdd.insertarDatos(dato3, 'INFO_DISPOSITIVOS')

    dato11 = (16999992201, -55, '10:3f:4a:23:1a:69')
   # bdd.insertarDatos(dato11, 'MEDICIONES_RSSI')
    #bdd.insertarDatos(dato1, 'INFO_DISPOSITIVOS')
   # print(bdd.checkEntrada('10:3f:4a:23:1a:69'))
   # print(bdd.checkEntrada('aa:3f:4a:23:1a:69'))
   # bdd.borrarEntrada('INFO_DISPOSITIVOS', '10:3f:4a:23:1a:69')
    #print("LEER: ",bdd.leerDatos("MEDICIONES_RSSI",'10:3f:4a:23:1a:69'))
    #print(bdd.getAllDatos('10:3f:4a:23:1a:69'))
    #bdd.actualizarDatos('MEDICIONES_RSSI', 'RSSI', -10, '10:3f:4a:23:1a:69')
    #print(bdd.getAllDatos('10:3f:4a:23:1a:69'))
    print(bdd.getTablas())
    print(bdd.tablaExists("INFO_DEVICE"))
    print(bdd.tablaExists("INFO_DISPOSITIVOS"))
    print(bdd.leerDatos( 'INFO_DISPOSITIVOS', '10:3f:4a:23:1a:69'))

    bdd2 = BaseDeDatos('otra_bdd.db')
    print(bdd2.cursorBdd == bdd.cursorBdd)
    print(bdd2 == bdd)

    bdd.finalizarConexion()
